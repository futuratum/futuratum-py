import numpy as np


def gen_payouts(balances):
    payouts = np.array(balances)
    for i in range(len(balances) - 1):
        gain = (payouts[i] / sum(payouts[i:])) * sum(payouts[i+1:])
        adjustments = np.zeros(len(balances))
        adjustments[i] = gain
        for j in range(i + 1, len(balances)):
            adjustments[j] = -(balances[j] / sum(balances[i+1:])) * gain
        payouts += adjustments
    return payouts


"""
psuedo code

// x[j:] all the values of x where the index >= j
// EG: x = [1, 2, 3]
// x[1:] == [2, 3]

function genPayouts(balances) {
  payouts = copy(balances);
  for (i = 0; i < balances.length; i += 1) {
    gain = (payouts[i] / (sum(payouts[i:]))) * sum(payouts[i+1:]);
    adjustments = zeroArray(length=balances.length);
    adjustments[i] = gain;
    for (j = i + 1; balances.length; j += 1) {
      adjustments[j] = -(balances[j] / sum(balances[i+1:]) * gain
    }
    payouts = addVectors(payouts, adjustments)
  }
}
"""


