# from __future__ import print_function
from sys import argv
import csv
import nltk
from saxutil.txt_proc import tkn_transform
from flowserve import desc_to_refnum_clf


def run_pipe_on_text(txt, pipe, tokenize_method=nltk.word_tokenize):
    intkns = tokenize_method(txt)
    ottkns, _ = tkn_transform.run_pipeline(pipe, intkns)
    return ' '.join(ottkns)


if __name__ == '__main__':
    pipe = desc_to_refnum_clf.mk_transform_pipe_v1()
    inpath = argv[1]
    txtcol = int(argv[2])
    delimiter = '\t'

    with open(inpath, encoding='utf8') as infile:
        rdr = csv.reader(infile, delimiter=delimiter)
        for row in rdr:
            row[txtcol] = run_pipe_on_text(row[txtcol], pipe)
            print(row[txtcol])
