import os
import time
import json
from sys import argv
import pandas as pd
import boto3
from lib.saxutil import date_util
import lib.trade.bittrex


if __name__ == '__main__':
    s3 = boto3.resource('s3')
    buck = s3.Bucket('millersolomon')
    sleep_seconds = int(argv[1])
    num_results_per_file = int(argv[2])
    results = []
    while 1 == 1:
        print('result set size: ' + str(len(results)))
        res = lib.trade.bittrex.get_market_summaries()
        results.extend(res['result'])
        print(len(results))
        if len(results) >= num_results_per_file:
            fn = 'bittrex.market.{ep0}.{ep1}.json'
            ep0 = date_util.convert_to_timestamp(
                pd.to_datetime(results[0]['TimeStamp'])
            )
            ep1 = date_util.convert_to_timestamp(
                pd.to_datetime(results[-1]['TimeStamp'])
            )
            fn = fn.format(ep0=str(ep0), ep1=str(ep1))
            print('WRITING TO: ' + fn)
            buck.put_object(
                Key=os.path.join('quote', fn),
                Body=json.dumps(results)
            )
            results = []
        time.sleep(sleep_seconds)
