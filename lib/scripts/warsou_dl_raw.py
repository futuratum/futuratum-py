import boto3
from lib.db.read.doc import biz as bizdb
from lib.crawl.forum import warsou


def dl_raws(opids):
	for i in range(len(opids)):
		print( str(i) + '/' + str(len(opids)) )
		res = warsou.request_postid_page(opids[i])
		if res.status_code == 404:
			raise Exception('404 response')
		s3 = boto3.resource('s3')
		buck = s3.Bucket('millersolomon')
		buck.put_object(
			Key='raw_docs/biz/{opid}.html'.format(opid=opids[i]),
			Body=res.text
		)


if __name__ == '__main__':
	print('Get OPIDS')
	opids = list(set(bizdb.biz_opids()))
	dl_raws(opids)

