import spacy
from lib.saxutil.txt_proc import letter_rep, tkn_transform


def run(txts):
    nlp = spacy.load(
        'en',
        add_vectors=False,
        entity=False,
        matcher=False,
        parser=False,
    )
    tknset = set()
    lrr = tkn_transform.LetterRepReplacer()
    tknset = set()
    for i in range(len(txts)):
        doc = nlp(txts[i])
        tkns = [w.lemma_ for w in doc]
        tags = [w.tag_ for w in doc]
        rtkns, rtags = lrr.run(tkns, tags)
        for j in range(len(rtkns)):
            if rtkns[j] != tkns[j]:
                tknset.add(rtkns[j])

    lrm = letter_rep.LetterRepMiner(nlp)
    cands_on_tkn = {}
    alltkns = list(tknset)
    for i in range(len(alltkns)):
        cands_on_tkn[alltkns[i]] = lrm.get_vocab_matches(alltkns[i])

    return cands_on_tkn



