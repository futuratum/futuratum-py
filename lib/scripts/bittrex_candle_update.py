from sys import argv
import json
import boto3
from lib.db import futr_cnx
from lib.db.importers.quote import bittrex


SYMBOL_CONVERTOR = {'BCC':'BCH'}


if __name__ == '__main__':
    cnx = futr_cnx.get_cnx()
    asset_name = argv[1]
    db_asset_name = SYMBOL_CONVERTOR.get(asset_name, asset_name)
    exasset_name = argv[2]
    epgap = int(argv[3])
    s3 = boto3.resource('s3')
    buck = s3.Bucket('millersolomon')
    pfx = 'quote/bittrex.candle.{asset_name}.{exasset_name}.{epgap}'
    pfx = pfx.format(
        asset_name=asset_name, exasset_name=exasset_name, epgap=str(epgap)
    )
    objs = buck.objects.filter(Prefix=pfx)
    candle_dicts = []
    for o in objs:
        dta = o.get()['Body'].read().decode('utf-8')
        candle_dicts.extend(json.loads(dta))
        print(o.key + '\t' + str(len(candle_dicts)))


    candle_row_genr = bittrex.BittrexCandleRowGenr.from_cnx(
        cnx, db_asset_name, exasset_name, 'bittrex', epgap
    )
    rows = candle_row_genr.extract_rows(candle_dicts=candle_dicts)
    # Defensive coding to prevent repeat inserts.
    # The database constraints and SQL conflict conditions should mean
    # this won't be an issue, but it's better to be safe than sorry.
    rows = list(set([tuple(t) for t in rows]))
    print('INSERTING ' + str(len(rows)) + ' ROWS.')
    bittrex.insert_rows(cnx, rows)
    print('DONE')


