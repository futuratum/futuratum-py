import os
import json
import pandas as pd
import boto3
from lib.saxutil import date_util


if __name__ == '__main__':
    s3 = boto3.resource('s3')
    buck = s3.Bucket('millersolomon')
    objs = list(buck.objects.filter(Prefix='quote/bittrex.market'))
    consolidated = []
    results = []
    for o in objs:
        if o.key.endswith('.ldjson'):
            continue
        results.extend(json.loads(o.get()['Body'].read()))
        consolidated.append(o.key)
        if len(results) >= 5000:
            break

    dta = ''
    for r in results:
        dta += json.dumps(r) + '\n'


    ep0 = date_util.convert_to_timestamp(
        pd.to_datetime(results[0]['TimeStamp'])
    )
    ep1 = date_util.convert_to_timestamp(
        pd.to_datetime(results[-1]['TimeStamp'])
    )
    fn = 'bittrex.market.{ep0}.{ep1}.ldjson'
    fn = fn.format(ep0=str(ep0), ep1=str(ep1))
    buck.put_object(
        Key=os.path.join('quote', fn),
        Body=dta
    )

    for o in objs:
        o.delete()




