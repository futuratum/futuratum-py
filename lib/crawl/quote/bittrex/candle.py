from sys import argv
import os
import json
import boto3
import bittrex
import lib.manage.quote.bittrex.candle as candle_manage


EPGAP_ON_TICK_INTERVAL = {'fiveMin':60 * 5, 'oneMin':60, 'thirtyMin':60 * 30}

def save_to_s3(asset, exasset, tick_interval):
    market = exasset + '-' + asset
    bo = bittrex.Bittrex('', '', api_version=bittrex.API_V2_0)
    res = bo.get_candles(market, tick_interval)
    candles = res['result']
    td = candle_manage.time_data_from_candles(
        candles, EPGAP_ON_TICK_INTERVAL[tick_interval]
    )
    fn = td.mk_filename(asset, exasset)
    kpath = os.path.join('quote', fn)
    print(kpath)
    s3 = boto3.resource('s3')
    buck = s3.Bucket('millersolomon')
    buck.put_object(
        Key=os.path.join('quote', fn),
        Body=json.dumps(candles),
    )

if __name__ == '__main__':
    save_to_s3(argv[1], argv[2], argv[3])