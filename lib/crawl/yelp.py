import os, random, time
import requests
from bs4 import BeautifulSoup
from lib.saxutil import bow


FUA = '''
Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36
'''.strip().replace('\n', ' ')

def get_rating(review_div):
	divs = review_div.find_all('div')
	imgs = divs[1].find_all('img')
	if len(imgs) != 1:
		raise Exception('Error parsing the rating.')
	ratingstr = imgs[0].get('alt')
	return ratingstr[:3]


def get_text(review_div):
	pgraphs = review_div.find_all('p')
	if len(pgraphs) != 1:
		raise Exception('Error parsing review text')
	return pgraphs[0].text


def parse_dta(pgdta):
	texts, ratings = [], []
	soup = BeautifulSoup(pgdta)
	review_divs = soup.find_all('div', {'class':'review-content'})
	for i in range(len(review_divs)):
		rating = get_rating(review_divs[i])
		text = get_text(review_divs[i])
		texts.append(text)
		ratings.append(rating)
	return texts, ratings


def get_all_reviews(baseurl, maxiter=10):
	alltexts, allratings = [], []
	cur_url = str(baseurl)
	for i in range(maxiter):
		resp = requests.get(cur_url, headers={'User-Agent': FUA})
		texts, ratings = parse_dta(resp.text)
		ratings = [float(r) for r in ratings]
		alltexts.extend(texts)
		allratings.extend(ratings)
		soup = BeautifulSoup(resp.text)
		link_to_next = soup.find_all(
			'a', {'class':'u-decoration-none next pagination-links_anchor'}
		)
		if len(link_to_next) == 0:
			break
		link_to_next = link_to_next[0]
		print(link_to_next.get('href'))
		cur_url = link_to_next.get('href')
	return alltexts, allratings


# Link format
# https://www.yelp.com/search?find_desc=starbucks&find_loc=Dallas,+TX&start=10

def get_location_links(search_pgdta):
	soup = BeautifulSoup(search_pgdta)
	review_divs = soup.find_all(
		'div', {'class':'search-result natural-search-result'}
	)
	links = set()
	for rd in review_divs:
		subdivs = rd.find_all('a', {'class':'js-analytics-click'})
		assert(len(subdivs) == 2)
		href = subdivs[0].get('href')
		href = 'https://www.yelp.com' + href
		links.add(href)
	return links


def dl_result_page_loc_links(desc, loc='Dallas,+TX'):
	headers = {'User-Agent': FUA}
	params = {'find_desc':desc, 'find_loc':loc}
	if start != 0:
		params['start'] = start
	resp = requests.get(
		'https://www.yelp.com/search', params=params, headers=headers
	)
	return get_location_links(resp.text)


def dl_result_page(code, start):
	headers = {'User-Agent': FUA}
	resp = requests.get(
		'https://www.yelp.com/biz/' + code,
		params={'start':start},
		headers=headers,
	)
	return resp
	#return get_location_links(resp.text)


def process_links(links, min_delay, max_delay):
	headers = {'User-Agent': FUA}
	txts, rtgs = [], []
	for i in range(len(links)):
		print(str(i) + '/' + str(len(links)))
		resp = requests.get(links[i], headers=headers)
		time.sleep(random.uniform(min_delay, max_delay))
		cur_txts, cur_rtgs = parse_dta(resp.text)
		txts.extend(cur_txts)
		rtgs.extend(cur_rtgs)
	return txts, rtgs



