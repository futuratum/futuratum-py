import os
import sys
import io
import json
import uuid
import boto3
import datetime
import requests
from collections import Counter
import pytz
import pandas as pd
import nltk
from bs4 import BeautifulSoup
from lib.saxutil.txt_proc import ngram_matcher
from lib.db import futr_cnx
from lib.db.read import base as readbase
from lib.manage.text import doc_biz


def parse_dt_from_html_elem(elem):
    dt_text = elem.find_all('span', {'class':'posttime'})[0].text
    return parse_str_to_dt(dt_text)


def post_dta_from_elem(elem, opid):
    pid = int(elem.find_all('a', {'class':'js'})[-1].text)
    ep = elem.find_all('span', {'class':'posttime'})[0].get('title')
    ep = int(int(ep) / 1000.0)
    text = elem.find_all('p', {'itemprop':'text'})[0].text
    entries = elem.find_all('p', {'itemprop':'text'})
    bklinks = entries[0].find_all('a', {'class':'backlink'})
    bk_ids = [int(e.text.strip().replace('>', '')) for e in bklinks]
    return {'pid':pid, 'opid':opid, 'text':text, 'ep':ep, 'bk_ids':bk_ids}
    #return doc_biz.BizPost(pid=pid, opid=opid, text=text, ep=ep, bk_ids=bk_ids)


def find_op_post(dta):
    '''
    :type dta: html string
    '''
    soup = BeautifulSoup(dta)
    divs = soup.find_all(
        'div', {'itemtype':'http://schema.org/DiscussionForumPosting'}
    )
    opid = int(divs[0].find_all('a', {'class':'js'})[-1].text)
    return post_dta_from_elem(divs[0], opid)


def find_reply_posts(dta):
    op = find_op_post(dta)
    posts = []
    soup = BeautifulSoup(dta)
    tds = soup.find_all('td', {'class':'reply'})
    for td in tds:
        posts.append(post_dta_from_elem(td, op['pid']))
    return posts


def find_all_posts(dta):
    posts = [find_op_post(dta)]
    posts.extend(find_reply_posts(dta))
    return posts


def request_postid_page(postid):
    url = 'https://warosu.org/biz/post/{pi}'.format(pi=str(postid))
    return requests.get(url)


def find_all_posts_from_postid_page(postid):
    res = request_postid_page(postid)
    if res.status_code == 404:
        raise Exception('404 Repsonse')
    return find_all_posts(res.text)


def find_posts_in_dir(dirpath):
    posts = []
    fns = os.listdir(dirpath)
    for i in range(len(fns)):
        print(str(i) + '/' + str(len(fns)))
        fp = os.path.join(dirpath, fns[i])
        with open(fp) as f:
            dta = f.read()
        posts.append(find_all_posts(dta))
    return posts


class PostDownloader(object):
    def __init__(self, cnx=None):
        if cnx is None:
            cnx = futr_cnx.get_cnx()
        self.cnx = cnx
        self.seen_pids = set(readbase.all_col_vals(self.cnx, 'bizmeta', 'pid'))
        self.max_docid = readbase.max_col_val(self.cnx, 'docmeta', 'docid')
        self.posts = []

    def already_have(self, pid):
        if pid in self.seen_pids:
            return True
        return False

    def maybe_download(self, pid):
        if self.already_have(pid):
            return
        try:
            newposts = find_all_posts_from_postid_page(pid)
        except Exception as e:
            # Page may not exist.
            # TODO: handle the 404 exception specifically rather than
            # using a general exception class
            return False
        for i in range(len(newposts)):
            self.seen_pids.add(newposts[i]['pid'])
            self.max_docid += 1
            newposts[i]['docid'] = int(self.max_docid)
        self.posts.extend(newposts)
        return True

    def dl_new(self, n, pid=None, verbose=True):
        if pid == None:
            pid = max(self.seen_pids)
        for i in range(pid + 1, pid + 1 + n):
            if verbose:
                print(str(i - pid) + '/' + str(n))
            self.maybe_download(i)

    def dl_old(self, n, pid=None, verbose=True):
        if pid is None:
            pid = min(self.seen_pids)
        for i in reversed(range(pid - n, pid)):
            if verbose:
                print(str(pid - i) + '/' + str(n))
            self.maybe_download(i)


class LocalFileSystemPostDownloader(PostDownloader):
    def __init__(self, dp, cnx=None):
        super(LocalFileSystemPostDownloader, self).__init__(cnx)
        self.dp = dp

    def commit_to_local_file(self, fn):
        fp = os.path.join(self.dp, str(fn) + '.ldjson')
        if os.path.exists(fp):
            raise IOError('Path: ' + str(fp) + ' already exists.')
        with open(fp, 'w') as f:
            for p in self.posts:
                f.write(json.dumps(p) + '\n')
        self.posts = []


class S3PostDownloader(PostDownloader):
    def __init__(self, buckname, pfx, cnx=None):
        super(S3PostDownloader, self).__init__(cnx)
        self.buckname = buckname
        self.pfx = pfx
        self.s3 = boto3.resource('s3')
        self.buck = self.s3.Bucket(buckname)

    def commit_to_s3_key(self, fn):
        dta = ''
        for p in self.posts:
            dta += json.dumps(p) + '\n'
        self.buck.put_object(
            Key=os.path.join(self.pfx, str(fn) + '.ldjson'),
            Body=dta
        )
        self.posts = []










