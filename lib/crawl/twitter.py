import tweepy


def mk_api(consumer_key, consumer_secret, access_key, access_secret):
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_key, access_secret)
    return tweepy.API(auth)


def mk_cursor(api, q):
    return tweepy.Cursor(api.search, q=q)