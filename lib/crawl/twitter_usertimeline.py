#!/usr/bin/env python
# encoding: utf-8

import tweepy #https://github.com/tweepy/tweepy
import json
import os
from sys import argv

#Twitter API credentials
consumer_key = os.environ['TWITTER_CONSUMER_KEY']
consumer_secret = os.environ['TWITTER_CONSUMER_SECRET']
access_key = os.environ['TWITTER_ACCESS_TOKEN']
access_secret = os.environ['TWITTER_TOKEN_SECRET']


def get_all_tweets(screen_name):
	#Twitter only allows access to a users most recent 3240 tweets with this method
	
	#authorize twitter, initialize tweepy
	auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_key, access_secret)
	api = tweepy.API(auth)
	
	#initialize a list to hold all the tweepy Tweets
	alltweets = []	
	
	#make initial request for most recent tweets (200 is the maximum allowed count)
	new_tweets = api.user_timeline(
		screen_name=screen_name,
		count=200
	)
	
	#save most recent tweets
	alltweets.extend([t._json for t in new_tweets])
	
	#save the id of the oldest tweet less one
	oldest = new_tweets[-1].id - 1
	
	#keep grabbing tweets until there are no tweets left to grab
	while len(new_tweets) > 0:
		print("getting tweets before %s" % (oldest))
		#all subsiquent requests use the max_id param to prevent duplicates
		new_tweets = api.user_timeline(
			screen_name=screen_name,
			count=200,
			max_id=oldest
		)
		if len(new_tweets) == 0:
			break
		#update the id of the oldest tweet less one
		oldest = new_tweets[-1].id - 1
		#update the id of the oldest tweet less one
		alltweets.extend([t._json for t in new_tweets])
		
		#update the id of the oldest tweet less one
		
		print("...%s tweets downloaded so far" % (len(alltweets)))
	
	#transform the tweepy tweets into a 2D array that will populate the csv	
	#outtweets = [[tweet.id_str, tweet.created_at, tweet.text.encode("utf-8")] for tweet in alltweets]
	
	#write the csv	
	with open('%s_tweets.ldjson' % screen_name, 'w') as f:
		for t in alltweets:
			f.write(json.dumps(t) + '\n')
	'''
	with open('%s_tweets.csv' % screen_name, 'wb') as f:
		writer = csv.writer(f)
		writer.writerow(["id","created_at","text"])
		writer.writerows(outtweets)
	'''


if __name__ == '__main__':
	#pass in the username of the account you want to download
	get_all_tweets(argv[1])