import pandas as pd
import datetime
from lib.saxutil import date_util
from lib.db.read import base as readbase
from lib.db import futr_cnx


class IdMap(object):
    ID_ON_ASSET = None
    ID_ON_QUOTETYPE = None
    IN_ON_QUOTESRC = None
    ASSET_ON_ID = None
    QUOTETYPE_ON_ID = None
    QUOTESRC_ON_ID = None

    @staticmethod
    def init_maps(cnx=None):
        if cnx is None:
            cnx = futr_cnx.get_cnx()
        IdMap.ID_ON_ASSET = readbase.kv_pairs(
            cnx, 'asset', 'assetname', 'id'
        )
        IdMap.ASSET_ON_ID = {v:k for k,v in IdMap.ID_ON_ASSET.items()}

        IdMap.ID_ON_QUOTETYPE = readbase.kv_pairs(
            cnx, 'quotetype', 'quotetype', 'id'
        )
        IdMap.QUOTETYPE_ON_ID = {v:k for k,v in IdMap.ID_ON_QUOTETYPE.items()}

        IdMap.ID_ON_QUOTESRC = readbase.kv_pairs(
            cnx, 'quotesrc', 'quotesrc', 'id'
        )
        IdMap.QUOTESRC_ON_ID = {v:k for k,v in IdMap.ID_ON_QUOTESRC.items()}

    @staticmethod
    def is_init():
        if (
            IdMap.ID_ON_ASSET is None or
            IdMap.ID_ON_QUOTETYPE is None or
            IdMap.ID_ON_QUOTESRC is None
        ):
            return False
        return True

    @staticmethod
    def val_from_key(k, d, allow_none):
        if allow_none:
            return d.get(k)
        return d[k]


    @staticmethod
    def asset_id(asset, allow_none=False):
        return IdMap.val_from_key(asset, IdMap.ID_ON_ASSET, allow_none)

    @staticmethod
    def asset_name(_id, allow_none=False):
        return IdMap.val_from_key(_id, IdMap.ASSET_ON_ID, allow_none)

    @staticmethod
    def quotetype_id(quotetype, allow_none=False):
        return IdMap.val_from_key(quotetype, IdMap.ID_ON_QUOTETYPE, allow_none)

    @staticmethod
    def quotetype_name(_id, allow_none=False):
        return IdMap.val_from_key(_id, IdMap.QUOTETYPE_ON_ID, allow_none)

    @staticmethod
    def quotesrc_id(quotesrc, allow_none=False):
        return IdMap.val_from_key(quotesrc, IdMap.ID_ON_QUOTESRC, allow_none)


def _add_wl(varcol, stmt, params, wl, wc):
    if wl is None or len(wl) == 0:
        return stmt, params
    stmt += varcol + ' IN ('
    for i in wl:
        params.append(i)
        stmt += wc + ','
    return stmt[:-1] + ') AND ', params


def gen_stmt_and_params(
    asset_exasset_pairs,
    minep,
    maxep,
    quotetype_wl=None,
    quotesrc_wl=None,
    epgap_wl=None,
    wc='%s',
):
    stmt = '''
    SELECT
    AQ.ep0,
    AQ.ep1,
    AST.assetname as asset,
    EXAST.assetname as exasset,
    AQ.val as val,
    QT.quotetype as quotetype,
    QS.quotesrc as quotesrc
    FROM ASSET_QUOTE AS AQ
    INNER JOIN QUOTETYPE AS QT ON AQ.quotetype_id = QT.id
    INNER JOIN ASSET AS AST ON AQ.asset_id = AST.id
    INNER JOIN ASSET AS EXAST ON AQ.exasset_id = EXAST.id
    INNER JOIN QUOTESRC AS QS ON AQ.quotesrc_id = QS.id
    WHERE
    AQ.ep0 >= {minep} AND
    AQ.ep1 <= {maxep} AND
    '''.format(minep=wc, maxep=wc).strip()
    stmt += ' '
    params = [minep, maxep]
    stmt, params = _add_wl('QT.quotetype', stmt, params, quotetype_wl, wc)
    stmt, params = _add_wl('QS.quotesrc', stmt, params, quotesrc_wl, wc)
    stmt, params = _add_wl('AQ.ep1 - AQ.ep0', stmt, params, epgap_wl, wc)
    for asset, exasset in asset_exasset_pairs:
        params.extend([asset, exasset])
        stmt += '''
        (AST.assetname = {asset} AND EXAST.assetname = {exasset}) OR
        '''.strip().format(asset=wc, exasset=wc)
    stmt = stmt[:-3] + ';'
    return stmt, tuple(params)


def fetch(
    asset_exasset_pairs,
    minep,
    maxep,
    quotetype_wl=None,
    quotesrc_wl=None,
    epgap_wl=None,
    cnx=None,
    wc='%s'
):
    stmt, params = gen_stmt_and_params(
        asset_exasset_pairs=asset_exasset_pairs,
        minep=minep,
        maxep=maxep,
        quotetype_wl=quotetype_wl,
        quotesrc_wl=quotesrc_wl,
        epgap_wl=epgap_wl,
        wc=wc
    )
    if cnx is None:
        cnx = futr_cnx.get_cnx()
    return pd.read_sql(stmt, con=cnx, params=params)


def fetch_last_n_seconds(
    asset_exasset_pairs,
    n,
    quotetype_wl=None,
    quotesrc_wl=None,
    cnx=None,
    wc='%s'
):
    maxep = date_util.convert_to_timestamp(
        pd.to_datetime(datetime.datetime.utcnow())
    )
    minep = maxep - n
    return fetch(
        asset_exasset_pairs=asset_exasset_pairs,
        minep=minep,
        maxep=maxep,
        quotetype_wl=quotetype_wl,
        quotesrc_wl=quotesrc_wl,
        wc=wc,
    )
