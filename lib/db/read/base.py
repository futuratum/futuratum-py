import pandas as pd


def mk_check_for_col_val_stmt(tbl, colname, vals):
    stmt = 'SELECT {colname} FROM {tbl} WHERE {colname} IN'.format(
        colname=colname, tbl=tbl
    )
    stmt += '(' + ','.join([str(v) for v in vals]) + ');'
    return stmt


def col_val_collisions(cnx, tbl, colname, vals):
    stmt = mk_check_for_col_val_stmt(tbl, colname, vals)
    df = pd.read_sql(stmt, cnx)
    found = df[colname.lower()].values
    return set([int(i) for i in found])


def termdta_on_term(cnx):
    df = pd.read_sql('SELECT term, termid, casesense FROM TERM;', cnx)
    terms = [str(t) for t in df['term'].values]
    terms = [tuple(str(t).split()) for t in terms]
    termids = df['termid'].values
    casebools = df['casesense'].values
    return {
        terms[i]:(int(termids[i]), casebools[i]) for i in range(len(df))
    }


def max_col_val(cnx, tbl, col):
    df = pd.read_sql(
        'SELECT MAX({col}) FROM {tbl};'.format(col=col, tbl=tbl), cnx
    )
    return df['max'].values[0]


def all_col_vals(cnx, tbl, col):
    df = pd.read_sql(
        'SELECT {col} FROM {tbl};'.format(col=col, tbl=tbl), cnx
    )
    return df[col].values


def max_biz_ep(cnx):
    stmt = '''
    SELECT MAX(ep)
    FROM DOCMETA JOIN BIZMETA
    ON DOCMETA.docid = BIZMETA.docid;
    '''.strip()
    df = pd.read_sql(stmt, cnx)
    return df['max'].values[0]


def kv_pairs(cnx, tbl, kcol, vcol):
    d = {}
    stmt = 'SELECT {kcol}, {vcol} FROM {tbl};'
    stmt = stmt.format(kcol=kcol, vcol=vcol, tbl=tbl)
    df = pd.read_sql(stmt, cnx)
    for i in range(len(df)):
        d[df[kcol].values[i]] = df[vcol].values[i]
    return d