import pandas as pd
import lib.db.futr_cnx
import lib.db.read.base


def subreddit_subcnts_stmt(wc='%s'):
    stmt = '''
    SELECT ep, val as subcnt
    FROM subreddit_snapshots AS ss
    JOIN subreddit_snapshots_var AS ssv ON ss.varid = ssv.id
    JOIN subreddit AS sr ON sr.id = ss.srid
    WHERE
    ssv.name = 'SUBS' AND 
    sr.name = {wc} AND
    {wc} <= ss.ep AND
    ss.ep <= {wc}; 
    '''.strip()
    return stmt.format(wc=wc)


def subreddit_subcnts(srname, minep=None, maxep=None, cnx=None, wc='%s'):
    if cnx is None:
        cnx = lib.db.futr_cnx.get_cnx()
    if minep is None:
        minep = 0
    if maxep is None:
        maxep = 9999999999
    stmt = subreddit_subcnts_stmt(wc)
    params = (srname, minep, maxep,)
    df = pd.read_sql(stmt, cnx, params=params)
    df['subcnt'] = [int(i) for i in df['subcnt'].values]
    return df


