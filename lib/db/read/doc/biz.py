import pandas as pd
from lib.db import futr_cnx


def biz_opids(cnx=None):
	if cnx is None:
		cnx = futr_cnx.get_cnx()
    df = pd.read_sql(
        'SELECT DISTINCT pid FROM BIZMETA WHERE opid = pid;', cnx
    )
    return set(df['pid'].values)