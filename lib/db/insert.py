import sqlite3
import pandas as pd


def mogrify_insert_stmt_from_rows(cnx, tbl, rows, cols=None, on_conflict=None):
	cur = cnx.cursor()
	if cols is None:
		stmt = 'INSERT INTO {tbl} VALUES'.format(tbl=tbl)
	else:
		if len(cols) != len(rows[0]):
			raise IndexError(
				'The number of specified columns is not equal to row length.'
			)
		stmt = 'INSERT INTO {tbl} (' + ','.join(cols) + ') VALUES'.strip()
		stmt = stmt.format(tbl=tbl)
	fmt = '(' + ','.join(['%s'] * len(rows[0])) + ')'
	stmt += ','.join(cur.mogrify(fmt, r).decode('utf8') for r in rows)
	if on_conflict != None:
		stmt += ' ON CONFLICT ' + str(on_conflict)
	stmt += ';'
	cur.close()
	return stmt


def _mk_general_insert_rows_stmt(
	tbl,
	on_conflict,
	wc='%s',
	numcols=None,
	cols=None
):
	if cols is not None:
		numcols = len(cols)
	stmt = 'INSERT {conf} INTO {tbl} {cols} VALUES {vals};'
	if cols is None:
		cols_str = ''
	else:
		cols_str = '(' + ','.join(cols) + ')'
	vals_str = '(' + ','.join([wc] * numcols) + ')'
	if on_conflict is not None:
		conf_str = ' OR ' + str(on_conflict)
	else:
		conf_str = ''
	stmt = stmt.format(
		conf=conf_str,
		tbl=tbl,
		wc=wc,
		cols=cols_str,
		vals=vals_str
	)
	return stmt

def postgres_insert_rows(cnx, tbl, rows, cols=None, on_conflict=None):
	stmt = mogrify_insert_stmt_from_rows(
		cnx, tbl, rows, cols, on_conflict
	)
	cur = cnx.cursor()
	cur.execute(stmt)
	cur.close()
	cnx.commit()


def sqlite_insert_rows(cnx, tbl, rows, wc, on_conflict=None, cols=None):
	stmt = _mk_general_insert_rows_stmt(
		tbl=tbl,
		on_conflict=on_conflict,
		wc=wc,
		numcols=len(rows[0]),
		cols=cols
	)
	cur = cnx.cursor()
	cur.executemany(stmt, [tuple(r) for r in rows])
	cur.close()
	cnx.commit()


def insert_rows(cnx, tbl, rows, on_conflict=None):
	if hasattr(cnx, '__class__') and cnx.__class__ == sqlite3.Connection:
		# Is it a sqlite connection?
		sqlite_insert_rows(cnx, tbl, rows, wc='?', on_conflict=on_conflict)
	else:
		# If none of the above, assume a postgres connection.
		postgres_insert_rows(cnx, tbl, rows, on_conflict)
