def _exec_void(cnx, stmt):
    cur = cnx.cursor()
    cur.execute(stmt)
    cur.close()
    cnx.commit()


def _mk_string_id_tbl(cnx, tblname, pktype, vtype):
    stmt = '''
    CREATE TABLE {tblname} (
        id {pktype} PRIMARY KEY,
        name {vtype},
        UNIQUE(id, name)
    );
    '''.strip()
    stmt = stmt.format(
        tblname=tblname,
        pktype=pktype,
        vtype=vtype,
    )
    _exec_void(cnx, stmt)