import lib.db.read.quote
import lib.db.futr_cnx
import lib.trade.binance


def rows_from_candle_list(asset, exasset, cl, cnx):
    '''
    :type asset: str
    :type exasset: str
    :type cl: list<obj>
    :param cl: candle list returned by binance api
    :type cnx: dbapi2
    '''
    if not lib.db.read.quote.IdMap.is_init():
        lib.db.read.quote.IdMap.init_maps(cnx)

    asset_id = int(lib.db.read.quote.IdMap.ID_ON_ASSET[asset])
    exasset_id = int(lib.db.read.quote.IdMap.ID_ON_ASSET[exasset])
    quotesrc_id = int(lib.db.read.quote.IdMap.ID_ON_QUOTESRC['binance'])

    # open time
    ep0 = int(cl[0] / 1000)
    # close time
    ep1 = int(cl[6] / 1000)
    # close price
    p_close = float(cl[4])
    # exasset volume
    vol = float(cl[7])

    '''
    Volume: 0
    Close:6
    '''
    return [
        (asset_id, exasset_id, ep0, ep1, quotesrc_id, 0, vol),
        (asset_id, exasset_id, ep0, ep1, quotesrc_id, 6, p_close),
    ]


def rows_from_candle_lil(asset, exasset, clil, cnx=None):
    if cnx is None:
        cnx = lib.db.futr_cnx.get_cnx()
    rows = []
    for cl in clil:
        rows.extend(rows_from_candle_list(asset, exasset, cl, cnx))
    return rows