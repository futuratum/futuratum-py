from lib.db import insert, futr_cnx



def insert_rows(rows, cnx=None):
    if cnx is None:
        cnx = futr_cnx.get_cnx()
    cols = [
        'asset_id', 'exasset_id', 'ep0', 'ep1',
        'quotesrc_id', 'quotetype_id', 'val'
    ]
    insert.postgres_insert_rows(
        cnx, 'asset_quote', rows, cols, on_conflict='DO NOTHING'
    )