import os
import pandas as pd
from lib.saxutil import date_util
from lib.db import insert, futr_cnx
from lib.db.read import base, quote


def rows_from_candle_dict(
    candle_dict,
    quote_type_map,
    asset_id,
    exasset_id,
    quotesrc_id,
    dtkey,
    epgap
):
    rows = []
    for k in quote_type_map.keys():
        ep0 = date_util.convert_to_timestamp(
            pd.to_datetime(candle_dict[dtkey])
        )
        ep1 = ep0 + epgap
        row = (
            int(asset_id),
            int(exasset_id),
            int(ep0),
            int(ep1),
            int(quotesrc_id),
            int(quote_type_map[k]),
            candle_dict[k],
        )
        rows.append(row)
    return rows


def rows_from_market_summary_dict(qd, cnx=None):
    """
    Generates SQL rows from a market summary dictionary.
    """
    quote_type_map = {
        'Last':8,
        'OpenBuyOrders':9,
        'OpenSellOrders':10
    }
    if not quote.IdMap.is_init():
        quote.IdMap.init_maps(cnx)
    ep0 = date_util.convert_to_timestamp(pd.to_datetime(qd['TimeStamp']))
    # Market summaries provide a snapshot of a single moment time.
    # Because the measurement is only relevant for a single moment,
    # the start and end epoch should be the same.
    ep1 = int(ep0)
    exasset, asset = qd['MarketName'].split('-')
    asset_id = quote.IdMap.ID_ON_ASSET[asset]
    exasset_id = quote.IdMap.ID_ON_ASSET[exasset]
    quotesrc_id = quote.IdMap.ID_ON_QUOTESRC['bittrex']
    rows = []
    for quotetype, quotetype_id in quote_type_map.items():
        try:
            val = float(qd[quotetype])
        except:
            continue
        row = (
            int(asset_id),
            int(exasset_id),
            int(ep0),
            int(ep1),
            int(quotesrc_id),
            int(quotetype_id),
            val,
        )
        rows.append(row)
    return rows


class BittrexCandleRowGenr(object):
    def __init__(
        self,
        asset_id,
        exasset_id,
        quotesrc_id,
        epgap
    ):
        self.asset_id = asset_id
        self.exasset_id = exasset_id
        self.quotesrc_id = quotesrc_id
        self.quote_type_map={'O':5, 'V':1, 'BV':0, 'C':6, 'L':4, 'H':3}
        self.epgap = epgap

    @classmethod
    def from_cnx(cls, cnx, asset, exasset, quotesrc, epgap):
        quote.IdMap.init_maps(cnx)
        asset_id = quote.IdMap.ID_ON_ASSET[asset]
        exasset_id = quote.IdMap.ID_ON_ASSET[exasset]
        quotesrc_id = quote.IdMap.ID_ON_QUOTESRC[quotesrc]
        return BittrexCandleRowGenr(asset_id, exasset_id, quotesrc_id, epgap)

    def extract_rows(self, candle_dicts):
        rows = []
        for cd in candle_dicts:
            rows.extend(
                rows_from_candle_dict(
                    candle_dict=cd,
                    quote_type_map=self.quote_type_map,
                    asset_id=self.asset_id,
                    exasset_id=self.exasset_id,
                    quotesrc_id=self.quotesrc_id,
                    dtkey='T',
                    epgap=self.epgap
                )
            )
        return rows


def quote_rows_from_market_summary(mkt_summary, cnx=None):
    '''
    :param mkt_summary: market summary dictionary
    '''
    if not quote.IdMap.is_init():
        quote.IdMap.init_maps(cnx)
    exasset, asset = mkt_summary['MarketName'].split('-')
    asset_id = quote.IdMap.ID_ON_ASSET.get(asset)
    exasset_id = quote.IdMap.ID_ON_ASSET.get(exasset)
    if asset_id is None or exasset_id is None:
        return []
    return rows_from_market_summary_dict(mkt_summary)


def quote_rows_from_market_summaries(mkt_summaries, cnx=None):
    rows = []
    for ms in mkt_summaries:
        rows.extend(quote_rows_from_market_summary(ms, cnx))
    return rows


