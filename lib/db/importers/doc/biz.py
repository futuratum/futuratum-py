import json
from collections import Counter
import pandas as pd
import nltk
from lib.db import insert
from lib.db.read import base as readbase
from lib.db.importers.doc import base as docbase
from lib.saxutil.txt_proc import ngram_matcher  
from lib.manage.text import doc_biz


def mk_ngram_matcher(termdta_on_term):
	matcher = ngram_matcher.Matcher()
	for term_tuple in termdta_on_term:
		matcher.add_ngram( term_tuple )
	return matcher


class BizMetaDbImporter(docbase.DocDbImporter):
	def __init__(self, cnx):
		super(BizMetaDbImporter, self).__init__(cnx, 0)
		self.bizmetarows = []

	def _add_bizmeta(self, meta):
		self.bizmetarows.append([meta.pid, meta.docid, meta.opid])

	def run_nextline(self):
		self.nextline_err_check()
		l = next(self.fh)
		meta = doc_biz.BizMeta.loads(l)
		self._add_docmeta(meta)
		self._add_bizmeta(meta)
		assert(len(self.bizmetarows) == len(self.docmetarows))

	def import_content(self):
		super(BizMetaDbImporter, self).import_content()
		collisions = readbase.col_val_collisions(
			self.cnx, 'bizmeta', 'pid', [r[0] for r in self.bizmetarows]
		)
		bizmetarows_filt = [
			r for r in self.docmetarows if r[0] not in collisions
		]
		bizmetarows_filt = [
			r for r in self.bizmetarows if r[1] not in collisions
		]
		insert.insert_rows(self.cnx, 'bizmeta', bizmetarows_filt)

	def clear_content(self):
		super(BizMetaDbImporter, self).clear_content()
		self.bizmetarows = []


