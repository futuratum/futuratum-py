from collections import Counter
import nltk
from lib.manage.text import doc_base
from lib.db import insert
from lib.db.read import base as readbase


def docterm_rows_from_dict(d, ng_matcher, termdta_on_term, textkey='text'):
	rows = []
	matches = ng_matcher.find_matches(nltk.word_tokenize(text))
	cntr = Counter()
	for m in matches:
		cntr[termdta_on_term[tuple(m)][0]] += 1
	for termid, cnt in term_cntr.items():
		rows.append([int(d['docid']), int(d['termid']), int(d['cnt'])])
	return rows


class DocDbImporter(object):
	def __init__(self, cnx, doctype_id):
		self.cnx = cnx
		self.fileid = None
		self.fh = None
		self.docmetarows = []
		self.doctype_id = doctype_id

	def doc_cnt(self):
		'''
		:rtype: int
		'''
		return len(self.docmetarows)

	def setfile(self, fileid, fp):
		self.fh = open(fp)
		self.fileid = fileid

	def nextline_err_check(self):
		if self.fh is None or self.fileid is None:
			raise ValueError('fp or fileid is None. Be sure to call setfile.')

	def _add_docmeta(self, meta):
		self.docmetarows.append(
			[meta.docid, meta.ep, self.fileid, self.doctype_id]
		)

	def run_nextline(self):
		self.nextline_err_check()
		l = next(self.fh)
		meta = doc_base.DocMeta.loads(l, self.doctype_id)
		self._add_docmeta(meta)

	def import_content(self):
		collisions = readbase.col_val_collisions(
			self.cnx, 'docmeta', 'docid', [r[0] for r in self.docmetarows]
		)
		docmetarows_filt = [
			r for r in self.docmetarows if r[0] not in collisions
		]
		insert.insert_rows(self.cnx, 'docmeta', docmetarows_filt)

	def clear_content(self):
		self.docmetarows = []