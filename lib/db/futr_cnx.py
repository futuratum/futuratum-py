import os
import psycopg2


def get_cnx():
	return psycopg2.connect(
		user=os.environ['FUTR_DB_USER'],
		dbname=os.environ['FUTR_DB_NAME'],
		host=os.environ['FUTR_DB_HOST'],
		port=os.environ['FUTR_DB_PORT'],
		password=os.environ['FUTR_DB_PASS']
	)