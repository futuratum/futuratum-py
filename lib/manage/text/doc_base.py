import uuid
import json


class DocMeta(object):
	def __init__(self, docid, ep, doctype_id):
		self.docid = docid
		self.ep = ep
		self.doctype_id = doctype_id

	def __str__(self):
		return str( (self.docid, self.ep, self.doctype_id) )

	def __repr__(self):
		return self.__str__()

	def as_json_dict(self):
		return {
			'docid':self.docid,
			'ep':self.ep,
			'doctype_id':self.doctype_id
		}

	@classmethod
	def fromdict(cls, d, doctype_id):
		return DocMeta(
			docid=int(d['docid']),
			ep=int(d['ep']),
			doctype_id=doctype_id
		)


	@classmethod
	def loads(cls, dta, doctype_id):
		d = json.loads(dta)
		return DocMeta.fromdict(d, doctype_id)
