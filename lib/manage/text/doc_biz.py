import json
from lib.manage.text import doc_base


class BizMeta(doc_base.DocMeta):
    def __init__(self, docid, ep, pid, opid, bk_ids):
        super(BizMeta, self).__init__(docid, ep, 0)
        self.pid = pid
        self.opid = opid
        self.bk_ids = bk_ids

    def __str__(self):
        return str((self.docid, self.ep, self.pid, self.opid))

    def __repr__(self):
        return self.__str__()

    def as_json_dict(self):
        d = super(BizMeta, self).as_json_dict()
        d['pid'] = self.pid
        d['opid'] = self.opid
        d['bk_ids'] = list(self.bk_ids)
        return d

    @classmethod
    def dumps(cls, meta):
        return json.dumps(meta.as_json_dict())

    @classmethod
    def loads(cls, dta):
        d = json.loads(dta)
        return BizMeta(
            docid=int(d['docid']),
            pid=int(d['pid']),
            opid=int(d['opid']),
            ep=int(d['ep']),
            bk_ids=set([int(i) for i in d['bk_ids']]),
        )


class BizPost(BizMeta):
    def __init__(self, docid, ep, pid, opid, bk_ids, text):
        super(BizPost, self).__init__(docid, ep, pid, opid, bk_ids)
        self.text = text

    def as_json_dict(self):
        d = super(BizPost, self).as_json_dict()

    @classmethod
    def dumps(cls, post):
        return json.dumps(post.as_json_dict())

    @classmethod
    def loads(cls, dta):
        d = json.loads(dta)
        return BizPost(
            docid=int(d['docid']),
            pid=int(d['pid']),
            opid=int(d['opid']),
            ep=int(d['ep']),
            bk_ids=set([int(i) for i in d['bk_ids']]),
            text=dta['text']
        )

