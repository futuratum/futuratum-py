import os
import pandas as pd
from lib.saxutil import date_util


class QuoteCollectionTimeData(object):
    def __init__(self, minep, maxep, epgap=None):
        self.minep = minep
        self.maxep = maxep
        # epgap should be non if and only if the time period
        # gaps in the input file are uneven.
        self.epgap = epgap

    def as_tuple(self):
        return (self.minep, self.maxep, self.epgap,)

    def __str__(self):
        return str( self.as_tuple() )

    def __repr__(self):
        return self.__str__()

    def mk_filename(self, asset, exasset):
        fn = 'bittrex.candle.{asset}.{exasset}.{epgap}.{minep}.{maxep}.json'
        return fn.format(
            asset=asset,
            exasset=exasset,
            epgap=str(self.epgap),
            minep=str(self.minep),
            maxep=str(self.maxep),
        )


def time_data_from_candles(candles, epgap):
    minep = None
    maxep = None
    for i in range(len(candles)):
        ep = date_util.convert_to_timestamp(pd.to_datetime(candles[i]['T']))
        if minep is None or minep > ep:
            minep = int(ep)
        if maxep is None or maxep < ep:
            maxep = int(ep)
    return QuoteCollectionTimeData(minep, maxep, epgap)



