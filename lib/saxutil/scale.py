import math
import datetime
import numpy as np


def logistic_scale(val, coef):
    '''
    :type coef: float
    :param coef: Controls how values are scaled. When "coef" is high,
        relatively large value for "val" are required to get an output
        that approaches 1.0.
    :type val: float or numpy array of floating point numbers
    :rtype: type(val)
    '''
    if hasattr(val, 'append'):
        val = np.array(val)
    return 1.0 / ( 1.0 + np.exp(-(val - coef)) )


class LogisticScaler(object):
    def __init__(self, coef):
        self.coef = coef

    def run(self, val):
        return logistic_scale(val, self.coef)