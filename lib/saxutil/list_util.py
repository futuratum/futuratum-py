def detect_dup_indices(sorted_arr):
    dup_indices = set()
    for i in range(1, len(sorted_arr)):
        if sorted_arr[i] == sorted_arr[i - 1]:
            dup_indices.add(i - 1)
            dup_indices.add(i)
    return sorted(list(dup_indices))


def detect_dup_vals(arr):
    arr.sort()
    indices = detect_dup_indices(arr)
    return set([arr[i] for i in indices])


def are_all_same_len(lists):
    lens = set([len(l) for l in lists])
    if len(lens) > 1:
        return False
    return True


def raise_err_on_diff_len(lists):
    if are_all_same_len(lists):
        return
    outstr = ''
    for l in lists:
        outstr += str(l) + '\n'
    outstr = outstr.strip()
    raise IndexError('Diff len lists error\n' + str(outstr))


def bool_func_match_indices(inputlist, boolfunc):
    indices = []
    for i in range(len(inputlist)):
        if boolfunc(inputlist[i]):
            indices.append(i)
    return indices


def split_list_on_indices(inputlist, indices):
    raise_err_on_diff_len([indices, set(indices)])
    indices = sorted(list(indices))
    sublists = []
    beg = 0
    for i in indices:
        sublists.append(inputlist[beg:i])
        beg = int(i)
    sublists.append(inputlist[beg:])
    return sublists
