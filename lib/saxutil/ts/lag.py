import pandas as pd


def _max_lag_from_lag_dict(lags_on_col):
    lags = []
    for col, clags in lags_on_col.items():
        lags.extend(clags)
    return max(lags)


def mk_lag_df_non_sequential(df, lags_on_col):
    col_hdrs = [df.index.name]
    for col in lags_on_col.keys():
        lags = lags_on_col[col]
        if len(set(lags)) != len(lags):
            raise ValueError('lags for ' + str(col) + ' are not distinct.')
        for i in reversed(range(len(lags))):
            col_hdrs.append(str(col) + '__' + str(lags[i]))

    minrow = _max_lag_from_lag_dict(lags_on_col)
    rows = []
    for i in range(minrow, len(df)):
        row = []
        for col in lags_on_col.keys():
            row.append(df.index[i])
            lags = lags_on_col[col]
            for lag in reversed(lags):
                assert(i - lag >= 0)
                row.append( df[col].values[i-lag] )
        rows.append(row)
    rdf = pd.DataFrame(rows, columns=col_hdrs)
    rdf.index = rdf[df.index.name]
    del rdf[df.index.name]
    return rdf


#def mk_lag_df(df, lag_on_col):