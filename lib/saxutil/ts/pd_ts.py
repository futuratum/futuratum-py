import numpy as np
import pandas as pd
from lib.saxutil.ts import second_units


def time_delta_from_seconds(seconds):
    if seconds % second_units.HOUR == 0:
        return pd.Timedelta(hours=seconds / second_units.HOUR)
    elif seconds % second_units.MINUTE == 0:
        return pd.Timedelta(minutes=seconds / second_units.MINUTE)
    return pd.Timedelta(seconds=seconds)



def get_prv_pds_dta(
    df, ref_dt, lookback, period_seconds, offset=0, skipna=False
):
    '''
    Retrieve periods exactly matching the "ref_dt" - (period_seconds * i)"
    for 1 <= i <= lookback.

    For example, if the desired output is the last 3
    15 minute intervals prior to 03/20/2017 01:00:00, the input variables
    should be set as follows:
        - df = a pandas DataFrame with a Datetime index.
        - ref_dt = datetime.datetime(2017, 3, 20, 0, 0, 0)
        - lookback = 3
        - period_seconds= 15 * 60
    In this case, the method will output a dataframe that only has information
    for the following datetimes:
        - 03/20/2017 00:15:00
        - 03/20/2017 00:30:00
        - 03/20/2017 00:45:00

    This is useful when information exists between the time periods you want
    to extract. For time periods in contiguous rows, it's usuaully better to
    leverage the "shift" instance method for DataFrames.

    :type df: pandas.DataFrame
    :param df: DataFrame with a Datetime index.
    :type ref_dt: datetime.datetime
    :type lookback: int
    :type offset: int
    :type skipna: bool
    :param skipna: skip over null rows and take the "lookback" closest
        periods that match.
    :rtype: pandas.DataFrame
    '''
    indexvals = set(df.index)
    min_index = min(df.index)

    prv_dts = []
    i = 1
    while i <= lookback:
        seconds = (period_seconds * i) + (period_seconds * offset)
        delta = time_delta_from_seconds(seconds)
        prv_dt = ref_dt - delta

        if prv_dt < min_index:
            break

        if prv_dt not in indexvals and skipna:
            lookback += 1
            i += 1
            continue
        prv_dts.append(prv_dt)
        i += 1
    bools = df.index.isin(prv_dts)
    if len(set(bools)) == 1 and bools[0] == False and hasattr(df, 'columns'):
        return pd.DataFrame([], columns=df.columns)
    elif len(set(bools)) == 1 and bools[0] == False:
        return []
    return df.loc[prv_dts].sort_index()


class PrvPdsFetcher(object):
    def __init__(self, srs, lookback, period_seconds, skipna=False):
        self.srs = srs
        self.lookback = lookback
        self.period_seconds = period_seconds
        self.skipna = skipna

    def get_prv_pds_dta(self, ref_dt, offset=0):
        return get_prv_pds_dta(
            df=self.srs,
            ref_dt=ref_dt,
            lookback=self.lookback,
            period_seconds=self.period_seconds,
            offset=offset,
            skipna=self.skipna,
        )


def stdv_dists(df, col):
    return (df[col] - np.mean(df[col])) / np.std(df[col])


def get_weekday_aggs(df, col, aggs, names):
    assert(len(aggs) == len(names))
    ddf = df.groupby(df.index.date).sum()
    ddf.index = pd.to_datetime(ddf.index)
    grps = ddf.groupby(ddf.index.weekday)
    res = []
    for weekday, wdf in grps:
        cur_res = [weekday]
        for agg in aggs:
            cur_res.append( agg(wdf[col]) )
        res.append(cur_res)
    return pd.DataFrame(res, columns=['weekday'] + names)


def outliers(df, col, min_abs_dist):
    dist_srs = stdv_dists(df, col)
    return dist_srs[abs(dist_srs) >= min_abs_dist]


def append_lag_col(df, lag, col, new_col_name=None):
    if new_col_name is None:
        new_col_name = str(col) + '__' + str(lag)
    srs = df[col].shift(lag)
    df[new_col_name] = srs

