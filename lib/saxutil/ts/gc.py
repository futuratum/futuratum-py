import numpy as np
from scipy import stats
from sklearn.linear_model import LinearRegression, Lasso, Ridge
from sklearn.base import clone
from sklearn.svm import SVR
from sklearn.model_selection import cross_val_score
from lib.saxutil.ts import lagdta
from lib.saxutil.regression_util import ssr, abs_err

import warnings

warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")


def ftest_wald(n, rss1, rss2, p1, p2):
    '''
    Source: https://en.wikipedia.org/wiki/F-test

    :param n: number of observations
    :param rss1: residual sum of squares for the restricted mdl
    :param rss2: residual num of squares for the unrestricted mdl
    :param p1: number of parameters (including the intercept) for the
    restricted mdl
    :param p2: number of parameters (including the intercept) for the
    unrestricted mdl
    '''
    f = (
        ((rss1 - rss2) / float(p2 - p1)) /
        (rss2 / float(n - p2))
    )
    # http://www.slideshare.net/ThomasReader/granger-causality-testing
    # Use exampes on pages 6 and 7 to verify that p value is equivalent
    # given the same f-test number.
    p = stats.f.sf(f, p2 - p1, n - 1 - p2)
    return f, p


def _infer_beg_end(X, b=None, e=None):
    '''
    :type X: numpy.ndarray
    :type b: int
    :type e: int
    '''
    if b is None:
        b = 0
    if e is None:
        e = X.shape[0]

    if e < 0 or b < 0:
        raise ValueError('begin or end index is negative')
    if b > X.shape[0] or e > X.shape[0]:
        raise ValueError(
            'begin or end index is greater then the number of mtx rows'
        )
    return b, e


def _region_preds(X, mdl, b=None, e=None):
    b, e = _infer_beg_end(X, b, e)
    return mdl.predict(X)[b:e]


def _row_preds(X, mdl, I):
    '''
    :type X: np.ndarray
    :type mdl: sklearn regression model
    :type I: list<int>
    '''
    preds = mdl.predict(X)
    return np.array([preds[i] for i in I])


def _check_bounds(b, e, rows):
    if {b, e} != {None} and rows is not None:
        raise ValueError('b and e should be None if "rows" is specified')


def gen_index_arr(gt, index_vals):
    '''
    :type gt: GrangerTest
    :type index_vals: list<obj>
    :rtype: list<obj>

    Aligns GrangerTest w/ dataframe index values
    '''
    return index_vals[gt.maxlag() - 1:-gt.ahead]


def gen_index_map(gt, index_vals):
    arr = gen_index_arr(gt, index_vals)
    return {arr[i]: i for i in range(len(arr))}


class GrangerTest(object):
    def __init__(
            self,
            df,
            depcol,
            lag=None,
            mdl=None,
            lag_on_col=None,
            basecol=None,
            ahead=1
    ):
        """
        Applies the causality granger test to the indicated dependent and independent columns of data
        in the source data frame provided.  Specifically, it answers the question of whether or not a
        simple autoregressive predictive model built on the dependent column of data
        can be improved by adding information to the model from the independent columns.

        :param df: Source data for the granger test
        :type df: pd.DataFrame
        :param depcol: the column of the dependant variable
        :type depcol: object
        :param lag: the number of previous periods to consider
        :type lag: int
        :param lag_on_col: use this instead of the lag parameter to indicate specifically
               which columns to include (both dependent and independent) and to associate
               possibly different lags with each column.  Must always contain the dependent column name.
        :type lag_on_col: {object, int}
        :param ahead: The time series data provided in the source data frame is converted into a predictive model
                      by iterating through the rows sequentially, creating a new sample row for the predictive model
                      at each iteration.  The actual value being predicted for each row comes from the dependent
                      column and is by default 1 row ahead of the iteration, which is the default value for this
                      parameter.  You can change this parameter to indicate that the actual value for each sample
                      row in the predictive model should come from a different number of rows ahead (any positive value).
                      Default is 1
        :type ahead: int
        """

        if mdl is None:
            self.unfit = LinearRegression()
            self.bmdl = LinearRegression()
            self.jmdl = LinearRegression()
        else:
            self.unfit = clone(mdl)
            self.bmdl = clone(mdl)
            self.jmdl = clone(mdl)

        self.init_vars(df, depcol, lag, lag_on_col, basecol, ahead)
        self.init_data()
        self.init_mdls()

    def init_vars(self, df, depcol, lag, lag_on_col, basecol, ahead):
        self.df = df

        if lag_on_col is None and lag is not None:
            self.lag_on_col = {col: lag for col in df.columns}
            if basecol != depcol and basecol is not None:
                del self.lag_on_col[depcol]
        else:
            self.lag_on_col = lag_on_col

        self.lag = lag
        self.depcol = depcol
        self.basecol = basecol
        if self.basecol is None:
            self.basecol = depcol
        self.ahead = ahead

    def __repr__(self):
        return "DF: {shape} [{minidx}] - [{maxidx}] {depcol}[+{ahead}] > {loc}".format(
            shape=self.df.shape,
            minidx=self.df.index.min(),
            maxidx=self.df.index.max(),
            depcol=self.depcol,
            ahead=self.ahead,
            loc=self.lags_on_col()
        )

    def lags_on_col(self):
        return lagdta.convert_to_non_seq_lags(self.lag_on_col)

    def init_data(self):
        lags_on_col = self.lags_on_col()

        base_gen = lagdta.LagDataGen(
            self.df,
            self.basecol,
            lags_on_col={self.basecol: lags_on_col[self.basecol]},
        )
        self.bX, self.b_hdrs = base_gen.mk_mtx(minrow=self.maxlag(), ahead=self.ahead)

        joint_gen = lagdta.LagDataGen(self.df, self.basecol, lags_on_col)
        self.jX, self.j_hdrs = joint_gen.mk_mtx(ahead=self.ahead)
        self.ix = gen_index_map(self, self.df.index)

        # figure out which rows from original df are being used
        self.df_row_start_idx = self.maxlag() - 1
        self.df_row_end_idx = self.df.shape[0] - (self.ahead)

    def get_data_frame(self):
        return self.df


    def model_index(self):
        """

        :return: The index of the rows of the data in the joint and
                 base models in terms of the index values of the
                 dataframe passed in to the test
        :rtype: pandas.Index
        """
        return self.df.index[self.df_row_start_idx:self.df_row_end_idx]

    def index_arr(self):
        '''
        At init, the dataframe index is placed into a dictionary.
        In this dictionary, Dataframe index values are mapped to matrix rows.
        This method generates a vector "arr" such that arr[r] is the dataframe
        index value at matrix row r
        '''
        if len(self.ix) == 0:
            return np.zeros(0, dtype=int)

        arr = [None] * len(self.ix)
        for ixval, r in self.ix.items():
            arr[r] = ixval
        return np.array(arr)

    def _index_diffs(self):
        '''
        Works only on an index with values that support the
        subtraction operator.
        '''
        ix_vec = self.index_arr()
        diffs = np.zeros(len(ix_vec) - 1)
        for i in range(1, len(ix_vec)):
            diffs[i - 1] = ix_vec[i] - ix_vec[i - 1]
        return diffs

    def index_interval_range(self):
        '''
        Works only on an index with values that support the
        subtraction operator.
        '''
        diffs = self._index_diffs()
        return min(diffs), max(diffs)

    def is_index_interval_consistent(self):
        '''
        Works only on an index with values that support the
        subtraction operator.
        '''
        mind, maxd = self.index_interval_range()
        if mind == maxd:
            return True
        return False

    def get_closest_ixval_row(self, ixval):
        '''
        finds the row number with index value closest to param ixval.
        only works with a numeric index

        :type ixval: obj
        :param ixval: object of the same type as the index values.
        ixval need not be in the index.
        :rtype: int
        '''
        ixvec = self.index_arr()
        row = np.abs(ixvec - ixval).argmin()
        return row, ixvec[row]

    def get_closest_preceding_ixval_row(self, ixval):
        ixvec = self.index_arr()
        row = np.abs(ixvec - ixval).argmin()
        if ixvec[row] > ixval and row != 0:
            return row - 1, ixvec[row - 1]
        return row, ixvec[row]

    def init_mdls(self):
        self.init_base_mdl()
        self.init_joint_mdl()

    def col_ix(self, col, lag):
        hdr = str(col) + '__' + str(lag)
        if hdr not in self.j_hdrs:
            raise ValueError('column / lag combination not found.')
        return self.j_hdrs.index(hdr)

    def lag_arr(self, col, lag):
        return np.array(self.jX[:, self.col_ix(col, lag)])

    def maxlag(self):
        lags_on_col = self.lags_on_col()
        return lagdta._max_lag_from_lag_dict(lags_on_col)

    # return max(self.lag_on_col.values())


    def init_base_mdl(self):
        # self.bmdl = self._unfitted_mdl()
        self.bmdl.fit(self.bX, self.actuals())

    def cols(self):
        return self.lag_on_col.keys()

    def _unfitted_mdl(self):
        return clone(self.unfit)

    def nobs(self):
        return len(self.df) - self.maxlag() - (self.ahead - 1)

    def init_joint_mdl(self):
        # self.jmdl = self._unfitted_mdl()
        self.jmdl.fit(self.jX, self.actuals())

    def _infer_beg_end(self, b=None, e=None):
        return _infer_beg_end(self.bX, b, e)

    def _seg_regions(self, k):
        '''
        :type k: int
        '''
        endpoints = [(self.jX.shape[0] / k) * i for i in range(k + 1)]
        endpoints[-1] = self.jX.shape[0]
        regions = []
        for i in range(1, len(endpoints)):
            regions.append((endpoints[i - 1], endpoints[i],))
        return regions

    """
    def window_stds(self, col, winsize):
        return num_arr.window_stds(self.lag_arr(col, 1), winsize)

    def std_spike_pts(self, col, winsize, minstd):
        I = []
        wstds = self.window_stds(col, winsize)
        for i in range(winsize, len(wstds)):
            if wstds[i] >= minstd:
                I.append(i)
        return I
    """

    def actuals(self, b=None, e=None, rows=None):
        '''
        :type b: int
        :type e: int
        :type rows: list<int>
        '''
        _check_bounds(b, e, rows)
        Y = self.df[self.depcol].values
        A = []
        for i in range(self.maxlag() + (self.ahead - 1), len(Y)):
            A.append(Y[i])

        if rows is None:
            b, e = self._infer_beg_end(b, e)
            return np.array(A[b:e])
        return np.array([A[i] for i in rows])

    def base_preds(self, b=None, e=None, rows=None):
        '''
        :type b: int
        :type e: int
        :type rows: list<int>
        '''
        _check_bounds(b, e, rows)
        if rows is None:
            return _region_preds(self.bX, self.bmdl, b, e)
        return _row_preds(self.bX, self.bmdl, rows)

    def joint_preds(self, b=None, e=None, rows=None):
        '''
        :type b: int
        :type e: int
        :type rows: list<int>
        '''
        _check_bounds(b, e, rows)
        if rows is None:
            return _region_preds(self.jX, self.jmdl, b, e)
        return _row_preds(self.jX, self.jmdl, rows)

    def index_range(self, ix_b, ix_e):
        row_b = self.ix[ix_b]
        row_e = self.ix[ix_e]
        if row_e < row_b:
            raise ValueError('ix_b row is after ix_e row')
        return row_b, row_e

    def eval_dta(self):
        R = self.base_preds()
        U = self.joint_preds()
        A = self.actuals()
        I = self.index_arr()
        return R, U, A, I

    def joint_ssr(self, b=None, e=None, rows=None):
        '''
        :type b: int
        :type e: int
        :type rows: list<int>
        '''
        return ssr(self.joint_preds(b, e, rows), self.actuals(b, e, rows))

    def base_ssr(self, b=None, e=None, rows=None):
        '''
        :type b: int
        :type e: int
        :type rows: list<int>
        '''
        return ssr(self.base_preds(b, e, rows), self.actuals(b, e, rows))

    def ssr_red(self, b=None, e=None, rows=None):
        '''
        :type b: int
        :type e: int
        :type rows: list<int>
        '''
        return 1.0 - (self.joint_ssr(b, e, rows) / self.base_ssr(b, e, rows))

    def region_ssr_reds(self, numregions):
        '''
        :type numregions: int
        '''
        reds = []
        regions = self._seg_regions(numregions)
        for b, e in regions:
            reds.append(self.ssr_red(b, e))
        return reds

    def ssr_red_skew(self, numregions):
        reds = self.region_ssr_reds(numregions)
        return stats.skew(reds)

    def joint_abs_err(self, b=None, e=None):
        '''
        :type b: int
        :type e: int
        '''
        return abs_err(self.joint_preds(b, e), self.actuals(b, e))

    def base_abs_err(self, b=None, e=None):
        '''
        :type b: int
        :type e: int
        '''
        return abs_err(self.base_preds(b, e), self.actuals(b, e))

    def ftest_wald(self):
        return ftest_wald(
            n=self.nobs(),
            rss1=self.base_ssr(),
            rss2=self.joint_ssr(),
            p1=self.bX.shape[1] + 1,
            p2=self.jX.shape[1] + 1
        )

    def b_corr(self):
        return stats.pearsonr(self.actuals(), self.base_preds())

    def j_corr(self):
        return stats.pearsonr(self.actuals(), self.joint_preds())

    def result_tuple(self):
        return (
            self.b_corr()[0],
            self.j_corr()[0],
            self.ftest_wald()[0],
            self.ftest_wald()[1],
        )

    def joint_cv(self, cv, scoring='neg_mean_squared_error'):
        '''
        :type cv: int
        :param cv: number of cross validation folds
        '''
        return cross_val_score(
            self._unfitted_mdl(), self.jX, self.actuals(), cv=cv, scoring=scoring
        )

    def base_cv(self, cv, scoring='neg_mean_squared_error'):
        '''
        :type cv: int
        :param cv: number of cross validation folds
        '''
        return cross_val_score(
            self._unfitted_mdl(), self.bX, self.actuals(), cv=cv, scoring=scoring
        )

    def cv_err_reduction(self, cv, scoring='neg_mean_squared_error'):
        mean_joint = np.mean(self.joint_cv(cv, scoring))
        mean_base = np.mean(self.base_cv(cv, scoring))
        return 1.0 - (mean_joint / mean_base)

    def joint_coefs(self):
        if hasattr(self.jmdl, 'sparse_coef_'):
            coefs = self.jmdl.sparse_coef_.toarray()[0]
        else:
            coefs = self.jmdl.coef_

        pairs = []
        for i in range(len(coefs)):
            pairs.append((self.j_hdrs[i], coefs[i],))
        return pairs

    def col_joint_coefs(self, col):
        '''
        Extract all coeficeints that correspond to dataframe column "col"

        :type col: obj
        :param col: dataframe column
        '''
        if col not in self.df:
            raise ValueError('col not in dataframe')

        coef_lag_pairs = []
        pairs = self.joint_coefs()
        for hdr, coef in pairs:
            coef_col, lag = hdr.split('__')
            if coef_col != str(col):
                continue
            coef_lag_pairs.append((coef, int(lag),))
        return coef_lag_pairs

    def get_prediction_dataframe(self):
        return GrangerPredictionDataFrame({
            "base": self.base_preds(),
            "joint": self.joint_preds(),
            "actual": self.actuals()},
            index=self.model_index())


class DiffAnalyzer(object):
    def __init__(self, gt, absolute=True):
        self.gt = gt
        self.absolute = absolute
        self.R = gt.base_preds()
        self.U = gt.joint_preds()
        self.A = gt.actuals()
        diffs = np.zeros(len(self.A))

        for i in range(len(gt.actuals())):
            diff = self.U[i] - self.R[i]
            if self.absolute:
                diff = abs(diff)
            diffs[i] = diff

        self.diff_argsort = list(diffs.argsort())
        self.diff_argsort.reverse()

        self.RS = []
        self.US = []
        self.AS = []
        for i in self.diff_argsort:
            self.RS.append(self.R[i])
            self.US.append(self.U[i])
            self.AS.append(self.A[i])

    def r_corr(self, k):
        return stats.pearsonr(self.RS[:k], self.AS[:k])

    def u_corr(self, k):
        return stats.pearsonr(self.US[:k], self.AS[:k])

    def corr_diff(self, k):
        return self.u_corr(k)[0] - self.r_corr(k)[0]

    def corr_diffs(self, b=None, e=None):
        if b is None:
            b = 2
        if e is None:
            e = len(self.AS)
        return [self.corr_diff(i) for i in range(b, e)]

    def r_ssr(self, k):
        return ssr(self.RS[:k], self.AS[:k])

    def u_ssr(self, k):
        return ssr(self.US[:k], self.AS[:k])

    def r_abs_err(self, k):
        return abs_err(self.RS[:k], self.AS[:k])

    def u_abs_err(self, k):
        return abs_err(self.US[:k], self.AS[:k])

    def ssr_diff(self, k):
        return self.r_ssr(k) - self.u_ssr(k)

    def mean_ssr_diff(self, k):
        return self.ssr_diff(k) / float(k)

    def u_ssr_pct(self, k):
        return self.u_ssr(k) / self.u_ssr(len(self.A))

    def u_abs_err_pct(self, k):
        return self.u_abs_err(k) / self.u_abs_err(len(self.A))

    def ftest_wald(self, k):
        return ftest_wald(
            n=k,
            rss1=self.r_ssr(k),
            rss2=self.u_ssr(k),
            p1=self.gt.bX.shape[1],
            p2=self.gt.jX.shape[1],
        )

    def diff(self, i):
        return self.US[i] - self.RS[i]

    def diffs(self, b, e):
        return [self.US[i] - self.RS[i] for i in range(b, e)]

    def diff_indices(self, k):
        I = []
        for i in range(k):
            I.append(self.diff_argsort[i])
        return sorted(I)

    def diff_ranges(self, k, mingap):
        I = self.diff_indices(k)
        E = []
        curevt = [I[0]]
        for i in range(1, len(I)):
            if I[i] - I[i - 1] < mingap:
                curevt.append(I[i])
            else:
                E.append((curevt[0], curevt[-1],))
                curevt = [I[i]]
        E.append((curevt[0], curevt[-1],))
        return E


import pandas as pd


class GrangerPredictionDataFrame(pd.DataFrame):
    @property
    def _constructor(self):
        return GrangerPredictionDataFrame

    def ssr_joint(self, index=None):
        df = self if index is None else self.loc[index, :]
        return ssr(df['joint'], df['actual'])

    def ssr_base(self, index=None):
        df = self if index is None else self.loc[index, :]
        return ssr(df['base'], df['actual'])

    def ssr_reduction(self, index=None):
        df = self if index is None else self.loc[index, :]
        actuals = df['actual']
        return 1 - (ssr(df['joint'], actuals) / ssr(df['base'], actuals))


class GrangerTestHoldout(GrangerTest):
    def __init__(self, df, depcol, lag=None, mdl=None, lag_on_col=None, basecol=None, ahead=1, holdout_after=None):
        self.full_df = df
        self.holdout_after = holdout_after

        if self.holdout_after and df.shape[0] == 0:
            raise ValueError("Can't perform granger test holdout on empty data frame")

        if self.holdout_after and df.index[0] >= holdout_after:
            raise ValueError("Holdout date is at or before first row of the data frame")

        if self.holdout_after and df.index[-1] <= holdout_after:
            raise ValueError("Holdout date is at or beyond last row of the data frame")

        if holdout_after:
            split_df = df[holdout_after:]
            self.tst_idx =  df.index.get_loc(split_df.index[1]) \
                if split_df.index[0] == holdout_after \
                else df.index.get_loc(split_df.index[0])
        else:
            self.tst_idx = df.shape[0]

        trn_df = df.ix[:self.tst_idx]

        super(GrangerTestHoldout, self).__init__(trn_df, depcol,
                                                 lag=lag, mdl=mdl,
                                                 lag_on_col=lag_on_col, basecol=basecol,
                                                 ahead=ahead)

    def init_data(self):
        super(GrangerTestHoldout, self).init_data()

        maxlag = lagdta._max_lag_from_lag_dict(self.lags_on_col())
        self.tst_df = self.full_df.ix[self.tst_idx - maxlag:]

        if self.df.shape[0] < (maxlag + self.ahead):
            raise ValueError("Training data set does not have at least {:d} rows (maxlag + ahead)".format(
                maxlag + self.ahead
            ))

        if self.holdout_after and self.tst_df.shape[0] < (maxlag + self.ahead):
            raise ValueError("Test data set does not have at least {:d} rows (maxlag + ahead)".format(
                maxlag + self.ahead
            ))

        lags_on_col = self.lags_on_col()
        base_gen = lagdta.LagDataGen(
            self.tst_df,
            self.basecol,
            lags_on_col={self.basecol: lags_on_col[self.basecol]}
        )
        self.tst_bX, self.tst_b_hdrs = base_gen.mk_mtx(minrow=self.maxlag(), ahead=self.ahead)
        joint_gen = lagdta.LagDataGen(self.tst_df, self.basecol, lags_on_col)
        self.tst_jX, self.tst_j_hdrs = joint_gen.mk_mtx(ahead=self.ahead)
        self.tst_ix = gen_index_map(self, self.tst_df.index)

        self.tst_df_row_start_idx = self.maxlag() - 1
        self.tst_df_row_end_idx = self.tst_df.shape[0] - (self.ahead)

    def tst_model_index(self):
        return self.tst_df.index[self.tst_df_row_start_idx:self.tst_df_row_end_idx]

    def tst_actuals(self):
        return self.tst_df[self.depcol].values[self.maxlag() + (self.ahead-1):]

    def tst_base_preds(self):
        return self.bmdl.predict(self.tst_bX)

    def tst_joint_preds(self):
        return self.jmdl.predict(self.tst_jX)

    def tst_base_ssr(self):
        return ssr(self.tst_base_preds(), self.tst_actuals())

    def tst_joint_ssr(self):
        return ssr(self.tst_joint_preds(), self.tst_actuals())

    def tst_nobs(self):
        return self.tst_df.shape[0] - self.maxlag() - (self.ahead - 1)

    def tst_ftest_wald(self):
        return ftest_wald(
            n=self.tst_nobs(),
            rss1=self.tst_base_ssr(),
            rss2=self.tst_joint_ssr(),
            p1=self.tst_bX.shape[1] + 1,
            p2=self.tst_jX.shape[1] + 1
        )

    def get_data_frame(self):
        return self.full_df

    def get_train_data_frame(self):
        return self.df

    def get_test_data_frame(self):
        return self.tst_df

    def get_tst_prediction_dataframe(self):
        return GrangerPredictionDataFrame({
            "base": self.tst_base_preds(),
            "joint": self.tst_joint_preds(),
            "actual": self.tst_actuals()},
            index=self.tst_model_index())


