import numpy as np
import pandas as pd


def _numcols_from_lag_dict(lags_on_col):
	c = 0
	for col, lags in lags_on_col.items():
		c += len(lags)
	return c


def _max_lag_from_lag_dict(lags_on_col):
	lags = []
	for col, clags in lags_on_col.items():
		lags.extend(clags)
	return max(lags)


def non_seq_lag_from_seq_lag(lag):
	return [i + 1 for i in range(lag)]


def non_seq_lags_from_range(l0, l1):
	if l0 > l1:
		raise ValueError('l0 must be less than l1')

	non_seq_lags = []
	for lag in range(l0, l1 + 1):
		lags = non_seq_lag_from_seq_lag(lag)
		non_seq_lags.append(tuple(sorted(lags)))
	return non_seq_lags

def _non_seq_lag_dict_from_seq_lags(lag_on_col):
	lags_on_col = {}
	for col, lag in lag_on_col.items():
		lags_on_col[col] = non_seq_lag_from_seq_lag(lag)
	return lags_on_col

def convert_to_non_seq_lags(lag_on_col):
	lags_on_col = {}
	for col, lag in lag_on_col.items():
		if (
			isinstance(lag, set) or
			isinstance(lag, list) or
			isinstance(lag, np.ndarray)
		):
			lags_on_col[col] = lag
		else:
			lags_on_col[col] = non_seq_lag_from_seq_lag(lag)
	return lags_on_col

def mk_lag_mtx_non_sequential(df, lags_on_col, cols=None, minrow=None, ahead=1):
	'''
	:type df: pandas.DataFrame
	:type lags_on_col: dict<obj, list<int>>
	:param lags_on_col: list of lags for each column
	:type cols: list<obj>
	:type minrow: int
	:rtype: numpy.ndarray, list<str>
	'''
	if cols is None:
		cols = lags_on_col.keys()

	for col in cols:
		if col not in df.columns:
			raise IndexError(
				'Column: ' + str(col) + ' not found in dataframe.'
			)

	if minrow is None:
		minrow = _max_lag_from_lag_dict(lags_on_col)

	mtx_col_hdrs = []
	for col in cols:
		lags = lags_on_col[col]
		if len(set(lags)) != len(lags):
			raise ValueError('lags for ' + str(col) + ' are not distinct.')
		for i in reversed(range(len(lags))):
			mtx_col_hdrs.append(str(col) + '__' + str(lags[i]))

	assert(len(mtx_col_hdrs) == _numcols_from_lag_dict(lags_on_col))

	rows = []
	for i in range(minrow, len(df) - (ahead-1)):
		row = []
		for col in cols:
			lags = lags_on_col[col]
			for lag in reversed(lags):
				assert(i - lag >= 0)
				row.append( df[col].values[i - lag] )
		rows.append(row)
	return np.array(rows), mtx_col_hdrs


def mk_lag_mtx(df, lag=None, lag_on_col=None, cols=None, minrow=None):
	'''
	:type df: pandas.DataFrame
	:type lag: int
	:type lag_on_col: dict<object, int>
	:type cols: list<str>
	'''
	if cols is None and lag_on_col is None:
		cols = df.columns
	elif cols is None and lag_on_col is not None:
		cols = lag_on_col.keys()

	for col in cols:
		if col not in df.columns:
			raise IndexError(
				'Column: ' + str(col) + ' not found in dataframe.'
			)

	if lag_on_col is None and lag is None:
		raise ValueError('Neither lag nor lag_on_col are initialized')

	if lag_on_col is None:
		lag_on_col = {}
		for col in cols:
			lag_on_col[col] = lag

	lags_on_col = _non_seq_lag_dict_from_seq_lags(lag_on_col)

	if minrow is None:
		minrow = max(lag_on_col.values())

	return mk_lag_mtx_non_sequential(df, lags_on_col, cols, minrow)


def mk_dep_var_arr(df, lag, ycol):
	Y = []
	all_y = df[ycol].values
	for i in range(lag, len(df)):
		Y.append(all_y[i])
	return np.array(Y)


class LagDataGen(object):
	def __init__(self, df, ycol, lags_on_col):
		self.df = df
		self.ycol = ycol
		self.lags_on_col = lags_on_col

	@classmethod
	def init_from_seq_lags(cls, df, ycol, lag=None, lag_on_col=None):
		if lag_on_col is None and lag is None:
			raise ValueError('lag_on_col or lag must be given.')
		if lag_on_col is None and lag is not None:
			lag_on_col = {col:lag for col in df.columns}
		lags_on_col = _non_seq_lag_dict_from_seq_lags(lag_on_col)
		return LagDataGen(df, ycol, lags_on_col)

	def xcols(self):
		cols = []
		for col in self.df.columns:
			if col in self.lags_on_col:
				cols.append(col)
		return cols

	def maxlag(self):
		return _max_lag_from_lag_dict(self.lags_on_col)

	def mk_mtx(self, minrow=None, ahead=1):
		return mk_lag_mtx_non_sequential(
			self.df, self.lags_on_col, cols=self.xcols(), minrow=minrow, ahead=ahead
		)

	def actuals(self):
		return mk_dep_var_arr(self.df, self.maxlag(), self.ycol)