import ics
import datetime


def _dts_from_cal(cal, attr):
    dts = []
    for e in cal.events:
        dt = datetime.datetime(
            year=getattr(e, attr).year,
            month=getattr(e, attr).month,
            day=getattr(e, attr).day,
            hour=getattr(e, attr).hour,
            minute=getattr(e, attr).minute,
            second=getattr(e, attr).second,
        )
        dts.append(dt)
    return dts


class IcsDtExtractor(object):
    def __init__(self):
        self.cals = []

    def addcal(self, cal):
        self.cals.append(cal)

    def _get_dts(self, attr):
        dts = []
        for cal in self.cals:
            dts.extend(_dts_from_cal(cal, attr))
        return dts

    def get_beg_dts(self):
        return self._get_dts('begin')

    def get_end_dts(self):
        return self._get_dts('end')
