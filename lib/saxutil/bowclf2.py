import os, json, pickle
from collections import Counter
import numpy as np
from scipy import sparse
from sklearn.preprocessing import normalize
from sklearn.feature_selection import SelectKBest, chi2
from lib.saxutil import ftmap, cntr_util, bow


class Clf(bow.BagOfWords):
    def __init__(
        self, mdl, fmap, numcols, col_on_ft, row_norma='l2'
    ):
        super(Clf, self).__init__(fmap, (10000, numcols,) )
        self.mdl = mdl
        self.row_norma = row_norma
        self.col_on_ft = col_on_ft

    def predict(self, return_proba=False):
        mtx = self.mk_mtx(self.row_norma)
        if not return_proba:
            return self.mdl.predict(mtx)
        pred_prob_mtx = [ [None, None] for i in range(self.num_docs()) ]
        prob_arrs = self.mdl.predict_proba(mtx)
        for i in range(len(prob_arrs)):
            max_prob = max(prob_arrs[i])
            pred = self.mdl.classes_[list(prob_arrs[i]).index(max_prob)]
            pred_prob_mtx[i][0] = pred
            pred_prob_mtx[i][1] = max_prob
        return np.array(pred_prob_mtx)

    def add_cntr_doc(self, cntr):
        fcntr = self.fmap.mk_ft_cntr(cntr, False)
        self._add_ft_cntr(fcntr)

    def add_obj_list_doc(self, obj_list):
        self.add_cntr_doc(Counter(obj_list))


def serialize_clf_to_dir(clf, dirpath):
    if os.path.exists(dirpath):
        raise IOError('dirpath:\t' + str(dirpath) + ' already exists')
    os.mkdir(dirpath)
    with open(os.path.join(dirpath, 'fmap.json'), 'w') as f:
        f.write(ftmap.dumps(clf.fmap))
    with open(os.path.join(dirpath, 'col_on_ft.json'), 'w') as f:
        f.write(json.dumps(clf.col_on_ft))
    with open(os.path.join(dirpath, 'mdl.pkl'), 'wb') as f:
        f.write(pickle.dumps(clf.mdl))
    with open(os.path.join(dirpath, 'meta.json'), 'w') as f:
        d = {'row_norma':clf.row_norma, 'numcols':clf.ftmtx.shape[1]}
        f.write(json.dumps(d))


def load_clf_from_dir(dirpath):
    with open(os.path.join(dirpath, 'fmap.json')) as f:
        fmap = ftmap.loads(f.read())
    with open(os.path.join(dirpath, 'col_on_ft.json')) as f:
        col_on_ft_loaded = json.loads(f.read())
        col_on_ft = {}
        for ft, col in col_on_ft_loaded.items():
            col_on_ft[int(ft)] = int(col)
    with open(os.path.join(dirpath, 'mdl.pkl'), 'rb') as f:
        mdl = pickle.loads(f.read())
    with open(os.path.join(dirpath, 'meta.json')) as f:
        meta = json.loads(f.read())

    if meta.get('numcols') is None:
        meta['numcols'] = max(col_on_ft.values()) + 1

    return Clf(
        mdl=mdl,
        fmap=fmap,
        numcols=meta['numcols'],
        col_on_ft=col_on_ft,
        row_norma=meta['row_norma'],
    )


class Trnr(bow.BagOfWords):
    def __init__(self, shape=(10000, 500000,)):
        fmap = ftmap.FeatureMap()
        super(Trnr, self).__init__(fmap, shape)
        self._lbls = [[None] for i in range(shape[1])]
        self.ft_bl = set()

    def _expandrows(self, expandby):
        super(Trnr, self)._expandrows(expandby)
        self._lbls = self._lbls + [None for i in range(expandby)]

    def lbls(self):
        return self._lbls[:self.lastrow]

    def add_cntr_doc(self, obj_cntr, lbl):
        self._maybe_expand_rows()
        self._lbls[self.lastrow] = lbl
        cntr = self.fmap.mk_ft_cntr(obj_cntr, add_on_absent=True)
        self._add_ft_cntr(cntr)

    def add_obj_list_doc(self, obj_list, lbl):
        self.add_cntr_doc(Counter(obj_list), lbl)

    def map_fts_to_cols(self):
        ft_wl = self.fmap.fts() - self.ft_bl
        super(Trnr, self).map_fts_to_cols(ft_wl)

    def score_fts(self, scorer=chi2, row_norma='l2'):
        self.map_fts_to_cols()
        ft_on_col = {f:c for c,f in self.col_on_ft.items()}
        mtx = self.mk_mtx(row_norma)
        self.col_on_ft = None
        sel = SelectKBest(scorer, k='all')
        sel.fit_transform(mtx, self.lbls())

        score_on_ft = {}
        for col in range(len(sel.scores_)):
            ft = ft_on_col[col]
            if np.isnan(sel.scores_[col]):
                continue
            score_on_ft[ft] = sel.scores_[col]
        return score_on_ft

    def lowest_score_pct_fts(self, rm_proportion, scorer=chi2):
        if not 0.0 <= rm_proportion <= 1.0:
            raise ValueError('rm_proportion must be between 0 and 1')
        tups = list(self.score_fts(scorer).items())
        tups.sort(key=lambda t:t[1])
        lows = set()
        rm_end = int(len(tups) * rm_proportion)
        for i in range(0, rm_end):
            lows.add(tups[i][0])
        return lows


    def to_clf(self, unfitmdl, row_norma='l2'):
        unfitmdl.fit(self.mk_mtx(row_norma), self.lbls())
        return Clf(
            unfitmdl, self.fmap, self.ftmtx.shape[1], self.col_on_ft, row_norma
        )

    def lbl_rows(self, lblset):
        return set(
            [i for i in range(self.num_docs()) if self._lbls[i] in lblset]
        )

    def ftmtx_lbl_resultant(self, lbl, docfreq=False):
        return self.ftmtx_resultant(docfreq, row_wl=self.lbl_rows({lbl}))