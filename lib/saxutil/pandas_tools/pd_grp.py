import pandas as pd


def are_group_index_attrs_consistent(grps, attrs):
    '''
    :type grps: list<tuple(tuple, pandas.DataFrame)>
    :param grps: The output of a pandas.DataFrame.groupby run transformed
        from a generator to a list.
    :type attrs: list<str>
    '''
    if len(grps) == 0:
        raise ValueError('len(grps) == 0, so there is nothing to check')
    if len(attrs) == 0:
        raise ValueError(
            'len(attrs) == 0, so there are no attributes to check.'
        )
    ref_srs_on_attr = {}
    for gid, gdf in grps:
        for attr in attrs:
            a_srs = getattr(gdf.index, attr)
            if not hasattr(a_srs, 'values'):
                a_srs = pd.Series(a_srs)
            r_srs = ref_srs_on_attr.get(attr)
            if r_srs is None:
                ref_srs_on_attr[attr] = a_srs
                continue
            if list(a_srs.values) != list(r_srs.values):
                return False
    return True


