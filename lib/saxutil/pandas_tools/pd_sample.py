from lib.saxutil.pandas_tools.transform import tr_match


def draw_match_rows(input_df, matchrules_on_attr, allow_partial_matches=False):
    df = input_df.copy(deep=True)
    df['MATCH'] = [False] * len(df)
    tr = tr_match.IndexAttrMatchFlagTransformer(
        flgcol='MATCH',
        matchrules_on_attr=matchrules_on_attr,
        flgval=True,
        allow_partial_matches=allow_partial_matches,
    )
    df = tr.run(df)
    return df[df['MATCH'] == True]