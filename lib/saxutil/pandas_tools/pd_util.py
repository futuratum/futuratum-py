import abc
import numpy as np
import pandas as pd
from lib.saxutil.pandas_tools.transform.tr_base import (
    DataFrameTransformer,
    GroupBasedDataFrameTransformer,
    run_transform_pipeline,
)

class ApplyFuncTransformer(DataFrameTransformer):
    def __init__(self, paramcols, otcol, f):
        '''
        :param paramcols: columns that are parameters to method "f"
        :param otcol: column where result is output to
        :type f: method 
        :param f: Method that uses the values for the columns specified in the
        "paramcols" parameter as arguments. Accepts an array of len(paramcols)
            as an argument.
        '''
        self.paramcols = paramcols
        self.otcol = otcol
        self.f = f

    def run(self, input_df):
        df = input_df.copy(deep=True)
        paramdf = df[self.paramcols]
        df[self.otcol] = paramdf.apply(self.f, axis=1)
        return df

class ApplyDivisionTransformer(ApplyFuncTransformer):
    def __init__(self, col0, col1, otcol, smooth_col1=0):
        def f(x):
            return x[0] / float(x[1] + smooth_col1)
        super(ApplyDivisionTransformer, self).__init__(
            paramcols=[col0, col1], otcol=otcol, f=f
        )

class ApplySumTransformer(ApplyFuncTransformer):
    def __init__(self, paramcols, otcol):
        super(ApplySumTransformer, self).__init__(paramcols, otcol, np.sum)


def rm_by_val(input_df, refcol, drop_vals):
    return input_df[~input_df[refcol].isin(drop_vals)]


class RmByValTransformer(DataFrameTransformer):
    def __init__(self, refcol, drop_vals):
        self.refcol = refcol
        self.drop_vals = set(drop_vals)

    def run(self, input_df):
        return rm_by_val(input_df, self.refcol, self.drop_vals)


class DropColTransformer(DataFrameTransformer):
    def __init__(self, dropcol):
        self.dropcol = dropcol

    def run(self, input_df):
        df = input_df.copy(deep=True)
        del df[self.dropcol]
        return df


class AddColTransformer(DataFrameTransformer):
    def __init__(self, addcol, default_val=None):
        self.addcol = addcol
        self.default_val = default_val

    def run(self, input_df):
        df = input_df.copy(deep=True)
        df[self.addcol] = [self.default_val for i in range(len(df))]
        return df


def shift_col(input_df, shiftby, refcol, otcol):
    df = input_df.copy(deep=True)
    srs = df[refcol].shift(shiftby)
    df[otcol] = srs
    return df


class ShiftColTransformer(DataFrameTransformer):
    def __init__(self, shiftby, refcol, otcol):
        self.shiftby = shiftby
        self.refcol = refcol
        self.otcol = otcol

    def run(self, input_df):
        return shift_col(input_df, self.shiftby, self.refcol, self.otcol)


class GroupBasedShiftColTransformer(GroupBasedDataFrameTransformer):
    def __init__(self, shiftby, refcol, otcol, groupby):
        '''
        :type shiftby: int
        :type refcol: obj
        :param refcol: input column identifier, usually a string
        '''
        super(GroupBasedShiftColTransformer, self).__init__(groupby)
        self.shiftby = shiftby
        self.refcol = refcol
        self.otcol = otcol

    def run(self, input_df):
        pdgroupby = self.get_pd_groupby(input_df)
        df = input_df.copy(deep=True)
        df[self.otcol] = [np.nan] * len(df)
        grps = list(df.groupby(pdgroupby))
        for i in range(len(grps)):
            grps[i] = list(grps[i])
            grps[i][1] = shift_col(
                grps[i][1], self.shiftby, self.refcol, self.otcol
            )
        df = pd.concat(g[1] for g in grps)
        return df

def mk_autoreg_shift_pipe(
    refcol,
    minshift=None,
    maxshift=None,
    sfx='_minus',
    shiftset=None,
    groupby=None
):
    if shiftset is None and {minshift, maxshift} != {None}:
        shiftset = set(range(minshift, maxshift + 1))
    pipe = []
    for i in shiftset:
        otcol = refcol + sfx + str(i)
        if groupby is None:
            tr = ShiftColTransformer(
                shiftby=i,
                refcol=refcol,
                otcol=otcol
            )
        else:
            tr = GroupBasedShiftColTransformer(
                shiftby=i,
                refcol=refcol,
                otcol=otcol,
                groupby=groupby
            )
        pipe.append(tr)
    return pipe


def add_aggregate_col(input_df, refcols, newcol, aggby):
    df = input_df.copy(deep=True)
    df[newcol] = df[refcols].apply(aggby, axis=1)
    return df


class AddAggColTransformer(DataFrameTransformer):
    def __init__(self, refcols, newcol, aggby):
        self.refcols = refcols
        self.newcol = newcol
        self.aggby = aggby

    def run(self, input_df):
        return add_aggregate_col(
            input_df, self.refcols, self.newcol, self.aggby
        )


def rolling_agg_by_grp(
    input_df,
    refcol,
    otcol,
    window,
    groupby,
    aggby=np.nanmean,
    min_periods=None,
):
    '''
    :type input_df: pandas.DataFrame
    :param input_df: DataFrame you want to transform
    :type refcol: obj
    :param refcol: column to perform aggregation on
    :type otcol: obj
    :param otcol: column to output aggregation result to
    :type window: int
    :param window: window size for aggregation
    :type groupby: list<pandas.core.indexes.base.Index>
    :param groupby: list of groupby arguments
    :type aggby: method
    :type min_periods: int
    :param min_periods: all rolling aggregates must be calculated with at
        least "min_periods" values. If this is None, "min_periods" is
        functionally equal to "window".
    '''
    df = input_df.copy(deep=True)
    grps = df.groupby(groupby)
    cur_srs = None
    srs = pd.Series(index=df.index)
    for gtup, gdf in grps:
        rolling = gdf[refcol].rolling(window=window, min_periods=min_periods)
        aggsrs = rolling.apply(aggby)
        srs = pd.concat([srs, aggsrs], join='inner').dropna()
    df[otcol] = srs
    return df.sort_index()


class RollingAggByGroupTransformer(GroupBasedDataFrameTransformer):
    def __init__(
        self,
        refcol,
        otcol,
        window,
        groupby,
        aggby=np.nanmean,
        min_periods=None
    ):
        super(RollingAggByGroupTransformer, self).__init__(groupby)
        self.refcol = refcol
        self.otcol = otcol
        self.window = window
        self.aggby = aggby
        self.min_periods = min_periods

    def run(self, input_df):
        return rolling_agg_by_grp(
            input_df=input_df,
            refcol=self.refcol,
            otcol=self.otcol,
            window=self.window,
            groupby=self.get_pd_groupby(input_df),
            aggby=self.aggby,
            min_periods=self.min_periods
        )


def grp_level_cumsums(input_df, refcol, groupby):
    '''
    :type input_df: pandas.DataFrame
    :type refcol: obj
    :type groupby: list<pandas.core.indexes.base.Index>
    :rtype: list<float>
    '''
    cumsums = []
    grps = input_df[refcol].groupby(groupby)
    for _, gdf in grps:
        gdf = gdf.sort_index()
        for i in range(len(gdf)):
            cumsums.append( sum(gdf[:i]) )
    return cumsums


class CumSumTransformer(GroupBasedDataFrameTransformer):
    def __init__(self, refcol, otcol, groupby):
        '''
        :type refcol: obj
        :type otcol: obj
        :type groupby: list<str>
        '''
        super(CumSumTransformer, self).__init__(groupby)
        self.refcol = refcol
        self.otcol = otcol

    def run(self, input_df):
        df = input_df.copy(deep=True)
        pdgroupby = self.get_pd_groupby(input_df)
        df[self.otcol] = grp_level_cumsums(input_df, self.refcol, pdgroupby)
        return df


class ExpectedGrpValTransfromer(GroupBasedDataFrameTransformer):
    def __init__(self, refcol, otcol, groupby, aggby=np.median):
        '''
        Example: groupby=['hour', 'minute']

        2015-01-01 01:15:00, 20
        2015-01-01 01:15:30, 0
        2015-01-02 01:15:00, 20
        2015-01-02 01:30:00, 30

        Two groups would be extracted here.

        Group 1: (hour=1, minute=15)
            2015-01-01 01:15:00, 20
            2015-01-01 01:15:30, 0
            2015-01-02 01:15:00, 20
        Group 2: (hour=1, minute=30)
            2015-01-02 01:30:00, 30

        The expected value of the first group would be 15.
        15 = median([20, 0, 20])

        The expected value of the second group would be 30
        30 = median([30])

        :type refcol: obj
        :type otcol: obj
        :type groupby: list<str>
        :param groubpy: List of grouping rules that correspond the dataframe
            index attributes. 

            For instance, if you want to compute expected
            values based on the same time on each day, you could set the
            "groupby" parameter equal to ['hour', 'minute']. This cause the
            transformer to compute expected values for each minute of the day.
        '''
        super(ExpectedGrpValTransfromer, self).__init__(groupby)
        self.refcol = refcol
        self.otcol = otcol
        self.aggby = aggby

    def run(self, input_df):
        pdgroupby = self.get_pd_groupby(input_df)
        df = input_df.copy(deep=True)
        df[self.otcol] = [np.nan] * len(df)
        grps = list(df.groupby(pdgroupby))
        for i in range(len(grps)):
            grps[i][1][self.otcol] = self.aggby(grps[i][1][self.refcol])
        df = pd.concat(g[1] for g in grps)
        return df.sort_index()


class TimeDeltaTransformer(DataFrameTransformer):
    def __init__(
        self, refcol, otcol, seconds, np_op=np.subtract
    ):
        '''
        :type refcol: obj
        :param refcol: input column identifier, usually a string
        :type otcol: obj
        :param otcol: output column identifier, usually a string
        :type seconds: int
        :param seconds: number of seconds
        :type np_op: method
        :param np_op: Numpy operator method. In general, you'll probably
            want to stick with np.subtract.
        '''
        self.refcol = refcol
        self.otcol = otcol
        self.seconds = seconds
        self.np_op = np_op

    def run(self, input_df):
        df = input_df.copy(deep=True)
        otcol = np.array([np.nan] * len(df))
        for i in range(len(df.index)):
            lastdate = self.np_op(
                df.index[i], pd.Timedelta(seconds=self.seconds)
            )
            if not lastdate in df.index:
                continue
            j = df.index.get_loc(lastdate)
            otcol[i] = df.iloc[j][self.refcol]
        df[self.otcol] = otcol
        return df


class NextMatchDeltaTransformer(DataFrameTransformer):
    '''
    Usage: Calculate the number of indices until the the value of
        "refcol" is in the "matchvals" set.
    '''
    def __init__(
        self,
        refcol,
        matchvals,
        otcol,
        maxdist,
        right=False,
    ):
        self.refcol = refcol
        self.matchvals = set(matchvals)
        self.maxdist = maxdist
        self.otcol = otcol
        self.right = right

    def _left_logic(self, df, matches):
        j = 0
        for ix in df.index:
            if j >= len(matches):
                break
            assert(matches[j] >= ix)
            setto = min(self.maxdist, matches[j] - ix)
            df.set_value(ix, self.otcol, setto)
            if ix == matches[j]:
                j += 1
        return df

    def _right_logic(self, df, matches):
        j = len(matches) - 1
        for ix in reversed(df.index):
            if j < 0:
                break
            assert(matches[j] <= ix)
            setto = min(self.maxdist, ix - matches[j])
            df.set_value(ix, self.otcol, setto)
            if ix == matches[j]:
                j -= 1
        return df

    def run(self, input_df):
        df = input_df.copy(deep=True)
        matches = df[df[self.refcol].isin(self.matchvals)].index
        df[self.otcol] = np.array([np.nan] * len(df))
        if self.right:
            df = self._right_logic(df, matches)
        else:
            df = self._left_logic(df, matches)
        df[self.otcol] = df[self.otcol].fillna(self.maxdist)
        return df


class TimeDeltaToSecondsTransformer(DataFrameTransformer):
    '''
    Transforms time delta values to seconds.
    '''
    def __init__(self, refcol, otcol):
        '''
        :type refcol: obj
        :param refcol: input column identifier, usually a string
        :type otcol: obj
        :param otcol: output column identifier, usually a string
        '''
        self.refcol = refcol
        self.otcol = otcol

    def run(self, input_df):
        df = input_df.copy(deep=True)
        df[self.otcol] = df[self.refcol] / np.timedelta64(1, 's')
        return df


def add_daytime_values(input_df, otcol, daytime_dict):
    '''
    :type input_df: pandas.DataFrame
    :type otcol: obj
    :param otcol: output column identifier, usually a string
    :type daytime_dict: dict<str, obj>

    Sometimes, it may be desirable to add a value associated with a time of
    day. (EG: 12:15, 23:30). Obviously, these values could be trivially
    held in a hashmap, but applying the values directly to a dataframe
    can make things more straight foward in for machine learning.
    '''
    df = input_df.copy(deep=True)
    # begin with an all NaN series.
    srs = pd.Series(index=df.index)
    grps = df.groupby([df.index.hour, df.index.minute, df.index.second])
    for gid, gdf in grps:
        hr, mn, sc = gid
        ixstr = ':'.join([str(i).zfill(2) for i in gid])
        val = daytime_dict[ixstr]
        cursrs = pd.Series(
            index=gdf.index, data=[val for i in range(len(gdf))]
        )
        srs = pd.concat([srs, cursrs], join='inner').dropna()
    df[otcol] = srs
    return df.sort_index()


class AddDaytimeValuesTransform(DataFrameTransformer):
    def __init__(self, otcol, daytime_dict):
        '''
        :type otcol: obj
        :param otcol: output column identifier, usually a string
        :type daytime_dict: dict<str, obj>
        '''
        self.otcol = otcol
        self.daytime_dict = daytime_dict

    def run(self, input_df):
        return add_daytime_values(
            input_df=input_df,
            otcol=self.otcol,
            daytime_dict=self.daytime_dict
        )


class DateFlagTransformer(DataFrameTransformer):
    '''
    Flags entire Dates
    '''
    def __init__(self, otcol, dates):
        self.otcol = otcol
        self.dates = set(dates)

    def run(self, input_df):
        df = input_df.copy(deep=True)
        srs = pd.Series(index=df.index)
        grps = df.groupby([df.index.date])
        for dt, gdf in grps:
            data = []
            for i in range(len(gdf)):
                if dt in self.dates:
                    data.append(True)
                    continue
                data.append(False)
            cursrs = pd.Series(index=gdf.index, data=data)
            srs = pd.concat([srs, cursrs], join='inner').dropna()
        df[self.otcol] = srs
        return df


class DatetimeFlagTransformer(DataFrameTransformer):
    '''
    Flags only exact datetime matches
    '''
    def __init__(self, otcol, dts):
        '''
        :type otcol: obj
        :type dts: set<datetime>
        '''
        self.otcol = otcol
        self.dts = set(dts)

    def run(self, input_df):
        '''
        :type input_df: pandas.DataFrame
        :rtype: pandas.DataFrame
        '''
        df = input_df.copy(deep=True)
        data = []
        for i in range(len(df)):
            if df.index[i] in self.dts:
                data.append(True)
                continue
            data.append(False)
        df[self.otcol] = data
        return df




