import abc
from lib.saxutil.pandas_tools import pd_group

def run_transform_pipeline(pipe, input_df):
    df = input_df.copy(deep=True)
    for tr in pipe:
        df = tr.run(df)
    return df


class DataFrameTransformer(object):
    __metaclass__ = abc.ABCMeta

    def run(self, input_df):
        '''
        :type input_df: pandas.DataFrame

        Transforms input_df and returns the transformed DataFrame.
        Logic must be implemented in a class that inherits from this.
        '''
        raise NotImplementedError('''
        DataFrameTransformer is a base class that serves as a template.
        This method was either called from a DataFrameTransformer instance
        or a subclass that does not implement the "run(self, input_df)"
        method.
        ''')


class GroupBasedDataFrameTransformer(DataFrameTransformer):
    def __init__(self, groupby):
        '''
        :type groupby: list<str>
        '''
        self.groupby = groupby

    def get_pd_groupby(self, input_df):
        '''
        :type input_df: pandas.DataFrame
        :rtype: list<pandas.Index>
        '''
        return pd_group.convert_string_groupby(self.groupby, input_df)
