import pandas as pd
from lib.saxutil.pandas_tools.transform import tr_base


def _get_match_bools_on_attr(df, matchrule_on_attr):
    match_bools_on_attr = {}
    for attr, matchrule in matchrule_on_attr.items():
        if attr is None:
            # Setting attr to None will trigger matching on the index itself.
            attrvals = df.index
        else:
            attrvals = getattr(df.index, attr)
        if not hasattr(attrvals, 'isin'):
            attrvals = pd.Index(attrvals)
        match_bools_on_attr[attr] = attrvals.map(matchrule.match)
    return match_bools_on_attr


def flag_index_attribute_matches(
    input_df,
    flgcol,
    matchrules_on_attr,
    flgval=True,
    allow_partial_matches=False
):
    '''
    :type input_df: pandas.DataFrame
    :type flgcol: obj
    :type matchrules_on_attr: dict<str, matchrule.MatchRule>
    :param matchrules_on_attr:
        Key: 
            String name of the index attribute. The valid strings will vary
            based on the index type. For Datetime indices, valid values
            include, weekday, year, month, day, minute, and second.
        Value:
            matchrule.MatchRule object instance. MatchRule objects have an
            instance method "match". It returns True if a match is present
            and False otherwise.
        Example:
            Suppose the input DataFrame (input_df) has a DateTimeIndex. We want
            to flag all values where the weekday is Monday. Monday is encoded
            as 0 in Pandas and Python datetime modules.

            To accomplish this set "matchrules_on_attr" to ...
            matchrules_on_attr = {'weekday':matchrule.SetMatchRule({1})}

    :type flgval: bool
    '''
    df = input_df.copy(deep=True)
    match_bools_on_attr = _get_match_bools_on_attr(df, matchrules_on_attr)
    for i in range(len(df)):
        bools = set()
        for attr, ismatched in match_bools_on_attr.items():
            bools.add(ismatched[i])
        if len(bools) == 1 and True in bools:
            df.loc[df.index[i], flgcol] = flgval
        elif allow_partial_matches and True in bools:
            df.loc[df.index[i], flgcol] = flgval
    return df


class IndexAttrMatchFlagTransformer(tr_base.DataFrameTransformer):
    def __init__(
        self,
        flgcol,
        matchrules_on_attr,
        flgval=True,
        allow_partial_matches=False
    ):
        '''
        :type flgcol: obj
        :type matchrules_on_attr: dict<str, set<obj>>
        :param matchrules_on_attr: see the description of the paramter with the
        same name in method "flag_index_attribute_matches".
        :type flgval: bool
        '''
        self.flgcol = flgcol
        self.matchrules_on_attr = matchrules_on_attr
        self.flgval = flgval
        self.allow_partial_matches = allow_partial_matches

    def run(self, input_df):
        '''
        :type input_df: pandas.DataFrame
        '''
        return flag_index_attribute_matches(
            input_df=input_df,
            flgcol=self.flgcol,
            matchrules_on_attr=self.matchrules_on_attr,
            flgval=self.flgval,
            allow_partial_matches=self.allow_partial_matches
        )
