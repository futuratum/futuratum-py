from lib.saxutil.pandas_tools.transform import tr_base


class TrimNanTransformer(tr_base.DataFrameTransformer):
    def __init__(self, refcol, frombeg, fromend):
        '''
        :type refcol: obj
        :type frombeg: bool
        :type fromend: bool
        '''
        self.refcol = refcol
        self.frombeg = frombeg
        self.fromend = fromend

    def run(self, input_df):
        df = input_df.copy(deep=True)
        bix, eix = df.index[0], df.index[-1]
        if self.frombeg:
            bix = df[self.refcol].first_valid_index()
        if self.fromend:
            eix = df[self.refcol].last_valid_index()
        return df.truncate(before=bix, after=eix)


class FillNanTransformer(tr_base.DataFrameTransformer):
    def __init__(self, refcol, otcol, fill_val_col):
        self.refcol = refcol
        self.otcol = otcol
        self.fill_val_col = fill_val_col

    def run(self, input_df):
        df = input_df.copy(deep=True)
        df[self.otcol] = df[self.refcol].fillna(value=df[self.fill_val_col])
        return df