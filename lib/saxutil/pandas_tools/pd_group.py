def convert_string_groupby(string_groupby, df):
    '''
    :type string_groupby: list<str>
    :type df: pandas.DataFrame
    :rtype: list<pandas.Index>
    '''
    groupby = []
    for x in string_groupby:
        groupby.append(getattr(df.index, x))
    return groupby