import numpy as np


def _arr_type_check(preds, actuals):
	if type(actuals) in {list}:
		actuals = np.array(actuals)
	if type(preds) in {list}:
		preds = np.array(preds)
	return actuals, preds


def ssr(preds, actuals):
	'''
	:type preds: np.ndarray
	:type actuals: np.ndarray
	'''
	actuals, preds = _arr_type_check(preds, actuals)
	return ((actuals - preds) ** 2).sum()


def abs_err(preds, actuals):
	preds, actuals = _arr_type_check(preds, actuals)
	return np.absolute(actuals - preds).sum()
