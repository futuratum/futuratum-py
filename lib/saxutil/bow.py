from collections import Counter
import numpy as np
from scipy import sparse
from sklearn.preprocessing import normalize
from lib.saxutil import ftmap
from sklearn.feature_extraction.text import TfidfTransformer


class WeightMap(object):
    def __init__(self, ftmap):
        self.ftmap = ftmap
        self.wt_on_ft = {}

    def set_ft_wt(self, ft, wt):
        self.wt_on_ft[ft] = wt

    def get_ft_wt(self, ft, default=None):
        return self.wt_on_ft.get(ft, default)

    def get_obj_wt(self, obj, default=None):
        return self.get_ft_wt(self.ftmap.get_ft(obj), default)


def map_fts_to_cols(fts):
    """
    :type fts: list<int>
    """
    fts = list(fts)
    col_on_ft = {}
    for i in range(len(fts)):
        col_on_ft[fts[i]] = i
    return col_on_ft


def nz_shape(mtx):
    nzrows, nzcols = mtx.nonzero()
    numrows, numcols = 0, 0
    if len(nzrows) > 0:
        numrows = max(nzrows) + 1
    if len(nzcols) > 0:
        numcols = max(nzcols) + 1
    return numrows, numcols


def mk_mtx(fmap, ftmtx, col_on_ft, row_norma='l2'):
    '''
    :type fmap: FeatureMap
    :type ftmx: sparse.lil_matrix
    :type col_on_ft: dict<int, int>
    :type row_norma: str
    '''
    # number of rows, number of ftmtx columns
    numrows, ft_numcols = nz_shape(ftmtx)
    # number of clfmtx columns
    cl_numcols = max(col_on_ft.values()) + 1
    clfmtx = sparse.lil_matrix( (numrows, cl_numcols,) )

    for row_ix in range(numrows):
        nzrows, nzcols = ftmtx.getrow(row_ix).nonzero()
        # We're working with a single row. As such, the number of nonzero
        # rows should always be 1 or 0
        assert(len(set(nzrows)) <= 1)
        for ftcol in nzcols:
            clf_col_ix = col_on_ft.get(ftcol)
            if clf_col_ix is None:
                continue
            clfmtx[row_ix, clf_col_ix] = ftmtx[row_ix, ftcol]

    if row_norma is not None and len(clfmtx.nonzero()[0]) > 0:
        return normalize(clfmtx, norm=row_norma)
    return clfmtx


class BagOfWords(object):
    def __init__(self, fmap=None, shape=(10000, 500000,), col_on_ft=None):
        if fmap is None:
            fmap = ftmap.FeatureMap()
        self.fmap = fmap
        self.ftmtx = sparse.lil_matrix(shape)
        self.lastrow = 0
        self.col_on_ft = col_on_ft

    def _expandrows(self, expandby):
        expansion = sparse.lil_matrix( (expandby, self.ftmtx.shape[1],) )
        self.ftmtx = sparse.lil_matrix(sparse.vstack([self.ftmtx, expansion]))

    def _expandcols(self, expandby):
        expansion = sparse.lil_matrix( (self.ftmtx.shape[0], expandby,) )
        self.ftmtx = sparse.lil_matrix(sparse.hstack([self.ftmtx, expansion]))

    def num_docs(self):
        return self.lastrow

    def _maybe_expand_rows(self):
        if self.num_docs() >= self.ftmtx.shape[0]:
            self._expandrows(self.ftmtx.shape[0])

    def _add_ft_cntr(self, cntr):
        for ftcol, cnt in cntr.items():
            if ftcol >= self.ftmtx.shape[1]:
                self._expandcols(ftcol * 2)
            self.ftmtx[self.lastrow, ftcol] = cnt
        self.lastrow += 1

    def add_cntr_doc(self, cntr, add_on_absent=True):
        self._maybe_expand_rows()
        fcntr = self.fmap.mk_ft_cntr(cntr, add_on_absent)
        self._add_ft_cntr(fcntr)

    def add_obj_list_doc(self, obj_list, add_on_absent=True):
        self.add_cntr_doc(Counter(obj_list), add_on_absent)

    def get_row_ft_cntr(self, rownum):
        if rownum >= self.num_docs():
            raise IndexError('rownum out of range')
        _, cols = self.ftmtx.getrow(rownum).nonzero()
        cntr = Counter()
        for col in cols:
            cntr[col] += self.ftmtx[rownum, col]
        return cntr

    def get_row_obj_cntr(self, rownum):
        ftcntr = self.get_row_ft_cntr(rownum)
        ocntr = Counter()
        for ft, cnt in ftcntr.items():
            ocntr[self.fmap.get_obj(ft)] = cnt
        return ocntr

    def mk_mtx(self, row_norma='l2'):
        return mk_mtx(self.fmap, self.ftmtx, self.col_on_ft, row_norma)

    def ftmtx_resultant(self, docfreq=False, row_wl=None):
        numrows, numcols = nz_shape(self.ftmtx)
        resarr = np.zeros(numcols) 
        if row_wl is None:
            row_wl = set([i for i in range(self.num_docs())])
        for row in range(numrows):
            if row not in row_wl:
                continue
            _, nzcols = self.ftmtx.getrow(row).nonzero()
            for col in nzcols:
                if docfreq:
                    resarr[col] += 1
                else:
                    resarr[col] += self.ftmtx[row, col]
        return resarr

    def map_fts_to_cols(self, ft_wl=None):
        if ft_wl is None:
            ft_wl = self.fmap.fts()
        self.col_on_ft = map_fts_to_cols(ft_wl)

    def fts_below_freq(self, minfreq, docfreq=False):
        res = self.ftmtx_resultant(docfreq)
        lows = set()
        for i in range(len(res)):
            if res[i] < minfreq:
                lows.add(i)
        return lows

    def clear(self):
        self.ftmtx = sparse.lil_matrix(self.ftmtx.shape)
        self.lastrow = 0

    def rm_fts(self, rmfts):
        # Column mappings are no longer valid
        self.col_on_ft = None
        # Create the new feature map
        new_fmap = ftmap.rm_objs_remap(self.fmap, rmfts=rmfts)
        new_on_old = {}
        for oldft in self.fmap.fts():
            o = self.fmap.get_obj(oldft)
            newft = new_fmap.get_ft(o)
            if newft is not None:
                new_on_old[oldft] = newft

        # Remap the feature blacklist and the feature matrix
        new_ft_bl = set()
        new_ftmtx = sparse.lil_matrix(self.ftmtx.shape)
        for rownum in range(self.lastrow):
            oldcntr = self.get_row_ft_cntr(rownum)
            for oldft, cnt in oldcntr.items():
                newft = new_on_old.get(oldft)
                if newft is not None:
                    if oldft in self.ft_bl:
                        new_ft_bl.add(newft)
                    new_ftmtx[rownum, newft] =  cnt

        self.ftmtx = new_ftmtx
        self.ft_bl = new_ft_bl
        self.fmap = new_fmap

    def mk_idf_wt_map(self):
        tr = TfidfTransformer()
        self.map_fts_to_cols()
        ft_on_col = {f:c for c,f in self.col_on_ft.items()}
        mtx = self.mk_mtx()
        tr.fit(mtx)
        idfs = tr.idf_
        wm = WeightMap(self.fmap)
        for col in range(len(idfs)):
            ft = ft_on_col[col]
            wm.set_ft_wt(ft, idfs[col])
        self.col_on_ft = {}
        return wm
