import numpy as np
from scipy import stats


def correl_mono(Y, f):
    '''
    :type Y: list<float>
    :type f: method
    :rtype: (float, float)
    '''
    r,p = f(Y, [i for i in range(len(Y))])
    if str(r) == 'nan':
        return 0.0, 1.0
    else:
        return r, p


def pearson_mono(Y):
    return correl_mono(Y, stats.pearsonr)


def spearman_mono(Y):
    return correl_mono(Y, stats.spearmanr)


def log(x, b=np.e):
    return np.log(x) / np.log(b)


class NumpyLog(object):
    def __init__(self, b=np.e):
        self.b = b

    def run(self, x):
        return log(x, b=self.b)
