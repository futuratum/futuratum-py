import abc


class MatchRule(object):
    __metaclass__ = abc.ABCMeta

    def match(self, val):
        '''
        Abstract method. Implemented in subclasses
        '''
        raise NotImplementedError('')


class SetMatchRule(MatchRule):
    def __init__(self, matchvals):
        self.matchvals = set(matchvals)

    def match(self, val):
        if val in self.matchvals:
            return True
        return False


class RangeMatchRule(MatchRule):
    def __init__(self, minval, maxval):
        self.minval = minval
        self.maxval = maxval

    def match(self, val):
        if self.minval <= val <= self.maxval:
            return True
        return False