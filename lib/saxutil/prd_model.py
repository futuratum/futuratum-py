import os, json, pickle


class Model(object):
    def __init__(self, mdl, colnames):
        if hasattr(mdl, 'coef_') and len(mdl.coef_) != len(colnames):
            raise ValueError(
                'The number of colnames names:\t' + str(len(colnames)) + '\n'
                'The number of columns the model uses:\t' + str(len(mdl.coef_))
            )
        self.mdl = mdl
        self.col_on_name = {colnames[i]:i for i in range(len(colnames))}
        self.name_on_col = {i:colnames[i] for i in range(len(colnames))}
        self.default_val_on_col = {}
        self.rows = []

    def clear(self):
        self.rows = []

    def set_default_col_val(self, col, val):
        self.default_val_on_col[col] = val

    def default_col_val(self, col):
        return self.default_val_on_col.get(col)

    def get_colnames(self):
        return list(self.col_on_name.keys())

    def numcols(self):
        return len(self.get_colnames())

    def add_inst(self, instdict):
        l = [None for i in range(self.numcols())]

        for colname, val in instdict.items():
            col = self.col_on_name[colname]
            l[col] = val

        for i in range(len(l)):
            if l[i] is not None:
                continue
            colname = self.name_on_col.get(i)
            val = self.default_col_val(colname)
            if val is None:
                raise ValueError(
                    '''
                    Encountered a null value for a column without a
                    default value. Column {i}
                    '''.format(i=colname)
                )
            l[i] = val
        self.rows.append(l)

    def predict(self):
        return self.mdl.predict(self.rows)


def serialize_model_to_dir(model, dpath):
    os.mkdir(dpath)
    with open(os.path.join(dpath, 'mdl.pk'), 'wb') as f:
        f.write(pickle.dumps(model.mdl))
    with open(os.path.join(dpath, 'col_on_name.json'), 'w') as f:
        f.write(json.dumps(model.col_on_name))
    with open(os.path.join(dpath, 'default_val_on_col.json'), 'w') as f:
        f.write(json.dumps(model.default_val_on_col))


def load_model_from_dir(dpath):
    with open(os.path.join(dpath, 'mdl.pk'), 'rb') as f:
        mdl = pickle.loads(f.read())
    with open(os.path.join(dpath, 'col_on_name.json')) as f:
        col_on_name = json.loads(f.read())
    with open(os.path.join(dpath, 'default_val_on_col.json')) as f:
        default_val_on_col = json.loads(f.read())

    tups = list(col_on_name.items())
    tups.sort(key=lambda t:t[1])
    model = Model(mdl, [t[0] for t in tups])
    model.default_val_on_col = default_val_on_col
    return model