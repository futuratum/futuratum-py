def _mk_hashtup(match_val_on_attr):
    tups = []
    for attr, mv in match_val_on_attr.items():
        tups.append( (attr, mv,) )
    tups.sort()
    hashtup = []
    for t in tups:
        hashtup.extend( [ t[0], t[1], ] )
    return tuple(hashtup)

class ConditionInterp(object):
    def __init__(self):
        self.d = {}

    def add_condition(self, match_val_on_attr, interpolate_val):
        hashtup = _mk_hashtup(match_val_on_attr)
        self.d[hashtup] = interpolate_val

    def get_interp_val(self, match_val_on_attr):
        hashtup = _mk_hashtup(match_val_on_attr)
        return self.d.get(hashtup)