import os


def basepath():
    end = None
    pfx = os.path.realpath(__file__)
    while end not in {'lib'}:
        pfx, end = os.path.split(pfx)
    return os.path.join(pfx, end)


def data_dir():
    return os.path.join(basepath(), 'data')


def match_generalizer_fpath(version, name):
    relpath = 'senti/{version}/matchgenr_{name}.tsv'
    relpath = relpath.replace('/', os.sep)
    relpath = relpath.format(version=version, name=str(name))
    return os.path.join(data_dir(), relpath)
