import os
import json
import pickle
import numpy as np
from scipy import sparse
from sklearn.preprocessing import normalize
from sklearn.feature_selection import SelectKBest, chi2
from collections import Counter
from lib.saxutil import ftmap, cntr_util
from lib.saxutil.txt_proc import tkn_transform


def map_fts_to_cols(fts):
    """
    :type fts: list<int>
    """
    fts = list(fts)
    col_on_ft = {}
    for i in range(len(fts)):
        col_on_ft[fts[i]] = i
    return col_on_ft


def mk_mtx(fmap, ft_cntrs, col_on_ft, row_norma=None):
    """
    :type fmap: FeatureMap
    :type ft_cntrs: list<Counter>
    :type col_on_ft: dict<int, int>
    :type row_norma: str
    :rtype: scipy.sparse.lil_matrix
    """
    numrows = len(ft_cntrs)
    numcols = len(col_on_ft)
    mtx = sparse.lil_matrix((numrows, numcols,))
    for row_ix in range(numrows):
        for ft, cnt in ft_cntrs[row_ix].items():
            col_ix = col_on_ft.get(ft)
            if col_ix is None:
                continue
            mtx[row_ix, col_ix] = cnt
    if row_norma is not None:
        return normalize(mtx, norm=row_norma)
    return mtx


def get_fts(ft_cntrs):
    fts = set()
    for fc in ft_cntrs:
        for f, c in fc.items():
            fts.add(f)
    return fts


def nz_shape(mtx):
    nzrows, nzcols = mtx.nonzero()
    numrows, numcols = 0, 0
    if len(nzrows) > 0:
        numrows = max(nzrows) + 1
    if len(nzcols) > 0:
        numcols = max(nzcols) + 1
    return numrows, numcols


def mk_mtx2(fmap, ftmtx, col_on_ft, row_norma='l2'):
    '''
    :type fmap: FeatureMap
    :type ftmx: sparse.lil_matrix
    :type col_on_ft: dict<int, int>
    :type row_norma: str
    '''
    # number of rows, number of ftmtx columns
    numrows, ft_numcols = nz_shape(ftmtx)
    # number of clfmtx columns
    cl_numcols = max(col_on_ft.values()) + 1
    clfmtx = sparse.lil_matrix( (numrows, cl_numcols,) )

    for row_ix in range(numrows):
        nzrows, nzcols = ftmtx.getrow(row_ix).nonzero()
        # We're working with a single row. As such, the number of nonzero
        # rows should always be 1 or 0
        assert(len(set(nzrows)) <= 1)
        for ftcol in nzcols:
            clf_col_ix = col_on_ft.get(ftcol)
            if clf_col_ix is None:
                continue
            clfmtx[row_ix, clf_col_ix] = ftmtx[row_ix, ftcol]

    if row_norma is not None:
        return normalize(clfmtx, norm=row_norma)
    return clfmtx


class ClfBase2(object):
    def __init__(self, fmap, shape, col_on_ft=None):
        self.fmap = fmap
        self.ftmtx = sparse.lil_matrix(shape)
        self.lastrow = 0
        self.col_on_ft = col_on_ft

    def _expandrows(self, expandby):
        expansion = sparse.lil_matrix( (expandby, self.ftmtx.shape[1],) )
        self.ftmtx = sparse.lil_matrix(sparse.vstack([self.ftmtx, expansion]))

    def num_docs(self):
        return self.lastrow

    def get_row_ft_cntr(self, rownum):
        if rownum >= self.num_docs():
            raise IndexError('rownum out of range')
        _, cols = self.ftmtx.getrow(rownum).nonzero()
        cntr = Counter()
        for col in cols:
            cntr[col] += self.ftmtx[rownum, col]
        return cntr

    def get_row_obj_cntr(self, rownum):
        ftcntr = self.get_row_ft_cntr(rownum)
        ocntr = Counter()
        for ft, cnt in ftcntr.items():
            ocntr[self.fmap.get_obj(ft)] = cnt
        return ocntr

    def mk_mtx(self, row_norma='l2'):
        return mk_mtx2(self.fmap, self.ftmtx, self.col_on_ft, row_norma)

    def ftmtx_resultant(self, docfreq=False, row_wl=None):
        numrows, numcols = nz_shape(self.ftmtx)
        resarr = np.zeros(numcols) 
        if row_wl is None:
            row_wl = set([i for i in range(self.num_docs())])
        for row in range(numrows):
            _, nzcols = self.ftmtx.getrow(row).nonzero()
            for col in nzcols:
                if docfreq:
                    resarr[col] += 1
                else:
                    resarr[col] += self.ftmtx[row, col]
        return resarr

    def clear(self):
        self.ftmtx = sparse.lil_matrix(self.ftmtx.shape)
        self.lastrow = 0


class Clf2(ClfBase2):
    def __init__(
        self, mdl, fmap, shape, col_on_ft, row_norma='l2'
    ):
        super(Clf2, self).__init__(fmap, shape)
        self.mdl = mdl
        self.row_norma = row_norma
        self.col_on_ft = col_on_ft

    def add_cntr_doc(self, cntr):
        cntr = mk_ft_cntr(cntr, self.fmap, True)
        for ftcol, cnt in cntr.items():
            self.ftmtx[self.lastrow, ftcol] = cnt
        self.lastrow += 1

    def add_obj_list_doc(self, obj_list):
        self.add_cntr_doc(Counter(obj_list))

    def predict(self, return_proba=False):
        if not return_proba:
            return self.mdl.predict(self.mk_mtx(self.row_norma))
        pred_prob_mtx = [ [None, None] for i in range(self.num_docs()) ]
        #pred_prob_mtx = np.zeros((self.num_docs(), 2,))
        prob_arrs = self.mdl.predict_proba(self.mk_mtx(self.row_norma))
        for i in range(len(prob_arrs)):
            max_prob = max(prob_arrs[i])
            pred = self.mdl.classes_[list(prob_arrs[i]).index(max_prob)]
            pred_prob_mtx[i][0] = pred
            pred_prob_mtx[i][1] = max_prob
        return np.array(pred_prob_mtx)


class Trnr2(ClfBase2):
    def __init__(self, shape):
        fmap = ftmap.FeatureMap()
        super(Trnr2, self).__init__(fmap, shape)
        self._lbls = [[None] for i in range(shape[1])]
        self.ft_bl = set()

    def _expandcols(self, expandby):
        expansion = sparse.lil_matrix( (self.ftmtx.shape[0], expandby,) )
        self.ftmtx = sparse.lil_matrix(sparse.hstack([self.ftmtx, expansion]))

    def _expandrows(self, expandby):
        super(Trnr2, self)._expandrows(expandby)
        self._lbls = self._lbls + [None for i in range(expandby)]

    def lbls(self):
        return self._lbls[:self.lastrow]

    def add_cntr_doc(self, cntr, lbl):
        if self.lastrow >= self.ftmtx.shape[0]:
            self._expandrows(self.ftmtx.shape[0])
        cntr = mk_ft_cntr(cntr, self.fmap, True)
        for ftcol, cnt in cntr.items():
            if ftcol >= self.ftmtx.shape[1]:
                self._expandcols(self.ftmtx.shape[1])
            self.ftmtx[self.lastrow, ftcol] = cnt
        self._lbls[self.lastrow] = lbl
        self.lastrow += 1

    def add_obj_list_doc(self, obj_list, lbl):
        self.add_cntr_doc(Counter(obj_list), lbl)

    def map_fts_to_cols(self):
        ft_wl = self.fmap.fts() - self.ft_bl
        self.col_on_ft = map_fts_to_cols(ft_wl)

    def rm_fts(self, rmfts):
        # Column mappings are no longer valid
        self.col_on_ft = None
        # Create the new feature map
        new_fmap = ftmap.rm_objs_remap(self.fmap, rmfts=rmfts)
        new_on_old = {}
        for oldft in self.fmap.fts():
            o = self.fmap.get_obj(oldft)
            newft = new_fmap.get_ft(o)
            if newft is not None:
                new_on_old[oldft] = newft

        # Remap the feature blacklist and the feature matrix
        new_ft_bl = set()
        new_ftmtx = sparse.lil_matrix(self.ftmtx.shape)
        for rownum in range(self.lastrow):
            oldcntr = self.get_row_ft_cntr(rownum)
            for oldft, cnt in oldcntr.items():
                newft = new_on_old.get(oldft)
                if newft is not None:
                    if oldft in self.ft_bl:
                        new_ft_bl.add(newft)
                    new_ftmtx[rownum, newft] =  cnt

        self.ftmtx = new_ftmtx
        self.ft_bl = new_ft_bl
        self.fmap = new_fmap

    def to_clf(self, unfitmdl, row_norma='l2'):
        unfitmdl.fit(self.mk_mtx(row_norma), self.lbls())
        return Clf2(
            unfitmdl, self.fmap, self.ftmtx.shape, self.col_on_ft, row_norma
        )

