import io
import csv
from lib.saxutil import list_util, dict_util
from lib.saxutil.txt_proc import lookahead


def default_match_genr_postag_method(from_tags, to_size):
    if from_tags[0] is None:
        return [None] * to_size
    starts = [t[0].lower() for t in from_tags]
    if 'v' in starts:
        # If verb is in "from_tags" assume "to_tags" are all verbs
        return ['v'] * to_size
    else:
        # Assume "to_tags" are all the final tag in "from_tags"
        return [from_tags[-1]] * to_size


def update_from_ngrams_on_start(from_ngram, from_ngrams_on_start):
    dict_util.append_set_dict(from_ngrams_on_start, from_ngram[0], from_ngram)


def preproc_tkns_tags(tkns, tags, default_pos=None):
    if tags is None:
        tags = [default_pos] * len(tkns)
    list_util.raise_err_on_diff_len([tkns, tags])
    return tkns, tags


def preproc_ngram(ngram, case_sensitive):
    if not case_sensitive:
        ngram = [t.lower() for t in ngram]
    return tuple(ngram)


def check_for_key_err(from_ngram, to_dta_on_from_ngram):
    if to_dta_on_from_ngram.get(from_ngram) is not None:
        raise KeyError(str(from_ngram))

