import io, csv, re
import numpy as np
from collections import Counter
from lib.saxutil import list_util


def load_rules_from_tsv_dta(
    dta, tkncol, postagcol, orthcol=None, lemmacol=None
):
    if orthcol is None:
        orthcol = tkncol
    if lemmacol is None:
        lemmacol = orthcol
    tkns = []
    rules = []
    si = io.StringIO(dta)
    rdr = csv.reader(si, delimiter='\t')
    for r in rdr:
        tkn = r[tkncol].strip()
        tkns.append(tkn)
        rules.append(
            [
                {
                    'ORTH':r[orthcol].strip(),
                    'LEMMA':r[lemmacol].strip(),
                    'TAG':r[postagcol].strip(),
                }
            ]
        )
    list_util.raise_err_on_diff_len([tkns, rules])
    return tkns, rules


def term_indices(doc, termstr, use_lemma=False):
    indices = set()
    for i in range(len(doc)):
        if (
            (use_lemma and doc[i].lemma_ == termstr) or
            (doc[i].text == termstr)
        ):
            indices.add(i)
    return indices


def nearest_pos_ancestor(
    pospatt,
    input_tkn,
    maxdist=float('inf'),
):
    pospatt = re.compile(pospatt)
    dist = 0
    cur_tkn = input_tkn
    while cur_tkn.head != cur_tkn and dist < maxdist:
        if pospatt.match(cur_tkn.head.tag_):
            return cur_tkn.head
        cur_tkn = cur_tkn.head
    return None


def dijsktra(doc):
    edges = []
    for token in doc:
        for child in token.children:
            edges.append( (token.i, child.i,) )
    return nx.Graph(edges)


def tkn_dep_dists(doc, reftkn, graph):
    tups = []
    for tkn in doc:
        if tkn == reftkn or nx.has_path(graph, reftkn.i, tkn.i) == False:
            continue
        pathlen = nx.shortest_path_length(graph, reftkn.i, tkn.i)
        tups.append( (tkn, pathlen,) )
    return tups


def mk_inverse_term_dist_cntr(doc, ref_term_str):
    cntr = Counter()
    graph = dijsktra(doc)
    ref_indices = term_indices(doc, ref_term_str)
    print(str(ref_indices))
    for i in ref_indices:
        term_dists = tkn_dep_dists(doc, doc[i], graph)
        for term, dist in term_dists:
            cntr[term] += 1.0 / dist
    return cntr



"""
def nearest_ancestor_match(
    input_tkn,
    hd_pospatt,
    hd_lemmapatt,
    maxdist=float('inf'),
    input_lemma_wl = None,
):
    if input_lemma_wl is not None and input_tkn.lemma_ not in input_lemma_wl:
        return False
    hd_pospatt = re.compile(hd_pospatt)
    hd_lemmapatt = re.compile(hd_lemmapatt)
    dist = 0
    curtkn = input_tkn
    while curtkn.head != curtkn and dist < maxdist:
        if (
            hd_pospatt.match(curtkn.head.tag_) and
            hd_lemmapatt.match(curtkn.head.lemma_)
        ):
            return True
        curtkn = curtkn.head
        dist += 1
    return False
"""


"""
def has_child_match(tkn, pospatt, tknpatt, maxdist=None):
    for c in tkn.children:
        if pospatt.match(c.tag_) and pospatt.match(c.lemma_):
            return True
    return False


def has_ancestor_rel(
    tkn, descendant_lemma, ancestor_lemma, pospatt, maxdist=float('inf')
):
    if tkn.lemma_ == descendant_lemma:
        return nearest_ancestor_match(
            tkn=tkn, pospatt=pospatt, tknpatt='.*', maxdist=maxdist
        )
    return False
"""


"""
def get_adjective_subjects(doc, tkn_ix):
    if doc[tkn_ix].tag_ not in {'JJ', 'JJR'}:
        raise ValueError('the token at tkn_ix must be an adjective')

    nn_tags = {'NN', 'NNS', 'NNP', 'NNPS', 'PRP', 'PRP$'}

    if doc[tkn_ix].head.tag_ in nn_tags:
        # If the adjective's head is a noun, the adjective almost
        # certainly describes that noun. Return the index of the
        # of the noun
        return [doc[tkn_ix].head.i]

    # Case 2: The head of the adjective is 
    if doc[tkn_ix].head.lemma == 'be':
        children = list(doc[tkn_ix].head.children)
        nn_tkns = []
        for tkn in children:
            if tkn.head.tag_ in nn_tags:
                nn_tkns.append(tkn)
        if len(nn_tkns) == 1:
            return [nn_tkns[0].head.i]
    return []
"""

