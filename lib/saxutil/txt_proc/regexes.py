import re

IS_NUMERIC_PATT = re.compile(r'^[0-9]+\.?([0-9]+)?$')
IS_HASHTAG = re.compile(r'^#\w{3,}')
IS_TWITTER_UN = re.compile(r'^@(\w){4,15}$')
IS_URL = re.compile("^https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,}$")

EMOTICONS = r"""
    (?:
      [<>]?
      [:;=8]                     # eyes
      [\-o\*\']?                 # optional nose
      [\3\)\]\(\[dDpP/\:\}\{@\|\\] # mouth
      |
      [\)\]\(\[dDpP/\:\}\{@\|\\] # mouth
      [\-o\*\']?                 # optional nose
      [:;=8]                     # eyes
      [<>]?
      |
      <3                         # heart
      |
      </3                        # broke heart						
      |
      <\\3                       # broke heart
    )
"""


def match_spans(instr, patt):
    # Detect all subsections of the input string that match a regex 
    # pattern
    return [m.span() for m in list(patt.finditer(instr))]


def mk_letter_rep_replacer_patt(maxrep):
    """
    suppose maxrep == 2
    Match cases where the same letter repeats at least twice.
    Regex: ([A-Za-z])\1{2,}
    """
    return re.compile(r'([A-Za-z])\1{' + str(maxrep) + r',}')