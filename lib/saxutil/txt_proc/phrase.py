import nltk
import re

WANTBAD = re.compile(r"(want|need|wanted|needed)\s[^.,!?]*\sbad")

def ismatch(tkns, patt):
	strtkns = '\t'.join(tkns)
	assert( len(strtkns.split('\t') == len(tkns)) )
	#patt = re.compile(r"(want|need|wanted|needed)\s[^.,!?]*\sbad")
	res = list(patt.finditer(strtkns))
	if len(res) == 0:
		return False
	return True


class ChunkRuleDetector(object):
	def __init__(self):
		self.parser = nltk.RegexpParser(grammar)

	def run(self, tkns, tags):
		assert(len(tkns) == len(tags))
		matches = []
		tree = self.parser.parse(
			[ (tkns[i], tags[i]) for i in range(len(tkns)) ]
		)
		postree = tree.pos()
		for i in range(len(postree)):
			treelbl = postree[i][1]
			matches.append( (i, treelbl,) )
		return matches