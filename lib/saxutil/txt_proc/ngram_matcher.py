from lib.saxutil.txt_proc import lookahead
from lib.saxutil import pair_range_util


class Matcher(object):
	def __init__(self):
		self.ngram_candidates_on_start = {}

	def add_ngram(self, ngram):
		s = self.ngram_candidates_on_start.get(ngram[0], set())
		s.add(tuple(ngram))
		self.ngram_candidates_on_start[ngram[0]] = s

	def find_match_bnds(self, tkns):
		return lookahead.find_all_match_bounds(
			tkns, self.ngram_candidates_on_start
		)

	def find_matches(self, tkns):
		bnds = self.find_match_bnds(tkns)
		matches = []
		for b, e in bnds:
			matches.append(tkns[b:e+1])
		return matches

