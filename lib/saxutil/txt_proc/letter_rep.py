from lib.saxutil.txt_proc import regexes


def draw_candidates(tkn, cands=None):
    if cands is None:
        cands = set()
    patt = regexes.mk_letter_rep_replacer_patt(1)
    spans = regexes.match_spans(tkn, patt)
    for b, e in spans:
        cur_end = int(b) + 1
        while cur_end <= e:
            left = tkn[:b]
            mid = tkn[b:cur_end]
            right = tkn[e:]
            #print(str(left) + ',' + str(mid) + ',' + str(right))
            cand = left + mid + right
            if cand not in cands:
                cands.add( cand )
                cands = cands.union(draw_candidates(cand, cands))
            cur_end += 1
    return cands


class LetterRepMiner(object):
    def __init__(self, vocab):
        if hasattr(vocab, 'append'):
            vocab = set(vocab)
        self.vocab = vocab


    def get_vocab_matches(self, tkn):
        cands = draw_candidates(tkn)
        matches = set()
        for c in cands:
            if c in self.vocab:
                matches.add(c)
        return matches



