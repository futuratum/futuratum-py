from lib.saxutil import pair_range_util


def find_all_match_bounds(tkns, ngram_candidates_on_start):
    bnds = []
    for i in range(len(tkns)):
        candidate_ngrams = ngram_candidates_on_start.get(tkns[i])
        if candidate_ngrams is None:
            continue
        match_ngrams = []
        for ng in candidate_ngrams:
            if i + len(ng) - 1 > len(tkns):
                # ngram length exceeds the remaining length of the
                # token sequence.
                continue
            if tuple(tkns[i:i + len(ng)]) == ng:
                match_ngrams.append(ng)

        for ng in match_ngrams:
            if tuple(tkns[i:i + len(ng)]) in candidate_ngrams:
                bnds.append((i, i + len(ng) - 1,))
    return bnds


def non_subsumed_match_bounds(tkns, ngram_candidates_on_start):
    """
    :type tkns: list<str>
    :type ngram_candidates_on_start: dict<str, set<tuple>>
    """
    bnds = find_all_match_bounds(tkns, ngram_candidates_on_start)
    subsumed = pair_range_util.find_subsumed(bnds)
    bounds_filt = []
    for i in range(len(bnds)):
        if i not in subsumed:
            bounds_filt.append(bnds[i])
    return bounds_filt