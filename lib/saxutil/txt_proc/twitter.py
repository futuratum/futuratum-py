import wordsegment
import string
import nltk
from lib.saxutil.txt_proc import regexes
from lib.saxutil import list_util, path_util
from lib.saxutil.txt_proc import tkn_transform, match_generalizer, spacy_util


def split_on_hashtags(tkns, tags=None):
    """
    :type tkns: list<str>
    :rtype: list<list<str>>
    """
    if tags is None:
        tags = [None] * len(tkns)
    hashtag_indices = []
    def boolfunc(tkn):
        if (regexes.IS_HASHTAG.match(tkn) and tkn[1:].isnumeric() == False):
            return True
        return False
    indices = list_util.bool_func_match_indices(tkns, boolfunc)
    split_points = []
    for i in indices:
        split_points.append(i)
        if i < len(tkns) - 1:
            split_points.append(i + 1)
    tkn_sublists = list_util.split_list_on_indices(tkns, set(split_points))
    tag_sublists = list_util.split_list_on_indices(tags, set(split_points))
    return tkn_sublists, tag_sublists


def segment_sublist_hashtags(tkn_sublists, tag_sublists):
    seg_tkn_sublists, seg_tag_sublists = [], []
    list_util.raise_err_on_diff_len([tkn_sublists, tag_sublists])
    for i in range(len(tkn_sublists)):
        list_util.raise_err_on_diff_len([tkn_sublists[i], tag_sublists[i]])
        tknlen = len(tkn_sublists[i])
        if tknlen == 1 and regexes.IS_HASHTAG.match(tkn_sublists[i][0]):
            ht_tkns = wordsegment.segment(tkn_sublists[i][0][1:])
            ht_tags = [tag_sublists[i][-1] for j in range(len(ht_tkns))]
            list_util.raise_err_on_diff_len([ht_tkns, ht_tags])
            seg_tkn_sublists.append(ht_tkns)
            seg_tag_sublists.append(ht_tags)
        else:
            seg_tkn_sublists.append(tkn_sublists[i])
            seg_tag_sublists.append(tag_sublists[i])
    return seg_tkn_sublists, seg_tag_sublists


class HashtagSplitTransformer(object):
    """
    Does not support flags
    """
    def run(self, tkns, tags=None):
        tkn_sublists, tag_sublists = split_on_hashtags(tkns, tags)
        tkn_sublists, tag_sublists = (
            segment_sublist_hashtags(tkn_sublists, tag_sublists)
        )
        _tkns, _tags = [], []
        list_util.raise_err_on_diff_len([tkn_sublists, tag_sublists])
        for i in range(len(tkn_sublists)):
            list_util.raise_err_on_diff_len([tkn_sublists[i], tag_sublists[i]])
            _tkns.extend(tkn_sublists[i] + ['.'])
            _tags.extend(tag_sublists[i] + ['.'])
        return _tkns, _tags


class UserNameReplacer(tkn_transform.RegexReplacer):
    def __init__(self, flgset=set(), replace_with='twitteruser__'):
        super(UserNameReplacer, self).__init__(
            regexes.IS_TWITTER_UN, replace_with, flgset
        )


def add_emoticon_rules_from_match_genr_file(nlp, fpath):
    with open(fpath) as f:
        dta = f.read()
    tkns, rules = spacy_util.load_rules_from_tsv_dta(dta, tkncol=0, postagcol=2)
    for i in range(len(tkns)):
        nlp.tokenizer.add_special_case(tkns[i], rules[i])


class TwitterPreprocessor(object):
    '''
    Usage: Handle hashtags and twitter user handles in text. Output
    pre-preprocessed text using the "run" method.

    '''
    def __init__(self, vocab=set()):
        with open(path_util.match_generalizer_fpath('V5', 'ent')) as f:
            dta = f.read()
        mg_ent = match_generalizer.MatchGeneralizer.loads(dta)
        self.tokenizer = nltk.tokenize.casual.TweetTokenizer()
        lrvr = tkn_transform.LetterRepVocabReplacer(vocab)
        lrvr.add_postag_conflict_rule('good', 'JJ')
        self.pipe = [
            HashtagSplitTransformer(),
            tkn_transform.LetterRepReplacer(),
            tkn_transform.LetterRepVocabReplacer(vocab),
            tkn_transform.MatchGeneralizeTransformer(mg_ent),
        ]

    def run(self, text):
        """
        :type text: str
        :rtype: str

        Example:
            Input: @someuser i love your work #keepitup ur gr8
            Output: __twitteruser i love your work. keep it up. ur gr8.

        As one can see in the example. hashtags are segemented.
        The segmented hashtag texts are placed into seperate sentences.

        The seperate sentence heuristic is not always correct. Sometimes
        hashtags are used as regular words rather than topic markers.

        EG: Don't repeal the #randomlaw. Call your senator.

        This has almost no consequences for simple bag of words.
        It may cause errors in text preprocessing pipelines that
        depend on part of speech and/or word order.
        """
        tkns = self.tokenizer.tokenize(text)
        tkns, _ = tkn_transform.run_pipeline(self.pipe, tkns)
        return ' '.join(tkns)