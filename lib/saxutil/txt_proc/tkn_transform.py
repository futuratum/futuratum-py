import re
import string
from nltk.stem import PorterStemmer, WordNetLemmatizer
from lib.saxutil import list_util, dict_util
from lib.saxutil.txt_proc import regexes, negation, letter_rep


def replace_run(replace_func, tkns, tags=None):
    """
    This is the base method the replacer classes use. The first parameter
    "replace_func" is a function that takes two paramters. The first ("tkn")
    is a token. The second ("tag") is a part of speech or POS tag.

    EXAMPLE
    -------
    def input_replace_func(tkn, tag):
        if tkn == 'mom': return 'dad', tag
        return tkn, tag

    tkns, tags = replace_func(input_replace_func, ['mom', 'is', 'great'])

    # The following two statements will be True
    tkns == ['dad', 'is', 'great']
    tags == [None, None, None]
    -------

    In the example above, all tokens equal to "mom" are replaced with "dad".
    The POS tags remain the same. As such, if the token "mom" and the POS tag
    None are passed into "input_replace_func", the method will return the
    token / POS tag pair: ("dad", None).

    When "input_replace_func" is passed into "replace_run" via the
    "replace_func" parameter, it is run over the token and tag lists denoted by
    parameters "tkns" and "tags" respectivey. "replace_run" applies the logic
    in "input_replace_func" to the list of tokens tags. The result is that the
    input token and tag lists: ['mom', 'is', 'great'] and [None, None, None]
    are transformed into ['dad', 'is', 'great'] and [None, None, None].

    NOTE: If the paramter "tags" is not specified, "replace_run" will set the
    "tags" parameter equal to a list of None or NULL values with the same
    length as the input tokens ("tkns"). 

    :type replace_func: function
    :param replace_func: use the logic in function to transform "tkns" and
        "tags".
    a token and tag as parameters.
    :type tkns: list<str>
    :type tags: list<str>
    """
    _tkns = [None] * len(tkns)
    _tags = [None] * len(tkns)
    if tags is None:
        # If no "tags" parameter is passed in, set to a list of None
        # values equal to the length of the "tkns" list.
        tags = [None] * len(tkns)
    list_util.raise_err_on_diff_len([tkns, tags])
    for i in range(len(tkns)):
        tkn, tag = replace_func(tkns[i], tags[i])
        _tkns[i], _tags[i] = tkn, tag
    list_util.raise_err_on_diff_len([tkns, tags, _tkns, _tags])
    return _tkns, _tags


def filt_reject_indices(should_reject_func, tkns, tags=None):
    if tags is None:
        tags = [None] * len(tkns)
    list_util.raise_err_on_diff_len([tkns, tags])
    return (
        [i for i in range(len(tkns)) if should_reject_func(tkns[i], tags[i],)]
    )


def filt_run(should_reject_func, tkns, tags=None):
    """
    This is the base method the filter classes use. The first parameter
    "should_reject_func" is a function that takes two paramters. The first
    ("tkn") is a token. The second ("tag") is a part of speech or POS tag.

    EXAMPLE
    -------
    def input_should_reject(tkn, tag):
        if tkn in {'the', 'and', 'or'}:
            return True
        return False

    tkns, tags = filt_run(input_should_reject, ['the', 'dog', 'and', 'pony'])

    # The following statements will be true
    tkns == ['dog', 'pony']
    tags == [None, None]
    -------

    In the example above, any token equal to "the", "and", or "or" is rejected.
    The "filt_run" function generates a new list of tokens and POS tags that
    exclude all token / POS tag pairs where "input_shoud_reject" returns
    True.

    NOTE: If the paramter "tags" is not specified, "replace_run" will set the
    "tags" parameter equal to a list of None or NULL values with the same
    length as the input tokens ("tkns"). 

    :type should_reject_func: function
    :param should_reject_func: Function that returns True or False given a 
        token and tag as parameters. If True, reject. If False, keep. Use the
        logic in this method identify elements to remove from "tkns" and "tags".
    :type tkns: list<str>
    :type tags: list<str>
    """
    if tags is None:
        tags = [None] * len(tkns)
    reject_indices = filt_reject_indices(should_reject_func, tkns, tags)
    _tkns = [tkns[i] for i in range(len(tkns)) if i not in reject_indices]
    _tags = [tags[i] for i in range(len(tags)) if i not in reject_indices]
    list_util.raise_err_on_diff_len([_tkns, _tags])
    return _tkns, _tags


class FlagSplitter(object):
    """
    At TickerTags, the negation transform appends a flag on each
    negated token "__NEG". This created problems when trying to run additional
    transforms after the fact. The purpose of this class is to identify when
    a flag is present and account for prior to transformation.
    """

    def __init__(self, flgset):
        if len(flgset) == 0:
            self.patt = re.compile('$a')
            return
        pattstr = '('
        for flg in flgset:
            pattstr += flg + '|'
        pattstr = pattstr[:-1] + '$)'
        self.patt = re.compile(pattstr)

    def split(self, tkn):
        res = self.patt.search(tkn)
        if res is None:
            return tkn, ''
        return tkn[:res.start()], tkn[res.start():len(tkn)]


class FiltBase(object):
    def __init__(self, flgset):
        self.flg_splitr = FlagSplitter(flgset)

    def run(self, tkns, tags=None):
        return filt_run(self.tkn_processor, tkns, tags)


class ReplacerBase(object):
    def __init__(self, flgset):
        self.flg_splitr = FlagSplitter(flgset)

    def run(self, tkns, tags=None):
        return replace_run(self.tkn_processor, tkns, tags)

    def spacyrun(self, spacydoc):
        tkns, tags = [w.text for w in spacydoc], [w.tag_ for w in spacydoc]
        tkns, tags = replace_run(self.tkn_processor, tkns, tags)
        for i in range(len(tkns)):
            spacydoc[i].text = tkns[i]
            spacydoc[i].tag_ = tags[i]
            spacydoc[i].keep = True
        return spacydoc


class LowerCaseReplacer(ReplacerBase):
    """
    Usage: Replace all tokens with their lower case equivalent.
    """
    def __init__(self, flgset=set()):
        super(LowerCaseReplacer, self).__init__(flgset)

    def tkn_processor(self, tkn, tag=None):
        rtkn, flg = self.flg_splitr.split(tkn)
        return rtkn.lower() + flg, tag


class StemReplacer(ReplacerBase):
    """
    Usage: Replace all tokens with thier stemmed form.
    """
    def __init__(self, stemmer=PorterStemmer(), flgset=set()):
        super(StemReplacer, self).__init__(flgset)
        self.stemmer = stemmer

    def tkn_processor(self, tkn, tag=None):
        rtkn, flg = self.flg_splitr.split(tkn)
        return self.stemmer.stem(rtkn) + flg, tag


class LemmaReplacer(ReplacerBase):
    def __init__(self, flgset=set()):
        super(LemmaReplacer, self).__init__(flgset)
        self.wnl = WordNetLemmatizer()
        self.wn_pos_on_tbnk_pos_start = {
            'J':'a', 'R':'r', 'V':'v'
        }

    def tkn_processor(self, tkn, tag):
        if tag is None or len(tag) == 0:
            raise ValueError('POS tag required')
        rtkn, flg = self.flg_splitr.split(tkn)
        wnpos = self.wn_pos_on_tbnk_pos_start.get(tag[0], 'n')
        rtkn = self.wnl.lemmatize(tkn, wnpos)
        return rtkn, tag


class LetterRepReplacer(ReplacerBase):
    def __init__(self, maxrep=2, flgset=set()):
        super(LetterRepReplacer, self).__init__(flgset)
        self.maxrep = maxrep
        self.patt = regexes.mk_letter_rep_replacer_patt(self.maxrep)

    def tkn_processor(self, tkn, tag):
        matches = self.patt.finditer(tkn)
        rtkn = str(tkn)
        for m in matches:
            rtkn = rtkn.replace(m.group(), m.group()[0:self.maxrep])
        return rtkn, tag


class LetterRepVocabReplacer(ReplacerBase):
    def __init__(self, vocab, flgset=set()):
        super(LetterRepVocabReplacer, self).__init__(flgset)
        self.lrm = letter_rep.LetterRepMiner(vocab)
        self.match_wl_on_tag = {}

    def add_postag_conflict_rule(self, tkn, tag):
        dict_util.append_set_dict(self.match_wl_on_tag, tag, tkn)

    def add_global_conflict_rule(self, tkn):
        # Use a wildcard for the POS tag conflict
        self.add_postag_conflict_rule(tkn, '*')

    def tkn_processor(self, tkn, tag):
        rtkn, flg = self.flg_splitr.split(tkn)
        matches = self.lrm.get_vocab_matches(rtkn)
        if len(matches) == 1:
            rtkn = list(matches)[0] + flg
        else:
            wl = self.match_wl_on_tag.get('*', set())
            wl = wl.union(self.match_wl_on_tag.get(tag, set()))
            wl_matches = set()
            for m in matches:
                if m in wl:
                    wl_matches.add(m)
            if len(wl_matches) == 1:
                rtkn = list(wl_matches)[0] + flg
        return rtkn, tag


class RegexReplacer(ReplacerBase):
    def __init__(self, patt, replace_with, flgset=set()):
        super(RegexReplacer, self).__init__(flgset)
        self.replace_with = replace_with
        self.patt = patt

    def tkn_processor(self, tkn, tag=None):
        rtkn, flg = self.flg_splitr.split(tkn)
        if self.patt.match(rtkn):
            return self.replace_with + flg, tag
        return rtkn + flg, tag


class NumberReplacer(RegexReplacer):
    """
    Usage: Set all numeric tokens to a uniform string.
    """
    def __init__(self, flgset=set(), replace_with='__numeric'):
        super(NumberReplacer, self).__init__(
            regexes.IS_NUMERIC_PATT, replace_with, flgset
        )


class UrlReplacer(RegexReplacer):
    """
    Usage: Set all URL tokens to a uniform string
    """
    def __init__(self, flgset=set(), replace_with='__URL'):
        super(UrlReplacer, self).__init__(
            regexes.IS_URL, replace_with, flgset
        )


class TermWithPosReplacer(ReplacerBase):
    def __init__(self, flgset=set()):
        super(TermWithPosReplacer, self).__init__(flgset)
        self.to_on_from = {}

    def add_tkn_pos_transform(self, tkn, tag, to_tkn):
        self.to_on_from[(tkn, tag,)] = to_tkn

    def tkn_processor(self, tkn, tag):
        sptkn, flg = self.flg_splitr.split(tkn)
        rtkn = self.to_on_from.get( (sptkn, tag,), sptkn )
        return rtkn + flg, tag


class PunctuationStripReplacer(ReplacerBase):
    def __init__(self, flgset=set()):
        super(PunctuationStripReplacer, self).__init__(flgset)
        self.punc = string.punctuation

    def tkn_processor(self, tkn, tag=None):
        rtkn, flg = self.flg_splitr.split(tkn)
        return rtkn.strip(self.punc), tag


class NegationFlagger(object):
    def __init__(self, negs, method, flg='__neg'):
        # EG: negs = {'no', "n't", 'not', 'never'}
        self.negs = set(negs)
        self.method = method
        self.flg = flg

    def run(self, tkns, tags):
        indices = self.method(self.negs, tkns, tags)
        rtkns = list(tkns)
        for i in indices:
            rtkns[i] = rtkns[i] + self.flg
        return rtkns, tags


class StopWordFilter(FiltBase):
    """
    Usage: Remove stopwords
    """
    def __init__(self, stopwords, flgset=set()):
        super(StopWordFilter, self).__init__(flgset)
        self.stopwords = set(stopwords)

    def tkn_processor(self, tkn, tag=None):
        rtkn, flg = self.flg_splitr.split(tkn)
        if rtkn in self.stopwords:
            return True
        return False


class NumberFilter(FiltBase):
    """
    Usage: Remove numeric tokens.
    """
    def __init__(self, flgset=set()):
        super(NumberFilter, self).__init__(flgset)

    def tkn_processor(self, tkn, tag=None):
        rtkn, flg = self.flg_splitr.split(tkn)
        if regexes.IS_NUMERIC_PATT.match(rtkn):
            return True
        return False


class PunctuationFilter(FiltBase):
    def __init__(self, flgset=set()):
        super(PunctuationFilter, self).__init__(flgset)
        self.punc = set(string.punctuation)

    def tkn_processor(self, tkn, tag=None):
        # If all chars in a token are punctuation, return a "True" flag.
        rtkn, flg = self.flg_splitr.split(tkn)
        for i in rtkn:
            if i not in self.punc:
                return False
        return True


class PosWhitelistFilter(FiltBase):
    def __init__(self, wl):
        self.wl = set(wl)

    def tkn_processor(self, tkn, tag):
        if tag in self.wl:
            return False
        return True


class MatchGeneralizeTransformer(object):
    """
    This transformer does not support flag identification.
    As such, it must be run prior to any transform that flags tokens
    """
    def __init__(self, match_generalizer):
        self.mg = match_generalizer

    def run(self, tkns, tags=None):
        #print(self.mg.run(tkns, tags))
        ptkns, ptags = self.mg.run(tkns, tags)
        return ptkns, ptags


class CharSplitTransformer(object):
    def __init__(self, charset):
        self.charset = set(charset)

    def run(self, tkns, tags=None):
        if tags is None:
            tags = ['N'] * len(tkns)
        rtkns = []
        rtags = []
        for i in range(len(tkns)):
            split_tkn = ''
            split_pts = []
            for j in range(len(tkns[i])):
                if tkns[i][j] in self.charset:
                    split_tkn += '|*|'
                else:
                    split_tkn += tkns[i][j]
            segs = split_tkn.split('|*|')
            rtkns.extend(segs)
            rtags.extend([tags[i]] * len(segs))
        return rtkns, rtags



def run_pipeline(transformers, tkns, tags=None):
    if tags is None:
        tags = [None] * len(tkns)

    _tkns, _tags = list(tkns), list(tags)
    for tr in transformers:
        _tkns, _tags = tr.run(_tkns, _tags)
    return _tkns, _tags
