"""
P0:
	Explanation:
	Examples:
		not even all that good.
    	not good
P6:
	Examples:
    	None of the women are very prepared.
    		"are" and "prepared" is negated
    	None of the men are that timid.
    		"are" and timid" is negated
"""
NEGATION_V2 = r"""
P0: {<VBP|VBZ|VBD|MD>?<_NEG_R.*><PDT|RB.*|DT.*|IN|PRP\$>*<JJ.*>}
P1: {<VBP|VBZ|VBD|MD>?<_NEG_R.*><RB.*>?<VB.*><RB.*>*<JJ.*>?}
P4: {<_NEG_DT><NN.*>}
P5: {<_NEG_N.*><RB>*<VB.*><JJ>?}
P6: {<_NEG_N.*><IN><DT>?<NN.*|PRP><VB.*><RB|IN>?<JJ.*>?}
P7: {<_NEG_IN.*><DT>?<JJ>?<NN.*>}
P8: {<_NEG_DT.*|_NEG_R.*><JJ>?<NN.*>}
PX9: {<_NEG_N.*|_NEG_DT|_NEG_RB><_NEG_JJR><IN>?<JJ>}
P9: {<_NEG_JJR><IN>?<JJ>}
"""


PHRASES_V1 = r"""
NBAR: {<PRP.*|NN.*|JJ>*<NN.*|PRP.*>}
NP:
	{<NBAR>}
	{<NBAR><IN><NBAR>}
"""

SO_BAD = (
	PHRASES_V1 +
	r"P0: {<VB.*><NP>?<RB><JJ>}"
)