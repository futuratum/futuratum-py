import io
import csv
from lib.saxutil.txt_proc import synonym, lookahead


class MatchGeneralizer(object):
    '''
    A MatchGeneralizer instance remaps the values of a token or sequence of
    tokens to a different value. This is helpful when dealing with
    abbreviations and/or other languages. 
    '''
    def __init__(self, case_sensitive=False, default_pos='NN'):
        self.case_sensitive = case_sensitive
        self.default_pos = default_pos
        self.from_ngrams_on_start = {}
        self.to_dta_on_from_ngram = {}

    def add(self, from_ngram, to_ngram, to_tags=None):
        '''
        :type from_ngram: tuple<str>
        :type to_ngram: tuple<str>
        '''
        to_ngram, to_tags = synonym.preproc_tkns_tags(
            to_ngram, to_tags, self.default_pos
        )
        from_ngram = synonym.preproc_ngram(from_ngram, self.case_sensitive)
        to_ngram = synonym.preproc_ngram(to_ngram, True)
        synonym.check_for_key_err(from_ngram, self.to_dta_on_from_ngram)
        self.to_dta_on_from_ngram[from_ngram] = (to_ngram, to_tags)
        synonym.update_from_ngrams_on_start(from_ngram, self.from_ngrams_on_start)

    def match_bounds(self, tkns):
        if not self.case_sensitive:
            tkns = [t.lower() for t in tkns]
        return lookahead.non_subsumed_match_bounds(
            tkns, self.from_ngrams_on_start
        )

    def run(self, tkns, tags=None):
        tkns, tags = synonym.preproc_tkns_tags(tkns, tags)
        bnds = self.match_bounds(tkns)
        if len(bnds) == 0:
            return tkns, tags
        bnd_ix = 0
        proc_tkns, proc_tags = [], []
        i = 0
        while i < len(tkns):
            if bnd_ix >= len(bnds) or bnds[bnd_ix][0] != i:
                proc_tkns.append(tkns[i])
                proc_tags.append(tags[i])
                i += 1
                continue
            beg, end = bnds[bnd_ix]
            from_ngram = tuple(tkns[beg:end + 1])
            if not self.case_sensitive:
                from_ngram = tuple([t.lower() for t in from_ngram])
            to_ngram, to_tags = self.to_dta_on_from_ngram[from_ngram]
            proc_tkns.extend(to_ngram)
            proc_tags.extend(to_tags)
            i += len(from_ngram)
            bnd_ix += 1
        return proc_tkns, proc_tags

    @classmethod
    def dumps(cls, mg):
        si = io.StringIO()
        wtr = csv.writer(si, delimiter='\t')
        items = list(mg.to_dta_on_from_ngram.items())
        for from_ngram, to_tup in items:
            to_ngram, to_tags = to_tup
            row = [' '.join(from_ngram), ' '.join(to_ngram), ' '.join(to_tags)]
            wtr.writerow(row)
        return si.getvalue().strip()

    @classmethod
    def loads(cls, dta, case_sensitive=False):
        mg = MatchGeneralizer(case_sensitive=case_sensitive)
        si = io.StringIO(dta)
        rdr = csv.reader(si, delimiter='\t')
        for r in rdr:
            from_ngram = r[0].strip().split(' ')
            to_ngram = r[1].strip().split(' ')
            if len(r) == 2:
                to_tags = ['N'] * len(to_ngram)
            else:
                to_tags = r[2].strip().split(' ')
            mg.add(from_ngram, to_ngram, to_tags)
        return mg
