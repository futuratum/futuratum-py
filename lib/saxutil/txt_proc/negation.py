import io
import csv
import string
import spacy
import nltk
from lib.saxutil.txt_proc import chunk_grammar


def gen_tsv_corpus(texts, default_val=0):
    nlp = spacy.load(
        'en',
        add_vectors=False,
        entity=False,
        matcher=False,
        parser=False
    )
    si = io.StringIO()
    wtr = csv.writer(si, delimiter='\t')
    for txt in texts:
        txt = txt.replace('\t', ' ').replace('\n', ' ')
        doc = nlp(txt)
        tkns = [w.text for w in doc]
        """
        has_neg = False
        for t in tkns:
            if t in negs:
                has_neg = True
        if not has_neg:
            continue
        """
        tags = [w.tag_ for w in doc]
        for i in range(len(tkns)):
            wtr.writerow([tkns[i], tags[i], default_val])
        wtr.writerow([])
    return si.getvalue().strip()


def _mk_negation_pos_tags(negs, tkns, tags, tkns_to_append=set()):
    negtags = []
    for i in range(len(tags)):
        if tkns[i] in negs:
            negtags.append('_NEG_' + tags[i])
        elif tkns[i] in tkns_to_append:
            # Special Case
            negtags.append(tags[i] + '_' + tkns[i])
        else:
            negtags.append(tags[i])
    return negtags


class RuleBasedNegator(object):
    def __init__(self, negs, grammar):
        self.negs = negs
        self.prsr = nltk.RegexpParser(grammar)
        self._tags_on_treelbl = {}

    def set_negation_postags(self, treelbl, postags):
        self._tags_on_treelbl[treelbl] = set(postags)

    def run(self, tkns, tags):
        negtags = _mk_negation_pos_tags(self.negs, tkns, tags)
        matches = []
        tree = self.prsr.parse(
            [ (tkns[i], negtags[i],) for i in range(len(tkns)) ]
        )
        postree = tree.pos()
        for i in range(len(postree)):
            tkn, tag = postree[i][0]
            treelbl = postree[i][1]
            if tag in self._tags_on_treelbl.get(treelbl, set()):
                matches.append( (i, treelbl,) )
        return matches


def rule_based_v2(negs, tkns, tags, grammar=chunk_grammar.NEGATION_V2):
    negator = RuleBasedNegator(negs, grammar)
    jj_tags = {'JJ', 'JJR', 'JJS'}
    vb_tags = {'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ', 'MD'}
    nn_tags = {'NN', 'NNS', 'NNP', 'NNPS'}
    for treelbl in {'P0', 'P1', 'P5', 'P6'}:
        negator.set_negation_postags(treelbl, jj_tags.union(vb_tags))
    for treelbl in {'P7', 'P8'}:
        negator.set_negation_postags(treelbl, jj_tags.union(nn_tags))
    for treelbl in {'P4'}:
        negator.set_negation_postags(treelbl, nn_tags)
    #for treelbl in {'P9'}:
    #    negator.set_negation_postags(treelbl, jj_tags)
    return negator.run(tkns, tags)


def get_eval_dta_rule_based_v1(dta):
    tkns = []
    lbls = []
    prds = []
    negs = {
        'no', "n't", 'not', 'never', 'nobody', 'nothing', 'none', 'without'
    }
    si = io.StringIO(dta)
    rdr = csv.reader(si, delimiter='\t')
    sent_tkns, tags, sentlbls = [], [], []
    for r in rdr:
        if len(r) == 3:
            sent_tkns.append(r[0])
            tags.append(r[1])
            sentlbls.append(int(r[2]))
        if len(r) == 0:
            matches = rule_based_v1(negs, sent_tkns, tags)
            indices = [m[0] for m in matches]
            lbls.extend(sentlbls)
            tkns.extend(sent_tkns)
            sent_prds = [0 for i in range(len(sent_tkns))]
            for i in indices:
                sent_prds[i] = 1
            prds.extend(sent_prds)
            assert(len(lbls) == len(prds))
            sent_tkns, tags, sentlbls = [], [], []
        if len(r) == 1 and r[0] == '#END#':
            break
    return lbls, prds, tkns