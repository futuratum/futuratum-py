import unittest
import os
import io
import json
import uuid
import sqlite3
import pandas as pd
from lib.db.importers.doc import biz
from lib.tests.db.importers.doc import base as basetest


class TestBizDbImporter(unittest.TestCase):
    def setUp(self):
        dicts = [
            {'docid':0, 'ep':100, 'pid':1001, 'opid':1001, 'bk_ids':[]},
            {'docid':1, 'ep':200, 'pid':1002, 'opid':1001, 'bk_ids':[]},
            {'docid':2, 'ep':215, 'pid':1003, 'opid':1001, 'bk_ids':[]},
        ]
        basetest.setUp(self, dicts)

    def tearDown(self):
        basetest.tearDown(self)

    def test_run(self):
        importer = biz.BizMetaDbImporter(self.cnx)
        importer.fh = self.si
        importer.fileid = 1
        importer.run_nextline()
        importer.run_nextline()
        importer.run_nextline()
        self.assertEqual(importer.docmetarows[0], [0, 100, 1, 0])
        self.assertEqual(importer.docmetarows[1], [1, 200, 1, 0])
        self.assertEqual(importer.docmetarows[2], [2, 215, 1, 0])
        self.assertEqual(importer.bizmetarows[0], [1001, 0, 1001])
        self.assertEqual(importer.bizmetarows[1], [1002, 1, 1001])
        self.assertEqual(importer.bizmetarows[2], [1003, 2, 1001])

        importer.import_content()
        df = pd.read_sql('SELECT * FROM docmeta;', self.cnx)
        self.assertEqual(list(df['docid'].values), [0, 1, 2])
        self.assertEqual(list(df['ep'].values), [100, 200, 215])
        self.assertEqual(list(df['fileid'].values), [1, 1, 1])
        self.assertEqual(list(df['doctype_id'].values), [0, 0, 0])
        df = pd.read_sql('SELECT * FROM bizmeta;', self.cnx)
        self.assertEqual(list(df['docid'].values), [0, 1, 2])
        self.assertEqual(list(df['pid'].values), [1001, 1002, 1003])
        self.assertEqual(list(df['opid'].values), [1001, 1001, 1001])

        importer.clear_content()
        self.assertEqual(len(importer.docmetarows), 0)
        self.assertEqual(len(importer.bizmetarows), 0)


if __name__ == '__main__':
    unittest.main()
