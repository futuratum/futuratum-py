import unittest
import os
import io
import json
import uuid
import sqlite3
import pandas as pd
from lib.db.importers.doc import base
from lib.tests.db import mk_sqlite


def setUp(self, dicts):
    dta = ''
    for d in dicts:
        dta += json.dumps(d) + '\n'
    self.si = io.StringIO(dta)
    self.tmpfile = os.path.join('/tmp', str(uuid.uuid4()))
    self.cnx = sqlite3.connect(self.tmpfile)
    mk_sqlite.run(self.cnx)


def tearDown(self):
    os.remove(self.tmpfile)


class TestDocDbImporter(unittest.TestCase):
    def setUp(self):
        dicts = [
            {'docid':0, 'ep':100},
            {'docid':1, 'ep':200},
            {'docid':4, 'ep':250}
        ]
        setUp(self, dicts)

    def tearDown(self):
        tearDown(self)

    def test_run(self):
        importer = base.DocDbImporter(self.cnx, 8)
        importer.fh = self.si
        importer.fileid = 1
        importer.run_nextline()
        importer.run_nextline()
        importer.run_nextline()
        self.assertEqual(importer.docmetarows[0], [0, 100, 1, 8])
        self.assertEqual(importer.docmetarows[1], [1, 200, 1, 8])
        self.assertEqual(importer.docmetarows[2], [4, 250, 1, 8])
        importer.import_content()
        df = pd.read_sql('SELECT * FROM docmeta;', self.cnx)
        self.assertEqual(list(df['docid'].values), [0, 1, 4])
        self.assertEqual(list(df['ep'].values), [100, 200, 250])
        self.assertEqual(list(df['fileid'].values), [1, 1, 1])
        self.assertEqual(list(df['doctype_id'].values), [8, 8, 8])
        importer.clear_content()
        self.assertEqual(len(importer.docmetarows), 0)


if __name__ == '__main__':
    unittest.main()

