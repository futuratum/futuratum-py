import unittest
import os
import sqlite3
from lib.db.read import base
from lib.tests.db import mk_sqlite


class TestCompareConditions(unittest.TestCase):
    def test_single_val_compare_condition(self):
        self.assertEqual(
            base.SingleValCompareCondition('x', 'y').genr(),
            'x = y'
        )
        self.assertEqual(
            base.SingleValCompareCondition('x', 'y', '<').genr(),
            'x < y'
        )


    def test_two_val_arith_compare_condition(self):
        condition = base.TwoValArithCompareCondition(
            col0='x',
            arith_op='-',
            col1='y',
            val=5
        )
        self.assertEqual(condition.genr(), 'x - y = 5')
        condition = base.TwoValArithCompareCondition(
            col0='x',
            arith_op='+',
            col1='y',
            val=10,
            compare_op='>'
        )
        self.assertEqual(condition.genr(), 'x + y > 10')

    def test_in_set_condition(self):
        condition = base.InSetCondition('colVal', [5, 10, 15])
        cstr = condition.genr()
        elems = cstr.split('IN')
        self.assertEqual(elems[0].strip(), 'colVal')
        vals = elems[1].strip()[1:-1].split(',')
        vals = [int(i) for i in vals]
        self.assertEqual(set(vals), {5, 10, 15})


class TestMethods(unittest.TestCase):
    def setUp(self):
        self.tmpfile = os.path.join('/tmp', str(os.getpid()))
        self.cnx = sqlite3.connect(self.tmpfile)
        mk_sqlite.run(self.cnx)
        cur = self.cnx.cursor()

        cur.execute( 'INSERT INTO ASSET VALUES(0, "BTC");' )
        cur.execute( 'INSERT INTO ASSET VALUES(1, "ETH");' )
        cur.execute( 'INSERT INTO ASSET VALUES(2, "LSK");' )
        self.cnx.commit()

    def tearDown(self):
        os.remove(self.tmpfile)

    def test_as_dataframe(self):
        pipe = [base.SingleValCompareCondition('id', 0, '=')]
        stmt = base.mk_stmt(['assetname'], 'asset_quote', pipe)
        self.assertEqual(stmt, 'SELECT assetname FROM asset_quote WHERE id = 0;')
        df = base.as_dataframe(self.cnx, ['assetname'], 'asset', pipe)
        self.assertEqual(df['assetname'].values[0], 'BTC')

        pipe = [base.SingleValCompareCondition('id', 1, '>=')]
        df = base.as_dataframe(self.cnx, ['assetname'], 'asset', pipe)
        self.assertEqual(list(df['assetname'].values), ['ETH', 'LSK'])


if __name__ == '__main__':
    unittest.main()
