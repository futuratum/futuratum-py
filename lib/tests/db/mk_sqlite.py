import os
from lib.saxutil import path_util


def run(cnx):
	fp = os.path.join(path_util.basepath(), 'db/create_sqlite.sql')
	fp = fp.replace('/', os.sep)
	with open(fp) as f:
		dta = f.read()
	stmts = dta.split(';')
	cur = cnx.cursor()
	for stmt in stmts:
		stmt += ';'
		cur.execute(stmt.lower())
	cur.close()
	cnx.commit()


