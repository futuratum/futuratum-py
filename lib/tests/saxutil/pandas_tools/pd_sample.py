import unittest
import datetime
import pandas as pd
from lib.saxutil import matchrule
from lib.saxutil.pandas_tools import pd_sample


class TestMethods(unittest.TestCase):
    def test_draw_match_rows(self):
        # Generate the test DataFarme
        lil = [
            [datetime.datetime(2017, 1, 1, 0), 50, 5],
            [datetime.datetime(2017, 1, 1, 0), 45, 5],
            [datetime.datetime(2017, 1, 1, 0), 40, 5],
            [datetime.datetime(2017, 1, 1, 0), 30, 5],
            [datetime.datetime(2017, 1, 1, 1), 20, 5],
            [datetime.datetime(2017, 1, 2, 0), 50, 5],
            [datetime.datetime(2017, 1, 2, 0), 45, 5],
            [datetime.datetime(2017, 1, 2, 0), 40, 5],
            [datetime.datetime(2017, 1, 2, 0), 30, 5],
            [datetime.datetime(2017, 1, 2, 1), 20, 5],
        ]
        df = pd.DataFrame(
            lil, columns=['Datetime', 'Classic', 'Boneless']
        )
        df.index = pd.to_datetime(df['Datetime'])
        del df['Datetime']
        # Set the attributes that constitute a match
        matchrules_on_attr = {
            'weekday':matchrule.SetMatchRule({6,0}),
            'hour':matchrule.SetMatchRule({0})
        }
        odf = pd_sample.draw_match_rows(
            input_df=df,
            matchrules_on_attr=matchrules_on_attr,
            allow_partial_matches=False
        )
        exvals = (
            list(df['Classic'].values[:4]) +
            list(df['Classic'].values[5:-1])
        )
        self.assertEqual(list(odf['Classic'].values), exvals)


if __name__ == '__main__':
    unittest.main()

