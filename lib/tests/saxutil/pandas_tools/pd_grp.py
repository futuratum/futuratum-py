import unittest
import datetime
import random
import pandas as pd
from lib.saxutil.pandas_tools import pd_grp


class TestMethods(unittest.TestCase):
	def test_check_group_consistency(self):
		# St up first pass
		lil = []
		base_dt = datetime.datetime(2000, 1, 1, 0)
		for i in range(47):
			lil.append(
				[
					base_dt + datetime.timedelta(hours=i),
					1,
					random.randint(0, 10)
				]
			)
		df = pd.DataFrame(lil, columns=['dt', 'x', 'r'])
		df.index = pd.to_datetime(df['dt'])
		del df['dt']

		# First pass shouldn't match
		grps = list(df.groupby([df.index.date]))
		iscon = pd_grp.are_group_index_attrs_consistent(
			grps=grps, attrs=['hour', 'minute']
		)
		self.assertEqual(iscon, False)

		# Set up second pass
		lil.append(
			[
				base_dt + datetime.timedelta(hours=i+1),
				1,
				random.randint(0, 10)
			]
		)
		df = pd.DataFrame(lil, columns=['dt', 'x', 'r'])
		df.index = pd.to_datetime(df['dt'])
		del df['dt']

		# Second pass should match
		grps = list(df.groupby([df.index.date]))
		iscon = pd_grp.are_group_index_attrs_consistent(
			grps=grps, attrs=['hour', 'minute']
		)
		self.assertEqual(iscon, True)

if __name__ == '__main__':
	unittest.main()
