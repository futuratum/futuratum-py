import unittest
import datetime
import numpy as np
import pandas as pd
from lib.saxutil.pandas_tools import pd_util
from lib.saxutil.ts import second_units


class TestTransformers(unittest.TestCase):
    def test_apply_func(self):
        lil = [
            [0.1, 2],
            [0.9, 3],
            [0.3, 4]
        ]
        df = pd.DataFrame(lil, columns=['x', 'y'])
        def f(x):
            return 1 - x
        tr = pd_util.ApplyFuncTransformer(['x'], 'xa', f)
        dfo = tr.run(df)
        for i in range(len(dfo)):
            self.assertAlmostEqual(dfo['xa'].values[i], 1-dfo['x'].values[i])

        lil = [
            [1,  2, 1],
            [2,  4, 1],
            [4,  8, 1],
            [8, 16, 1],
        ]
        df = pd.DataFrame(lil, columns=['x', 'y', 'z'])

        def f(x):
            return x[0] / float(x[1])
        tr = pd_util.ApplyFuncTransformer(
            paramcols=['y', 'x'], otcol='ydivx', f=f
        )
        df = tr.run(df)
        self.assertEqual(list(df['ydivx'].values), [2, 2, 2, 2])

        tr = pd_util.ApplyFuncTransformer(
            paramcols=['y', 'z'], otcol='yplz', f=np.sum
        )
        df = tr.run(df)
        self.assertEqual(list(df['yplz'].values), [3, 5, 9, 17])

    def test_apply_subclasses(self):
        lil = [
            [1,  2, 1],
            [2,  4, 1],
            [4,  8, 1],
            [8, 16, 1],
        ]
        df = pd.DataFrame(lil, columns=['x', 'y', 'z'])
        tr = pd_util.ApplyDivisionTransformer('y', 'x', 'ydivx')
        df = tr.run(df)
        self.assertEqual(list(df['ydivx'].values), [2, 2, 2, 2])

        tr = pd_util.ApplyDivisionTransformer(
            col0='y', col1='x', otcol='ydivx_sm', smooth_col1=1
        )
        df = tr.run(df)
        self.assertEqual(
            list(df['ydivx_sm'].values),
            [2.0 / (1 + 1), 4.0 / (2 + 1), 8.0 / (4 + 1), 16.0 / (8 + 1)]
        )

        tr = pd_util.ApplySumTransformer(['y', 'z'], 'yplz')
        df = tr.run(df)
        self.assertEqual(list(df['yplz'].values), [3, 5, 9, 17])

    def test_rm_by_val(self):
        lil = [
            [1, 2],
            [2, 4],
            [3, 6],
            [1, 2],
            [2, 4],
            [3, 6],
        ]
        df = pd.DataFrame(lil, columns=['x', 'y'])

        tr = pd_util.RmByValTransformer('x', {1})
        odf = tr.run(df)
        self.assertEqual(list(odf['y'].values), [4, 6, 4, 6])
        self.assertEqual(list(odf['x'].values), [2, 3, 2, 3])

        tr = pd_util.RmByValTransformer('x', {2, 3})
        odf = tr.run(df)
        self.assertEqual(list(odf['y'].values), [2, 2])
        self.assertEqual(list(odf['x'].values), [1, 1])

        tr = pd_util.RmByValTransformer('y', {2})
        odf = tr.run(df)
        self.assertEqual(list(odf['x'].values), [2, 3, 2, 3])
        self.assertEqual(list(odf['y'].values), [4, 6, 4, 6])

        tr = pd_util.RmByValTransformer('y', {9})
        odf = tr.run(df)
        self.assertEqual(list(odf['x'].values), [1, 2, 3, 1, 2, 3])
        self.assertEqual(list(odf['y'].values), [2, 4, 6, 2, 4, 6])

    def test_rolling_agg_by_grp(self):
        lil = [
            [datetime.datetime(2017, 1, 1, 0, 0),   50, 20],
            [datetime.datetime(2017, 1, 2, 0, 0),  100, 20],
            [datetime.datetime(2017, 1, 3, 0, 0),  200, 20],
            [datetime.datetime(2017, 1, 4, 0, 0),  400, 20],
            [datetime.datetime(2017, 1, 5, 0, 0),  800, 20],

            [datetime.datetime(2017, 1, 1, 2, 0),  100, 20],
            [datetime.datetime(2017, 1, 2, 2, 0),  200, 20],
            [datetime.datetime(2017, 1, 3, 2, 0),  400, 20],
            [datetime.datetime(2017, 1, 4, 2, 0),  800, 20],
            [datetime.datetime(2017, 1, 5, 2, 0), 1600, 20],
        ]
        df = pd.DataFrame(lil, columns=['dt', 'x', 'y'])
        df.index = pd.to_datetime(df['dt'])
        del df['dt']

        tr = pd_util.RollingAggByGroupTransformer(
            refcol='x',
            otcol='xr',
            window=3,
            groupby=['hour', 'minute']
        )
        dfo = tr.run(df)
        self.assertEqual(
            list(dfo['xr'].dropna().values),
            [
                ( 200 + 100 +  50) / 3.0,
                ( 400 + 200 + 100) / 3.0,
                ( 400 + 200 + 100) / 3.0,
                ( 800 + 400 + 200) / 3.0,
                ( 800 + 400 + 200) / 3.0,
                (1600 + 800 + 400) / 3.0,
            ]
        )
        self.assertTrue(np.isnan(dfo.iloc[0]['xr']))
        self.assertTrue(np.isnan(dfo.iloc[1]['xr']))
        self.assertTrue(np.isnan(dfo.iloc[2]['xr']))
        self.assertTrue(np.isnan(dfo.iloc[3]['xr']))

        tr = pd_util.RollingAggByGroupTransformer(
            refcol='x',
            otcol='xr',
            window=3,
            groupby=['hour', 'minute'],
            min_periods=2
        )
        dfo = tr.run(df)
        self.assertTrue(np.isnan(dfo.iloc[0]['xr']))
        self.assertTrue(np.isnan(dfo.iloc[1]['xr']))
        self.assertEqual(
            list(dfo['xr'].values[2:]),
            [
                # 2017-01-01 00:00:00 (50)
                # 2017-01-02 00:00:00 (100) - CUR
                (50 + 100) / 2.0,
                # 2017-01-01 00:02:00 (100)
                # 2017-01-02 00:02:00 (200) - CUR
                (100 + 200) / 2.0,
                # 2017-01-01 00:00:00 (50)
                # 2017-01-02 00:00:00 (100)
                # 2017-01-03 00:00:00 (200) - CUR
                (50 + 100 + 200) / 3.0,
                # 2017-01-01 00:02:00 (100)
                # 2017-01-02 00:02:00 (200)
                # 2017-01-03 00:02:00 (400) - CUR
                (100 + 200 + 400) / 3.0,
                # 2017-01-02 00:00:00 (100)
                # 2017-01-03 00:00:00 (200)
                # 2017-01-04 00:00:00 (400) - CUR
                (100 + 200 + 400) / 3.0,
                # 2017-01-02 00:02:00 (200)
                # 2017-01-03 00:02:00 (400)
                # 2017-01-04 00:02:00 (800) - CUR
                (200 + 400 + 800) / 3.0,
                # 2017-01-03 00:00:00 (200)
                # 2017-01-04 00:00:00 (400)
                # 2017-01-05 00:00:00 (800) - CUR
                (200 + 400 + 800) / 3.0,
                # 2017-01-03 00:02:00 (400)
                # 2017-01-04 00:02:00 (800)
                # 2017-01-05 00:02:00 (1600) - CUR
                (400 + 800 + 1600) / 3.0,
            ]
        )

    def test_cumsum(self):
        lil = [
            [datetime.datetime(2017, 1, 1, 0,  0, 0), 50, 5],
            [datetime.datetime(2017, 1, 1, 0, 15, 0), 45, 5],
            [datetime.datetime(2017, 1, 1, 0, 30, 0), 40, 5],
            [datetime.datetime(2017, 1, 1, 0, 45, 0), 30, 5],
            [datetime.datetime(2017, 1, 1, 1, 15, 0), 20, 5]
        ]
        cols = ['Datetime', 'Classic', 'Boneless']
        df = pd.DataFrame(lil, columns=cols)
        df.index = df['Datetime']
        del df['Datetime']

        tr = pd_util.CumSumTransformer(
            'Classic', 'ClassicCumSum', ['date']
        )
        odf = tr.run(df)
        self.assertEqual(list(odf['Classic'].values), list(df['Classic'].values))
        self.assertEqual(
            list(odf['ClassicCumSum'].values),
            [0, 50, 95, 135, 165]
        )

        tr = pd_util.CumSumTransformer(
            'Boneless', 'BonelessCumSum', ['date']
        )
        odf = tr.run(odf)
        self.assertEqual(
            list(odf['BonelessCumSum'].values), [0, 5, 10, 15, 20]
        )
        self.assertEqual(
            list(odf['Boneless'].values), list(df['Boneless'].values)
        )

    def test_shift_col(self):
        lil = [
            [1, 2],
            [2, 3],
            [3, 4],
            [4, 5],
        ]
        df = pd.DataFrame(lil, columns=['x', 'y'])
        tr = pd_util.ShiftColTransformer(1, 'x', 'xl1')
        df= tr.run(df)
        self.assertEqual(list(df['xl1'][1:].values), list(df['x'][:3].values))
        tr = pd_util.ShiftColTransformer(2, 'x', 'xl2')
        df = tr.run(df)
        self.assertEqual(list(df['xl2'][2:].values), list(df['x'][:2].values))

    def test_group_based_shift_col(self):
        lil = []
        for i in {1,2,3}:
            lil.extend([
                [datetime.datetime(2000, 1, i, 11,  0, 0), 10],
                [datetime.datetime(2000, 1, i, 11, 15, 0), 20],
                [datetime.datetime(2000, 1, i, 11, 30, 0), 30],
                [datetime.datetime(2000, 1, i, 11, 45, 0), 40],
            ])
        df = pd.DataFrame(lil, columns=['dt', 'x'])
        df.index = pd.to_datetime(df['dt'])
        del df['dt']
        tr = pd_util.GroupBasedShiftColTransformer(
            shiftby=1,
            refcol='x',
            otcol='xs',
            groupby=['date'],
        )
        odf = tr.run(df)
        self.assertEqual(
            list(odf.dropna()['xs'].values), [10, 20, 30] * 3
        )
        for i in {0, 4, 8}:
            self.assertTrue(np.isnan(odf['xs'].values[i]))

    def test_autoreg_shift_pipe(self):
        lil = [
            [1, 2],
            [2, 3],
            [3, 4],
            [4, 5],
        ]
        df = pd.DataFrame(lil, columns=['x', 'y'])
        pipe = pd_util.mk_autoreg_shift_pipe(
            refcol='x', minshift=1, maxshift=2
        )
        odf = pd_util.run_transform_pipeline(pipe, df)
        self.assertTrue(np.isnan(odf['x_minus1'].values[0]))
        self.assertTrue(np.isnan(odf['x_minus2'].values[0]))
        self.assertTrue(np.isnan(odf['x_minus2'].values[1]))
        self.assertEqual(list(odf['x_minus1'].values[1:]), [1, 2, 3])
        self.assertEqual(list(odf['x_minus2'].values[2:]), [1, 2])

        lil = []
        for i in {1,2,3}:
            lil.extend([
                [datetime.datetime(2000, 1, i, 11,  0, 0), 10],
                [datetime.datetime(2000, 1, i, 11, 15, 0), 20],
                [datetime.datetime(2000, 1, i, 11, 30, 0), 30],
                [datetime.datetime(2000, 1, i, 11, 45, 0), 40],
            ])
        df = pd.DataFrame(lil, columns=['dt', 'x'])
        df.index = pd.to_datetime(df['dt'])
        del df['dt']
        pipe = pd_util.mk_autoreg_shift_pipe(
            refcol='x', minshift=1, maxshift=2, groupby=['date']
        )

        odf = pd_util.run_transform_pipeline(pipe, df)

        self.assertEqual(
            list(odf['x_minus1'].dropna().values), [10, 20, 30] * 3
        )
        for i in {0, 4, 8}:
            self.assertTrue(np.isnan(odf['x_minus1'].values[i]))

        self.assertEqual(
            list(odf['x_minus2'].dropna().values), [10, 20] * 3
        )
        for i in {0, 1, 4, 5, 8, 9}:
            self.assertTrue(np.isnan(odf['x_minus2'].values[i]))

    def test_add_agg_col(self):
        lil = [
            [1,  2, 1],
            [2,  4, 1],
            [4,  8, 1],
            [8, 16, 1],
        ]
        df = pd.DataFrame(lil, columns=['x', 'y', 'z'])
        tr = pd_util.AddAggColTransformer(['x', 'y'], 'xyagg', sum)
        df = tr.run(df)
        self.assertEqual(list(df['xyagg'].values), [3, 6, 12, 24])

    def test_expected_col_val(self):
        lil = [
            [datetime.datetime(2015, 1, 1, 1, 15,  0), 20],
            [datetime.datetime(2015, 1, 1, 1, 15, 30), 0 ],
            [datetime.datetime(2015, 1, 1, 1, 45,  0), 0 ],
            [datetime.datetime(2015, 1, 2, 1, 15,  0), 20],
            [datetime.datetime(2015, 1, 2, 1, 30,  0), 30],
            [datetime.datetime(2015, 1, 3, 1, 30,  0), 30],
            [datetime.datetime(2015, 1, 3, 1, 45,  0), 10],
        ]
        df = pd.DataFrame(lil, columns=['dt', 'x'])
        df.index = pd.to_datetime(df['dt'])
        del df['dt']
        tr = pd_util.ExpectedGrpValTransfromer(
            'x', 'xe', ['hour', 'minute']
        )
        odf = tr.run(df)
        self.assertEqual(
            list(odf['xe'].values), [20, 20, 5, 20, 30, 30, 5]
        )

    def test_time_delta_transformer(self):
        lil = [
            [datetime.datetime(2000, 1,  1), 1],
            [datetime.datetime(2000, 1,  8), 2],
            [datetime.datetime(2000, 1, 15), 3],
            [datetime.datetime(2000, 1, 16), 0],
            [datetime.datetime(2000, 1, 22), 4],
            [datetime.datetime(2000, 1, 29), 5],
        ]
        df = pd.DataFrame(lil, columns=['dt', 'x'])
        df.index = pd.to_datetime(df['dt'])
        tr = pd_util.TimeDeltaTransformer(
            'x', 'xlw', second_units.WEEK
        )
        dfo = tr.run(df)
        self.assertEqual(dfo.loc[datetime.datetime(2000, 1,  8)]['xlw'], 1)
        self.assertEqual(dfo.loc[datetime.datetime(2000, 1, 15)]['xlw'], 2)
        self.assertTrue(
            np.isnan(dfo.loc[datetime.datetime(2000, 1, 16)]['xlw'])
        )
        self.assertEqual(dfo.loc[datetime.datetime(2000, 1, 22)]['xlw'], 3)
        self.assertEqual(dfo.loc[datetime.datetime(2000, 1, 29)]['xlw'], 4)
        self.assertEqual(list(dfo['x'].values), [1, 2, 3, 0, 4, 5])

    def test_next_match_delta(self):
        # Test with integer index
        lil = [
            [1, False],
            [2, False],
            [3, True],
            [4, False],
            [5, False],
            [6, False],
            [7, True],
        ]
        df = pd.DataFrame(lil, columns=['x', 'b'])
        tr = pd_util.NextMatchDeltaTransformer('b', {True}, 'd', 5)
        dfo = tr.run(df)
        self.assertEqual(list(dfo['d'].values), [2, 1, 0, 3, 2, 1, 0])

        lil = [
            [1, False],
            [2, False],
            [3, False],
        ]
        df = pd.DataFrame(lil, columns=['x', 'b'])
        dfo = tr.run(df)
        self.assertEqual(list(dfo['d'].values), [5, 5, 5])

        tr = pd_util.NextMatchDeltaTransformer(
            'b', {True}, 'd', pd.Timedelta(days=3)
        )
        # Test with datetime index
        lil = [
            [datetime.datetime(2000, 1, 1), False],
            [datetime.datetime(2000, 1, 3), True],
            [datetime.datetime(2000, 1, 4), False],
            [datetime.datetime(2000, 1, 5, 12), True],
            [datetime.datetime(2000, 1, 6), False]
        ]
        df = pd.DataFrame(lil, columns=['dt', 'b'])
        df.index = pd.to_datetime(df['dt'])
        del df['dt']
        dfo = tr.run(df)
        tdenom = np.timedelta64(1, 's')
        self.assertEqual(dfo['d'].values[0] / tdenom, 3600 * 48)
        self.assertEqual(dfo['d'].values[1] / tdenom, 0)
        self.assertEqual(dfo['d'].values[2] / tdenom, 3600 * 36)
        self.assertEqual(dfo['d'].values[3] / tdenom, 0)
        # No upcoming matches
        self.assertEqual(dfo['d'].values[4] / tdenom, 3600 * 72)

        tr = pd_util.NextMatchDeltaTransformer(
            'b', {True}, 'd', pd.Timedelta(days=3), right=True
        )
        dfo = tr.run(df)
        # No previous matches
        self.assertEqual(dfo['d'].values[0] / tdenom, 3600 * 72)
        self.assertEqual(dfo['d'].values[1] / tdenom, 0)
        # Match 24 hours ago
        self.assertEqual(dfo['d'].values[2] / tdenom, 3600 * 24)
        # Match
        self.assertEqual(dfo['d'].values[3] / tdenom, 0)
        # Match 12 hours ago
        self.assertEqual(dfo['d'].values[4] / tdenom, 3600 * 12)

    def test_time_delta_to_seconds(self):
        lil = [
            [pd.Timedelta(days=1)],
            [pd.Timedelta(days=1, hours=12)],
            [pd.Timedelta(days=1, hours=18)]
        ]
        df = pd.DataFrame(lil, columns=['dt'])
        tr = pd_util.TimeDeltaToSecondsTransformer('dt', 'dts')
        dfo = tr.run(df)
        self.assertEqual(
            list(dfo['dts'].values),
            [
                second_units.DAY,
                second_units.DAY + (second_units.HOUR * 12),
                second_units.DAY + (second_units.HOUR * 18),
            ]
        )

    def test_apply_datetime_values(self):
        lil = []
        daytime_dict = {}
        for x in {1, 2}:
            for i in range(2):
                for j in range(4):
                    lil.append(
                        [datetime.datetime(2000, 1, x, i, j*15, 0), 0]
                    )
                    ixlist = [str(i).zfill(2), str(j*15).zfill(2), str('00')]
                    ixstr = ':'.join(ixlist)
                    # This will set the daytime_dict entry equal to the
                    # current hour quarter when 0 indexed.
                    # 0: 0
                    # 1: 15
                    # 2: 30
                    # 3: 45
                    daytime_dict[ixstr] = j
        df = pd.DataFrame(lil, columns=['dt', 'x'])
        df.index = pd.to_datetime(df['dt'])
        del df['dt']
        tr = pd_util.AddDaytimeValuesTransform('dv', daytime_dict)
        odf = tr.run(df)
        # Check that the index is the same
        self.assertEqual(list(df.index.values), list(odf.index.values))
        # Check that the daytime values are as expected
        self.assertEqual(
            list(odf['dv'].values), [0, 1, 2, 3] * (len(df) // 4)
        )
        grps = list(odf.groupby([odf.index.minute, odf.index.second]))
        for i in range(len(grps)):
            self.assertEqual(set(grps[i][1]['dv']), {i})

    def test_date_flag(self):
        lil = [
            [datetime.date(2017, 1, 1), 1],
            [datetime.date(2017, 1, 5), 2],
            [datetime.date(2017, 1, 9), 3],
        ]
        df = pd.DataFrame(lil, columns=['dt', 'x'])
        df.index = pd.to_datetime(df['dt'])
        del df['dt']
        tr = pd_util.DateFlagTransformer(
            otcol='FLG',
            dates={datetime.date(2017, 1, 5)}
        )
        odf = tr.run(df)
        self.assertEqual(list(odf['FLG'].values), [False, True, False])

    def test_datetime_flag(self):
        lil = [
            [datetime.datetime(2017, 1, 1, 0, 0), 1],
            [datetime.datetime(2017, 1, 5, 0, 0), 2],
            [datetime.datetime(2017, 1, 5, 1, 0), 3],
            [datetime.datetime(2017, 1, 9, 0, 0), 4],
        ]
        df = pd.DataFrame(lil, columns=['dt', 'x'])
        df.index = pd.to_datetime(df['dt'])
        del df['dt']
        tr = pd_util.DatetimeFlagTransformer(
            otcol='FLG',
            dts={datetime.datetime(2017, 1, 5, 0, 0)},
        )
        odf = tr.run(df)
        self.assertEqual(
            list(odf['FLG'].values), [False, True, False, False]
        )


if __name__ == '__main__':
    unittest.main()