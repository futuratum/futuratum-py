import unittest
import datetime
import numpy as np
import pandas as pd
from lib.saxutil import matchrule
from lib.saxutil.pandas_tools.transform import tr_match


class TestMethods(unittest.TestCase):
    def test_index_attribute_match(self):
        # Generate the test DataFarme
        lil = [
            [datetime.datetime(2017, 1, 1, 0), 50, 5],
            [datetime.datetime(2017, 1, 1, 0), 45, 5],
            [datetime.datetime(2017, 1, 1, 0), 40, 5],
            [datetime.datetime(2017, 1, 1, 0), 30, 5],
            [datetime.datetime(2017, 1, 1, 1), 20, 5],
            [datetime.datetime(2017, 1, 2, 0), 50, 5],
            [datetime.datetime(2017, 1, 2, 0), 45, 5],
            [datetime.datetime(2017, 1, 2, 0), 40, 5],
            [datetime.datetime(2017, 1, 2, 0), 30, 5],
            [datetime.datetime(2017, 1, 2, 1), 20, 5],
        ]
        for i in range(len(lil)):
            lil[i].append(False)
        df = pd.DataFrame(
            lil, columns=['Datetime', 'Classic', 'Boneless', 'FLG']
        )
        df.index = pd.to_datetime(df['Datetime'])
        del df['Datetime']

        ### Test full match with weekday and hour
        # Set the attributes that constitute a match
        matchrules_on_attr = {
            'weekday':matchrule.SetMatchRule({6,0}),
            'hour':matchrule.SetMatchRule({0})
        }
        # matchvals_on_attr = {'weekday':{6,0}, 'hour':{0}}
        # initialize the transformer
        tr = tr_match.IndexAttrMatchFlagTransformer(
            flgcol='FLG',
            matchrules_on_attr=matchrules_on_attr,
            flgval=True,
            allow_partial_matches=False,
        )
        df = tr.run(df)
        self.assertEqual(
            list(df['FLG'].values), [True, True, True, True, False] * 2
        )

        ### Test partial matches w/ weekday and hour
        # Reset dataframe flag values
        df['FLG'] = [False] * len(df)
        assert(set(df['FLG']) == {False})
        tr.allow_partial_matches = True
        df = tr.run(df)
        self.assertEqual(list(df['FLG'].values), [True] * 10)

        ### Test matching on dates
        # Reset dataframe flag values
        df['FLG'] = [False] * len(df)
        assert(set(df['FLG']) == {False})
        matchrules_on_attr = {
            'date':matchrule.SetMatchRule(set([datetime.date(2017, 1, 1)]))
        }
        tr = tr_match.IndexAttrMatchFlagTransformer(
            flgcol='FLG',
            matchrules_on_attr=matchrules_on_attr,
            flgval=True,
            allow_partial_matches=False
        )
        df = tr.run(df).sort_index()
        self.assertEqual(list(df['FLG'].values), ([True] * 5) + ([False] * 5))


if __name__ == '__main__':
	unittest.main()