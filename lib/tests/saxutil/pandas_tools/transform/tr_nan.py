import unittest
import datetime
import numpy as np
import pandas as pd
from lib.saxutil.pandas_tools.transform import tr_nan


class TestMethods(unittest.TestCase):
    def test_trim_nan(self):
        lil = [
            [np.nan, 1, 0],
            [np.nan, 1, 0],
            [     0, 1, 0],
            [     0, 1, 0],
            [     0, 1, 0],
            [np.nan, 1, 0],
        ]
        df = pd.DataFrame(lil, columns=['x', 'y', 'z'])

        tr = tr_nan.TrimNanTransformer('x', frombeg=True, fromend=False)
        odf = tr.run(df)
        self.assertTrue(np.isnan(odf['x'].values[-1]))
        self.assertEqual(list(odf['x'].values[:-1]), [0, 0, 0])

        tr = tr_nan.TrimNanTransformer('x', frombeg=True, fromend=True)
        odf = tr.run(df)
        self.assertEqual(list(odf['x'].values), [0, 0, 0])

        tr = tr_nan.TrimNanTransformer('x', frombeg=False, fromend=True)
        odf = tr.run(df)
        self.assertTrue(np.isnan(odf['x'].values[0]))
        self.assertTrue(np.isnan(odf['x'].values[1]))
        self.assertEqual(list(odf['x'].values[2:]), [0, 0, 0])

        dts = [
            datetime.datetime(2000, 1, 1),
            datetime.datetime(2000, 1, 2),
            datetime.datetime(2000, 1, 3),
            datetime.datetime(2000, 1, 4),
            datetime.datetime(2000, 1, 5),
            datetime.datetime(2000, 1, 6),
        ]
        df.index = pd.to_datetime(dts)
        dfo = tr.run(df)
        self.assertTrue(np.isnan(odf['x'].values[0]))
        self.assertTrue(np.isnan(odf['x'].values[1]))
        self.assertEqual(list(odf['x'].values[2:]), [0, 0, 0])

    def test_fill_nan(self):
        lil = [
            [5, 2, np.nan],
            [1, 2, 3],
            [1, 2, 3],
            [5, 2, np.nan]
        ]
        df = pd.DataFrame(lil, columns=['x', 'y', 'z'])
        tr = tr_nan.FillNanTransformer(
            refcol='z', otcol='z', fill_val_col='x'
        )
        dfo = tr.run(df)
        self.assertEqual(list(dfo['z'].values), [5, 3, 3, 5])


if __name__ == '__main__':
	unittest.main()