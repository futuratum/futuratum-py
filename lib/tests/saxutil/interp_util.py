import unittest
from lib.saxutil import interp_util


class TestConditionInterpolater(unittest.TestCase):
    def test_run(self):
        cint = interp_util.ConditionInterp()
        self.assertEqual(cint.get_interp_val({'x':12, 'y':15}), None)
        cint.add_condition({'x':12, 'y':15}, 20)
        self.assertEqual(cint.get_interp_val({'x':12, 'y':15}), 20)

        self.assertEqual(cint.get_interp_val({'x':22, 'y':15}), None)
        cint.add_condition({'x':22, 'y':15}, 30)
        self.assertEqual(cint.get_interp_val({'x':22, 'y':15}), 30)


if __name__ == '__main__':
    unittest.main()
