import unittest
from lib.saxutil import bow
from sklearn.feature_extraction.text import TfidfTransformer


class TestBow(unittest.TestCase):
	def test_mk_wt_map(self):
		docs = [
			['i', 'am', 'close', 'to', 'new', 'york'],
			['i', 'am', 'close', 'to', 'toronto'],
			['i', 'am', 'close', 'to', 'dallas'],
			['i', 'am', 'close', 'to', 'florida']
		]
		bag = bow.BagOfWords()
		for d in docs:
			bag.add_obj_list_doc(d)
		wm = bag.mk_idf_wt_map()

		wts0 = set()
		for w in {'new', 'york', 'toronto', 'dallas', 'florida'}:
			wts0.add(wm.get_obj_wt(w))
		self.assertEqual(len(wts0), 1)

		wts1 = set()
		for w in {'i', 'am', 'close', 'to'}:
			wts1.add(wm.get_obj_wt(w))
		self.assertEqual(len(wts1), 1)

		# make sure the weights for high frequency words are lower than
		# the weights of low frequency words.
		w = [list(wts0)[0]] + [list(wts1)[0]]
		self.assertTrue(w[0] > w[1])
		


if __name__ == '__main__':
	unittest.main()
