import unittest
from lib.saxutil.txt_proc import twitter


class TestMethods(unittest.TestCase):
    def test_split_on_hashtags(self):
        tkns = 'i am #lovingit today !'.split()
        sublists, tag_sublists = twitter.split_on_hashtags(tkns)
        ex_sublists = [['i', 'am'], ['#lovingit'], ['today', '!']]
        self.assertEqual(sublists, ex_sublists)
        ex_tag_sublists = [ [None] * 2, [None], [None] * 2]
        self.assertEqual(tag_sublists, ex_tag_sublists)

        tkns = '''
        he is a #monster going to #losemymind i'm fucking
        #appalled
        '''.split()
        sublists, tag_sublists = twitter.split_on_hashtags(tkns)
        ex_sublists = [
            'he is a'.split(),
            '#monster'.split(),
            'going to'.split(),
            '#losemymind'.split(),
            'i\'m fucking'.split(),
            '#appalled'.split(),
        ]
        self.assertEqual(sublists, ex_sublists)

    def test_segment_sublist_hashtags(self):
        in_tkn_sublists = [
            'i am'.split(),
            '#soappalled'.split(),
            'today'.split()
        ]
        in_tag_sublists = [ ['NN', 'VB'], ['HT'], ['VBD'] ]
        ex_tkn_sublists = list(in_tkn_sublists)
        ex_tkn_sublists[1] = 'so appalled'.split()
        ex_tag_sublists = list(in_tag_sublists)
        ex_tag_sublists[1] = ['HT', 'HT']
        ot_tkn_sublists, ot_tag_sublists = (
            twitter.segment_sublist_hashtags(in_tkn_sublists, in_tag_sublists)
        )
        self.assertEqual(ot_tkn_sublists, ex_tkn_sublists)
        self.assertEqual(ot_tag_sublists, ex_tag_sublists)


class TestTwitterUserNameReplacer(unittest.TestCase):
    def test_run(self):
        unr = twitter.UserNameReplacer()
        tkn, tag = unr.tkn_processor('@username_x')
        self.assertEqual(tkn, unr.replace_with)


class TestTwitterPreprocessor(unittest.TestCase):
    def test_run(self):
        tpp = twitter.TwitterPreprocessor(['love', 'bad'])
        srctxt = '''
        @user i loooovvvve your work #keepitup ur gr8
        '''
        extxt = '''
        @user i love your work . keep it up . ur gr8 .
        '''.strip()
        self.assertEqual(tpp.run(srctxt), extxt)

        srctxt = '''
        not tonight #service #heroes too baaaddd i missed it
        '''.strip()
        extxt = 'not tonight . service . heroes . too bad i missed it .'
        self.assertEqual(tpp.run(srctxt), extxt)

if __name__ == '__main__':
    unittest.main()
