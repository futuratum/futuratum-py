import unittest
import spacy
from lib.saxutil.txt_proc import negation


NLP = spacy.load(
    'en',
    add_vectors=False,
    entity=False,
    matcher=False,
    parser=False
)

def convert_to_tkns_and_tags(nlp, txtdoc):
    doc = nlp.tokenizer(txtdoc)
    nlp.tagger(doc)
    return [w.text for w in doc], [w.tag_ for w in doc]


class TestRuleBased(unittest.TestCase):
    def test_run(self):
        negs = {
            'no', 'not', "n't", 'never', 'nobody', 'nothing', 'none',
            'without'
        }
        
        doc = 'i do not care about that'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('do'), 'P1',))
        self.assertEqual(matches[1], (tkns.index("care"), 'P1',))

        doc = 'i don\'t want to be sad'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('do'), 'P1',))
        self.assertEqual(matches[1], (tkns.index("want"), 'P1',))

        doc = 'i can not be sad about his actions today'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('can'), 'P1',))
        self.assertEqual(matches[1], (tkns.index('be'), 'P1',))
        self.assertEqual(matches[2], (tkns.index('sad'), 'P1',))

        doc = 'he will never get better at it'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('will'), 'P1',))
        self.assertEqual(matches[1], (tkns.index('get'), 'P1',))
        self.assertEqual(matches[2], (tkns.index('better'), 'P1',))

        doc = 'it is not her best work'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('is'), 'P0',))
        self.assertEqual(matches[1], (tkns.index('best'), 'P0',))

        doc = 'it is not my favorite meal'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual( matches[0], (tkns.index('is'), 'P0',) )
        self.assertEqual( matches[1], (tkns.index('favorite'), 'P0',) )

        doc = 'i have no respect left for him'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('respect'), 'P4',))

        doc = 'nobody really knew anything'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('knew'), 'P5',))

        doc = 'nobody knew anything worth mentioning'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('knew'), 'P5',))

        doc = 'none of the men are ready'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('are'), 'P6',))
        self.assertEqual(matches[1], (tkns.index('ready'), 'P6',))

        doc = 'none of the women are currently present'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('are'), 'P6',))
        self.assertEqual(matches[1], (tkns.index('present'), 'P6',))
        
        doc = 'none of it is good for anything big'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('is'), 'P6',))
        self.assertEqual(matches[1], (tkns.index('good'), 'P6',))

        doc = 'nobody is ready for it'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('is'), 'P5',))
        self.assertEqual(matches[1], (tkns.index('ready'), 'P5',))

        doc = 'the film is not that compelling'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('is'), 'P0',))
        self.assertEqual(matches[1], (tkns.index('compelling'), 'P0',))

        # ! KNOWN ERROR CASE !
        doc = 'her car has never looked so good before'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('has'), 'P1',))
        self.assertEqual(matches[1], (tkns.index('looked'), 'P1',))
        self.assertEqual(matches[2], (tkns.index('good'), 'P1',))

        doc = 'the film is not as compelling as i had hoped'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('is'), 'P0',))
        self.assertEqual(matches[1], (tkns.index('compelling'), 'P0',))

        doc = 'he acted without any consideration'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('consideration'), 'P7',))

        doc = 'he acted with no good morals'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('good'), 'P8',))
        self.assertEqual(matches[1], (tkns.index('morals'), 'P8',))
        
        doc = 'her work is not all that good'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('is'), 'P0',))
        self.assertEqual(matches[1], (tkns.index('good'), 'P0',))

        doc = 'it was not even all that good'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('was'), 'P0',))
        self.assertEqual(matches[1], (tkns.index('good'), 'P0',))

        doc = 'it was not all that good for the right reasons'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('was'), 'P0',))
        self.assertEqual(matches[1], (tkns.index('good'), 'P0',))

        doc = 'she could not even consider that possibility'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('could'), 'P1'))
        self.assertEqual(matches[1], (tkns.index('consider'), 'P1'))

        doc = 'it is no less than perfect'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(len(matches), 0)

        """
        doc = 'it was less than perfect'
        tkns, tags = convert_to_tkns_and_tags(NLP, doc)
        matches = negation.rule_based_v2(negs, tkns, tags)
        self.assertEqual(matches[0], (tkns.index('perfect'), 'P9',))
        """

if __name__ == '__main__':
    unittest.main()