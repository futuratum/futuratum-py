import unittest
import warnings
import spacy
from nltk import stem
from lib.saxutil.txt_proc import tkn_transform, synonym, match_generalizer


class DummySpacyWord(object):
    def __init__(self, tkn, tag):
        self.text = tkn
        self.tag_ = tag


class TestStemTransformer(unittest.TestCase):
    def test_run(self):
        ps = stem.PorterStemmer()

        # Test w/o flag or POS
        tkns = ['i', 'am', 'going', 'further']
        extkns = [ps.stem(t) for t in tkns]
        sr = tkn_transform.StemReplacer()
        self.assertEqual(sr.run(tkns)[0], extkns)
        self.assertEqual(sr.run(tkns)[1], [None] * 4)

        # Test w/ flag, w/o POS
        tkns = ['i', 'am_NEG', 'going_NEG', 'further_NEG']
        extkns = ['i', 'am_NEG', 'go_NEG', 'further_NEG']
        sr = tkn_transform.StemReplacer(flgset={'_NEG'})
        self.assertEqual(sr.run(tkns)[0], extkns)
        self.assertEqual(sr.run(tkns)[1], [None] * 4)


class TestLemmaReplacer(unittest.TestCase):
    def setUp(self):
        warnings.simplefilter('ignore', ResourceWarning)

    def tearDown(self):
        warnings.simplefilter('always')

    def test_run(self):
        tkns = ['defended', 'home', 'destroying']
        tags = ['VBP', 'NN', 'VBG']
        lr = tkn_transform.LemmaReplacer()
        rtkns, rtags = lr.run(tkns, tags)
        self.assertEqual(rtkns, ['defend', 'home', 'destroy'])
        self.assertEqual(rtags, tags)


class TestNumberReplacer(unittest.TestCase):
    def test_run(self):
        tkns = ['this', 'happened', '100', 'times']
        extkns = ['this', 'happened', '__numeric', 'times']
        rep = tkn_transform.NumberReplacer()
        self.assertEqual(rep.run(tkns)[0], extkns)
        spdoc = [ DummySpacyWord(tkns[i], 'NN',) for i in range(len(tkns)) ]
        rep.spacyrun(spdoc)
        rtkns = [w.text for w in spdoc]
        self.assertEqual(extkns, rtkns)


class TestUrlReplacer(unittest.TestCase):
    def test_run(self):
        # this also tests the flag splitter on a spacy run
        tkns = ['visit__neg', 'http://google.com', 'to', 'search']
        extkns = ['visit__neg', '__URL', 'to', 'search']
        rep = tkn_transform.UrlReplacer(flgset={'__neg'})
        spdoc = [ DummySpacyWord(tkns[i], 'NN',) for i in range(len(tkns)) ]
        rep.spacyrun(spdoc)
        rtkns = [w.text for w in spdoc]
        self.assertEqual(extkns, rtkns)


class TestTermWithPosReplacer(unittest.TestCase):
    def test_run(self):
        rep = tkn_transform.TermWithPosReplacer()
        rep.add_tkn_pos_transform('like', 'IN', 'preplike')
        tkns = ['I', 'like', 'that']
        tags = ['PRP', 'VBZ', 'NN']
        rtkns, rtags = rep.run(tkns, tags)
        self.assertEqual(rtkns, tkns)
        self.assertEqual(rtags, tags)
        tkns = ['it', 'is', 'not', 'like', 'that']
        tags = ['NN', 'VB', 'RB', 'IN', 'NN']
        rtkns, rtags = rep.run(tkns, tags)
        self.assertEqual(rtkns, ['it', 'is', 'not', 'preplike', 'that'])
        self.assertEqual(rtags, tags)


class TestLetterRepReplacer(unittest.TestCase):
    def test_run(self):
        tkns = ['this', 'is', 'sooooo', 'baaaddd']
        tags = ['NN', 'VB', 'RB', 'JJ']
        extkns = ['this', 'is', 'soo', 'baadd']
        lrr = tkn_transform.LetterRepReplacer()
        rtkns, rtags = lrr.run(tkns, tags)
        self.assertEqual(rtkns, extkns)


class TestLetterRepVocabReplacer(unittest.TestCase):
    def test_run(self):
        nlp = spacy.load(
            'en',
            add_vectors=False,
            entity=False,
            matcher=False,
            parser=False,
        )
        tkns = [
            'this', 'is', 'soo', 'veerrry__neg', 'goooooooddd__neg',
            'loosseer'
        ]
        tags = ['NN', 'VB', 'VB', 'RB', 'JJ', 'NN']
        extkns = ['this', 'is', 'so', 'very__neg', 'good__neg', 'loser']
        lrvr = tkn_transform.LetterRepVocabReplacer(
            nlp.vocab, flgset={'__neg'}
        )
        lrvr.add_postag_conflict_rule('good', 'JJ')
        lrvr.add_postag_conflict_rule('god', 'NN')
        lrvr.add_postag_conflict_rule('god', 'NNP')
        lrvr.add_postag_conflict_rule('loser', 'NN')
        lrvr.add_global_conflict_rule('so')
        rtkns, rtags = lrvr.run(tkns, tags)
        self.assertEqual(rtkns, extkns)
        self.assertEqual(rtags, tags)


class TestStopWordFilter(unittest.TestCase):
    def test_run(self):
        tkns = ['she', 'is', 'a', 'doctor', 'not', 'an', 'engineer']
        extkns = ['doctor', 'engineer']
        swf = tkn_transform.StopWordFilter({'she', 'is', 'a', 'not', 'an'})
        self.assertEqual(swf.run(tkns)[0], extkns)
        self.assertEqual(swf.run(tkns)[1], [None] * len(extkns))


class TestNumberFilter(unittest.TestCase):
    def test_run(self):
        tkns = 'it happened 123 times'.split()
        extkns = ['it', 'happened', 'times']
        nf = tkn_transform.NumberFilter()
        self.assertEqual(nf.run(tkns)[0], extkns)
        self.assertEqual(nf.run(tkns)[1], [None] * len(extkns))


class TestMatchGeneralizeTransformer(unittest.TestCase):
    def test_run(self):
        mg = match_generalizer.MatchGeneralizer()
        mg.add(('csng',), ('casing',),)
        mg.add(('pump', 'shaft',), ('pump_shaft',))
        tkns = 'the csng is near a pump shaft and a pump'.split(' ')
        ex = 'the casing is near a pump_shaft and a pump'.split(' ')
        tr = tkn_transform.MatchGeneralizeTransformer(mg)
        ptkns, ptags = tr.run(tkns)
        self.assertEqual(ptkns, ex)


class TestMethods(unittest.TestCase):
    def test_run_pipeline(self):
        tkns = ['i', 'am', 'running', 'fast']
        swf = tkn_transform.StopWordFilter({'i', 'am'})
        sr = tkn_transform.StemReplacer()
        _tkns, _tags = tkn_transform.run_pipeline([swf, sr], tkns)
        self.assertEqual(_tkns, ['run', 'fast'])


if __name__ == '__main__':
    unittest.main()
