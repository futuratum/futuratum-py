import unittest
from lib.saxutil.txt_proc.match_generalizer import MatchGeneralizer


class TestMatchGeneralizer(unittest.TestCase):
    def test_run(self):
        mg = MatchGeneralizer(case_sensitive=False)
        mg.add( ('too', 'much',), ('toomuch',), ('JJ',) )
        mg.add( ('too', 'far',), ('toofar',), ('JJ',) )
        mg.add( ('😋',), ('emo_smile',), ('UH',) )
        tkns = 'it is too much and too far for me 😋'.split(' ')
        extkns = 'it is toomuch and toofar for me emo_smile'.split(' ')
        ottkns, ottags = mg.run(tkns)
        self.assertEqual(ottkns, extkns)
        self.assertEqual(ottags[ottkns.index('toomuch')], 'JJ')
        self.assertEqual(ottags[ottkns.index('toofar')], 'JJ')
        # Test serialization
        dta = MatchGeneralizer.dumps(mg)
        mgl = MatchGeneralizer.loads(dta, False)
        self.assertEqual(ottkns, extkns)
        self.assertEqual(ottags[ottkns.index('toomuch')], 'JJ')
        self.assertEqual(ottags[ottkns.index('toofar')], 'JJ')

        del mg
        del mgl

        # Test w/o POS tags
        mg = MatchGeneralizer(case_sensitive=False)
        mg.add( ('too', 'much',), ('toomuch',) )
        mg.add( ('too', 'far',), ('toofar',) )
        mg.add( ('😋',), ('emo_smile',))
        ottkns, ottags = mg.run(tkns)
        self.assertEqual(ottkns, extkns)
        # Test serialization
        dta = MatchGeneralizer.dumps(mg)
        mgl = MatchGeneralizer.loads(dta, False)
        self.assertEqual(ottkns, extkns)
        self.assertEqual(ottags[ottkns.index('toomuch')], 'NN')
        self.assertEqual(ottags[ottkns.index('toofar')], 'NN')
        self.assertEqual(ottags[ottkns.index('emo_smile')], 'NN')

        # Test case senstivity
        mg = MatchGeneralizer(case_sensitive=False)
        mg.add( ('starbucks',), ('restur',) )
        tkns = 'i want to go to Starbucks'.split()
        ottkns, ottags = mg.run(tkns)
        self.assertEqual(ottkns, 'i want to go to restur'.split())

        # Test subsumed
        mg = MatchGeneralizer(case_sensitive=False)
        mg.add( ('bitcoin',), ('btc',) )
        mg.add( ('bitcoin', 'gold'), ('bcg',) )
        mg.add( ('bitcoin', 'cash'), ('bch',) )
        tkns = 'i have bitcoin gold'.split(' ')
        extkns = 'i have bcg'.split(' ')
        self.assertEqual(mg.run(tkns)[0], extkns)
        tkns = 'i have bitcoin cash'.split(' ')
        extkns = 'i have bch'.split(' ')
        self.assertEqual(mg.run(tkns)[0], extkns)



if __name__ == '__main__':
    unittest.main()