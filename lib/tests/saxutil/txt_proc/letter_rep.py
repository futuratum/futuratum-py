import unittest
import spacy
from lib.saxutil.txt_proc import letter_rep


class TestLetterRepMiner(unittest.TestCase):
    def test_run(self):
        nlp = spacy.load(
            'en',
            add_vectors=False,
            entity=False,
            matcher=False,
            parser=False,
        )
        lrm = letter_rep.LetterRepMiner(nlp.vocab)
        matches = lrm.get_vocab_matches('lllooossseeerrr')
        self.assertEqual(matches, {'loser', 'looser'})


if __name__ == '__main__':
    unittest.main()
