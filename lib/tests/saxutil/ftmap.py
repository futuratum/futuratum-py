from __future__ import print_function
import unittest
from lib.saxutil import ftmap
from collections import Counter


class TestFtmap(unittest.TestCase):
    def test_run(self):
        fm = ftmap.FeatureMap()

        str_objs = ['w0', 'w1', 'w2', 'w3']
        for sobj in str_objs:
            self.assertFalse(ftmap.is_compressable(sobj))
            fm.get_ft_add_on_absent(sobj)

        tup_objs = [('w0', 'w1',)]
        for tobj in tup_objs:
            self.assertTrue(ftmap.is_compressable(tobj))
            fm.get_ft_add_on_absent(tobj)

        self.assertEqual(fm.get_ft('w0'), 0)
        self.assertEqual(fm.get_ft('w1'), 1)
        self.assertEqual(fm.get_ft('w2'), 2)
        self.assertEqual(fm.get_ft('w3'), 3)
        self.assertEqual(fm.get_ft('w4'), None)

        # Test bigram feature objects
        ftnum = fm.get_ft(('w0', 'w1',))
        self.assertEqual(fm.get_obj(ftnum), ('w0', 'w1',))

        # Test unigram feature objects
        for i in range(4):
            self.assertEqual(fm.get_obj(i), 'w' + str(i))

        fm = ftmap.rm_objs_remap(fm, {0, 1, 2})
        # w0, w1, and w2 were flagged for removal, but
        # w0 and w1 are root objects. Because other features
        # depend on the presence of w0 and w1, these two objects
        # should remain in the feature map. w2 should go away.
        # w3 was not flagged for removal so it should stay.
        self.assertEqual(fm.root_objs(), {'w0', 'w1', 'w3'})

        # Test serialization
        dta = ftmap.dumps(fm)
        fm2 = ftmap.loads(dta)
        self.assertEqual(fm2._obj_on_ft, fm._obj_on_ft)
        self.assertEqual(fm2._ft_on_obj, fm._ft_on_obj)
        self.assertEqual(fm2._cur_ix, fm._cur_ix)

    def test_mk_ft_cntr(self):
        doc = [('love', 'N'), ('love', 'N',), ('that', 'N',), ('sound', 'N',)]
        fm = ftmap.FeatureMap()
        res = fm.mk_ft_cntr(Counter(doc), add_on_absent=True)
        ftlove = fm.get_ft( ('love', 'N',) )
        ftthat = fm.get_ft( ('that', 'N',) )
        ftsound = fm.get_ft( ('sound', 'N',) )
        self.assertEqual(res[ftlove], 2)
        self.assertEqual(res[ftthat], 1)
        self.assertEqual(res[ftsound], 1)

        doc = [('love', 'N'), ('love', 'N',), ('that', 'N',), ('near', 'N',)]
        res = fm.mk_ft_cntr(Counter(doc), add_on_absent=True)
        ftnear = fm.get_ft( ('near', 'N',) )
        self.assertEqual(fm.get_ft(('near', 'N')), ftnear)
        self.assertEqual(ftlove, fm.get_ft(('love', 'N',)))
        """
        tups = list(fm._obj_on_ft.items())
        tups.sort()
        for t in tups: print(t)
        """


if __name__ == '__main__':
    unittest.main()
