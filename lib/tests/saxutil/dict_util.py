import unittest
from lib.saxutil import dict_util


class TestDictUtilMethods(unittest.TestCase):
    def test_serialize_methods(self):
        d = {'a': 1, 'b': 1, 'c': 2, 'd': 3, 'e': 3, 'f': 4}
        dta = dict_util.csv_dumps(d)
        self.assertEqual(dict_util.csv_loads(dta, vtype=int), d)

        d = {k: float(v) for k, v in d.items()}
        dta = dict_util.csv_dumps(d)
        self.assertEqual(dict_util.csv_loads(dta, vtype=float), d)


if __name__ == '__main__':
    unittest.main()
