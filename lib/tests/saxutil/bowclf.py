import shutil, os, unittest
from collections import Counter
import numpy as np
from sklearn.linear_model import LogisticRegression
from lib.saxutil import bowclf2, ftmap

class TestV2(unittest.TestCase):
    def tearDown(self):
        if os.path.exists('__tempclf__'):
            shutil.rmtree('__tempclf__')

    def assert_ft_cntrs_equal_tkn_docs(self, docs, trnr):
        '''
        :type docs: list<obj>
        :type trnr: bowclf.Trnr

        Ensure that feature counters contain features associated with
        all objects in the list of expected docs.
        '''
        for i in range(len(docs)):
            ex_cntr = Counter([trnr.fmap.get_ft(t) for t in docs[i]])
            self.assertEqual(trnr.get_row_ft_cntr(i), ex_cntr)

    def assert_mtx_correct(self, docs, trnr):
        '''
        :type docs: list<obj>
        :type trnr: bowclf.Trnr

        Ensure that matrices are formed correctly.
        All non blacklisted features should be mapped to a matrix column.
        Column mappings should correspond to the column mappings in the
        trnr object.
        '''
        mtx = np.array(trnr.mk_mtx(None).todense(), dtype=float)
        for i in range(len(docs)):
            cntr = Counter([trnr.fmap.get_ft(t) for t in docs[i]])
            ex_row = [0] * mtx.shape[1]
            for ft in cntr:
                if ft not in trnr.ft_bl:
                    col = trnr.col_on_ft[ft]
                    ex_row[col] += cntr[ft]
            self.assertEqual(ex_row, list(mtx[i]))

    def test_unigram_run(self):
        # Allocate only a single row and a single column to ensure
        # that dynamic matrix expansion works as expected.
        trnr = bowclf2.Trnr( (1, 1,) )
        docs = [
            ['love', 'love', 'that', 'sound'],
            ['hate', 'hate', 'that', 'sound'],
            ['hate', 'hate', 'that', 'sound'],
            ['need', 'need', 'that', 'sound'],
        ]
        lbls = [1, 0, 0, 1]
        for i in range(len(docs)):
            trnr.add_obj_list_doc(docs[i], lbls[i])
        self.assert_ft_cntrs_equal_tkn_docs(docs, trnr)
        # Make sure feature removal works
        trnr.rm_fts( [trnr.fmap.get_ft('that')] )
        exdocs = [
            ['love', 'love', 'sound'],
            ['hate', 'hate', 'sound'],
            ['hate', 'hate', 'sound'],
            ['need', 'need', 'sound']
        ]
        self.assert_ft_cntrs_equal_tkn_docs(exdocs, trnr)
        trnr.map_fts_to_cols()
        self.assert_mtx_correct(exdocs, trnr)

        dfres = trnr.ftmtx_resultant(docfreq=True)
        tfres = trnr.ftmtx_resultant(docfreq=False)
        # TODO: Test output
        lovecol = trnr.col_on_ft[trnr.fmap.get_ft('love')]
        hatecol = trnr.col_on_ft[trnr.fmap.get_ft('hate')]
        needcol = trnr.col_on_ft[trnr.fmap.get_ft('need')]
        soundcol = trnr.col_on_ft[trnr.fmap.get_ft('sound')]
        # df
        self.assertEqual(dfres[lovecol], 1)
        self.assertEqual(dfres[hatecol], 2)
        self.assertEqual(dfres[needcol], 1)
        self.assertEqual(dfres[soundcol], 4)
        # tf
        self.assertEqual(tfres[lovecol], 2)
        self.assertEqual(tfres[hatecol], 4)
        self.assertEqual(tfres[needcol], 2)
        self.assertEqual(tfres[soundcol], 4)

        self.assertEqual(trnr.lbl_rows({1}), {0, 3})
        self.assertEqual(trnr.lbl_rows({0}), {1, 2})

        dfres_pos = trnr.ftmtx_lbl_resultant(1, docfreq=True)
        self.assertEqual(dfres_pos[lovecol], 1)
        self.assertEqual(dfres_pos[hatecol], 0)
        self.assertEqual(dfres_pos[needcol], 1)
        self.assertEqual(dfres_pos[soundcol], 2)
        dfres_neg = trnr.ftmtx_lbl_resultant(0, docfreq=True)
        self.assertEqual(dfres_neg[lovecol], 0)
        self.assertEqual(dfres_neg[hatecol], 2)
        self.assertEqual(dfres_neg[needcol], 0)
        self.assertEqual(dfres_neg[soundcol], 2)

        clf = trnr.to_clf(LogisticRegression(C=20))
        for d in docs:
            clf.add_obj_list_doc(d)
        self.assertEqual(list(clf.predict()), [1, 0, 0, 1])

        trnr.clear()
        self.assertEqual( list(trnr.ftmtx_resultant()), [] )
        self.assertEqual(trnr.lastrow, 0)

    def test_tuple_obj_run(self):
        trnr = bowclf2.Trnr( (1, 1,) )
        docs = [
            [('love', 'N',), ('love', 'N',), ('that', 'N',), ('sound', 'N',)],
            [('hate', 'N',), ('hate', 'N',), ('that', 'N',), ('sound', 'N',)],
            [('hate', 'N',), ('hate', 'N',), ('that', 'N',), ('sound', 'N',)],
            [('need', 'N',), ('need', 'N',), ('that', 'N',), ('sound', 'N',)],
        ]
        lbls = [1, 0, 0, 1]

        cntr = Counter(docs[0])
        for i in range(len(docs)):
            trnr.add_obj_list_doc(docs[i], lbls[i])
        self.assert_ft_cntrs_equal_tkn_docs(docs, trnr)

        # Make sure feature removal works
        trnr.rm_fts( [trnr.fmap.get_ft( ('that', 'N',) )] )
        exdocs = [
            [('love', 'N',), ('love', 'N',), ('sound', 'N',)],
            [('hate', 'N',), ('hate', 'N',), ('sound', 'N',)],
            [('hate', 'N',), ('hate', 'N',), ('sound', 'N',)],
            [('need', 'N',), ('need', 'N',), ('sound', 'N',)],
        ]
        self.assert_ft_cntrs_equal_tkn_docs(exdocs, trnr)
        trnr.map_fts_to_cols()
        self.assert_mtx_correct(exdocs, trnr)

        dfres = trnr.ftmtx_resultant(True)
        tfres = trnr.ftmtx_resultant(False)
        # TODO: Test output
        lovecol = trnr.col_on_ft[trnr.fmap.get_ft( ('love', 'N',) )]
        hatecol = trnr.col_on_ft[trnr.fmap.get_ft( ('hate', 'N',) )]
        needcol = trnr.col_on_ft[trnr.fmap.get_ft( ('need', 'N',) )]
        soundcol = trnr.col_on_ft[trnr.fmap.get_ft( ('sound', 'N',) )]
        # df
        self.assertEqual(dfres[lovecol], 1)
        self.assertEqual(dfres[hatecol], 2)
        self.assertEqual(dfres[needcol], 1)
        self.assertEqual(dfres[soundcol], 4)
        # tf
        self.assertEqual(tfres[lovecol], 2)
        self.assertEqual(tfres[hatecol], 4)
        self.assertEqual(tfres[needcol], 2)
        self.assertEqual(tfres[soundcol], 4)

        clf = trnr.to_clf(LogisticRegression(C=20))
        for d in docs:
            clf.add_obj_list_doc(d)
        preds = clf.predict(True)
        self.assertEqual([p[0] for p in preds], [1, 0, 0, 1])

        # Test directory serialization
        bowclf2.serialize_clf_to_dir(clf, '__tempclf__')
        clf2 = bowclf2.load_clf_from_dir('__tempclf__')
        for d in docs:
            clf2.add_obj_list_doc(d)
        preds2 = clf2.predict(True)
        self.assertEqual(len(preds), len(preds2))
        for i in range(len(preds)):
            self.assertEqual(preds[i][0], preds2[i][0])
            self.assertEqual(preds[i][1], preds2[i][1])

        # Test lowest score pct features
        self.assertEqual(
            trnr.lowest_score_pct_fts(.4),
            {trnr.fmap.get_ft( ('sound', 'N',) ),}
        )

        # Test clear
        trnr.clear()
        self.assertEqual( list(trnr.ftmtx_resultant()), [] )
        self.assertEqual(trnr.lastrow, 0)

    def test_fts_below_freq_unigram(self):
        trnr = bowclf2.Trnr( (1,1,) )
        docs = [
            'the the pump casing'.split(),
            'a pump shaft'.split(),
            'a pump impeller'.split()
        ]
        for d in docs:
            trnr.add_obj_list_doc(d, 1)
        tf_fts = trnr.fts_below_freq(2)
        ex_fts = {
            trnr.fmap.get_ft('casing'),
            trnr.fmap.get_ft('shaft'),
            trnr.fmap.get_ft('impeller')
        }
        self.assertEqual(tf_fts, ex_fts)
        df_fts = trnr.fts_below_freq(2, True)
        ex_fts.add(trnr.fmap.get_ft('the'))
        self.assertEqual(df_fts, ex_fts)
"""
class TestMethods(unittest.TestCase):
    def test_mk_mtx(self):
        fmap = ftmap.FeatureMap()
        objs = ['w0', 'w1', 'w2']
        for o in objs:
            fmap.get_ft_add_on_absent(o)
        col_on_ft = {0:0, 2:1}
        ft_cntrs = [
            {0:1, 2:2},
            {0:4, 2:1},
            {2:3},
        ]
        mtx = bowclf.mk_mtx(fmap, ft_cntrs, col_on_ft)
        mtx = np.array(mtx.todense())
        self.assertEqual(list(mtx[0]), [1,2])
        self.assertEqual(list(mtx[1]), [4,1])
        self.assertEqual(list(mtx[2]), [0,3])


class TestBowClf(unittest.TestCase):
    def assert_ft_cntrs_equal_tkn_docs(self, docs, trnr):
        '''
        :type docs: list<obj>
        :type trnr: bowclf.Trnr

        Ensure that feature counters contain features associated with
        all objects in the list of expected docs.
        '''
        for i in range(len(docs)):
            ex_cntr = Counter([trnr.fmap.get_ft(t) for t in docs[i]])
            self.assertEqual(trnr.ft_cntrs[i], ex_cntr)

    def assert_mtx_correct(self, docs, trnr):
        '''
        :type docs: list<obj>
        :type trnr: bowclf.Trnr

        Ensure that matrices are formed correctly.
        All non blacklisted features should be mapped to a matrix column.
        Column mappings should correspond to the column mappings in the
        trnr object.
        '''
        mtx = np.array(trnr.mk_mtx(None).todense(), dtype=float)
        for i in range(len(docs)):
            cntr = Counter([trnr.fmap.get_ft(t) for t in docs[i]])
            ex_row = [0] * mtx.shape[1]
            for ft in cntr:
                if ft not in trnr.ft_bl:
                    col = trnr.col_on_ft[ft]
                    ex_row[col] += cntr[ft]
            self.assertEqual(ex_row, list(mtx[i]))

    def test_run(self):
        docs = [
            ['i', 'love', 'that'],
            ['the', 'great', 'stuff'],
            ['the', 'terrible', 'thing'],
            ['that', 'evil', 'lie'],
            ['the', 'big', 'event'],
            ['the', 'lie', ('the', 'lie',)],
            ['terrible', 'thing'],
            ['the', 'love', 'stuff', ('the', 'love',)]
        ]
        lbls = [1, 1, 0, 0, 1, 0, 0, 1]
        trnr = bowclf.Trnr()
        for i in range(len(docs)):
            trnr.add_obj_list_doc(docs[i], lbls[i])

        trnr.map_fts_to_cols()

        # Make sure the object contains the expected initial data
        self.assertEqual(trnr.num_docs(), len(docs))
        self.assertEqual(trnr.lbls, lbls)
        self.assert_ft_cntrs_equal_tkn_docs(docs, trnr)
        self.assert_mtx_correct(docs, trnr)

        # Undo column mapping to make sure feature scoring works
        # without it already in place.
        trnr.col_on_ft = None

        # Test features below freq method
        lows = trnr.fts_below_freq(2)
        for ft in {'in', 'evil', 'big', 'event', 'great'}:
            self.assertIn(trnr.fmap.get_ft('evil'), lows)

        # Test Feature Scoring
        sd = {trnr.fmap.get_obj(f):s for f,s in trnr.score_fts().items()}
        # column mappings should have been created and purged
        # in the score_fts method. Make sure the purge happened.
        self.assertIsNone(trnr.col_on_ft)

        self.assertGreater(sd['lie'], sd[('the', 'lie',)])
        self.assertGreater(sd['love'], sd[('the', 'love')])
        for tkn in {'the', 'that'}:
            self.assertGreater(sd['love'], sd[tkn])
            self.assertGreater(sd['lie'], sd[tkn])
            self.assertGreater(sd['terrible'], sd[tkn])

        # Test remove by low feature score
        rms = {
            trnr.fmap.get_obj(f) for f in trnr.lowest_score_pct_fts(2 / 11.0)
        }
        self.assertIsNone(trnr.col_on_ft)
        self.assertEqual(rms, {'that', 'the'})

        # Test feature removal
        rms = set()
        rms.add(trnr.fmap.get_ft('the'))
        rms.add(trnr.fmap.get_ft('that'))
        rms.add(trnr.fmap.get_ft('i'))
        rms.add(trnr.fmap.get_ft('thing'))
        rms.add(trnr.fmap.get_ft( ('the', 'love',) ))

        # Blacklist unwanted features
        trnr.add_fts_to_bl(rms)
        # blacklisted features should remain in the feature map.
        for obj in {'the', 'that', 'i', 'thing'}:
            self.assertIsNotNone(trnr.fmap.get_ft(obj))
        trnr.map_fts_to_cols()
        self.assert_mtx_correct(docs, trnr)

        # Remove blacklisted features if they are not a component
        # of a compressed feature. If a root feature is a component
        # of a compressed feature it should remain in the feature map
        trnr.rm_fts(rms)
        for obj in {'that', 'i', 'thing'}:
            self.assertIsNone(trnr.fmap.get_ft(obj))
        self.assertIsNotNone(trnr.fmap.get_ft('the'))
        self.assertIsNotNone(trnr.fmap.get_ft( ('the', 'lie',) ))
        trnr.map_fts_to_cols()

        # Infer expected docs
        docs = []
        for c in trnr.ft_cntrs:
            doc = []
            for i in c:
                obj = trnr.fmap.get_obj(i)
                # "that", "i", and "thing" are not in a compressed
                # feature. They should not be in the feature map at
                # all after the removal is function is called.
                self.assertNotIn(obj, {'that', 'i', 'thing'})
                doc.append(obj)
            docs.append(doc)

        self.assert_ft_cntrs_equal_tkn_docs(docs, trnr)
        self.assert_mtx_correct(docs, trnr)

        # Make sure matrix dims are correct
        mtx = trnr.mk_mtx()
        self.assertEqual(mtx.shape[0], len(docs))
        self.assertEqual(mtx.shape[1], len(trnr.col_on_ft))

        clf = trnr.to_clf(LogisticRegression())
        clf.add_obj_list_doc(['love', 'this', 'track'])
        clf.add_obj_list_doc(['terrible', 'war'])
        # Make sure the feature counters match up to what we would exepct
        exfc0 = trnr.fmap.mk_ft_cntr({'love':1, 'this':1, 'track':1})
        self.assertEqual(exfc0, clf.ft_cntrs[0])
        exfc1 = trnr.fmap.mk_ft_cntr({'terrible':1, 'war':1})
        self.assertEqual(exfc1, clf.ft_cntrs[1])
        self.assertEqual(clf.num_docs(), 2)
        preds = clf.predict()
        self.assertEqual(list(preds), [1, 0])
        # Check clear() function
        clf.clear()
        self.assertEqual(clf.num_docs(), 0)
        # Test directory serialization
        bowclf.serialize_clf_to_dir(clf, 'tempclf')
        clf2 = bowclf.load_clf_from_dir('tempclf')
        shutil.rmtree('tempclf')

        self.assertEqual(clf2.col_on_ft, clf.col_on_ft)
        self.assertEqual(clf2.fmap._obj_on_ft, clf.fmap._obj_on_ft)
        self.assertEqual(clf2.fmap._ft_on_obj, clf.fmap._ft_on_obj)

        clf2.add_obj_list_doc(['love', 'this', 'track'])
        clf2.add_obj_list_doc(['terrible', 'war'])
        self.assertEqual(clf2.num_docs(), 2)
        preds = clf2.predict()
        self.assertEqual(list(preds), [1, 0])

        preds = clf2.predict(return_proba=True)
        self.assertEqual([p[0] for p in preds], [1, 0])
        for i in range(len(preds)):
            self.assertGreaterEqual(preds[i][1], .5)

    def test_resultant_cntr(self):
        trnr = bowclf.Trnr()
        for i in range(5):
            trnr.add_obj_list_doc(['x', 'x', 'y'], 1)
        tfcntr = trnr.resultant_cntr(False)
        dfcntr = trnr.resultant_cntr(True)
        self.assertEqual(tfcntr[trnr.fmap.get_ft('x')], 10)
        self.assertEqual(dfcntr[trnr.fmap.get_ft('x')], 5)
        self.assertEqual(tfcntr[trnr.fmap.get_ft('y')], 5)
        self.assertEqual(dfcntr[trnr.fmap.get_ft('y')], 5)

        fts_tf = trnr.fts_below_freq(6, False)
        self.assertEqual(fts_tf, {trnr.fmap.get_ft('y')})
        fts_df = trnr.fts_below_freq(6, True)
        self.assertEqual(
            fts_df, {trnr.fmap.get_ft('y'), trnr.fmap.get_ft('x')}
        )
        self.assertEqual(dfcntr, trnr.mk_lbl_ft_resultants(True)[1])
        self.assertEqual(tfcntr, trnr.mk_lbl_ft_resultants()[1])
"""


if __name__ == '__main__':
    unittest.main()
