import unittest
from lib.saxutil import list_util


class TestMethods(unittest.TestCase):
	def test_bool_func_match_indices(self):
		l = [1, 2, 3, 3, 2, 1]
		def boolfunc(i):
			if i == 2:
				return True
			return False
		indices = list_util.bool_func_match_indices(l, boolfunc)
		self.assertEqual(indices, [1,4])

	def test_split_list_on_indices(self):
		l = [0, 1, 2, 3, 4, 5, 6, 7, 8]
		sublists = list_util.split_list_on_indices(l, [5, 3])
		self.assertEqual(sublists[0], [0, 1, 2])
		self.assertEqual(sublists[1], [3, 4])
		self.assertEqual(sublists[2], [5, 6, 7, 8])

		l = ['w', 'w', '#', 'w']
		sublists = list_util.split_list_on_indices(l, [2, 3])
		self.assertEqual(sublists[0], ['w', 'w'])
		self.assertEqual(sublists[1], ['#'])
		self.assertEqual(sublists[2], ['w'])


if __name__ == '__main__':
	unittest.main()
