import unittest, os
from sklearn.linear_model import LinearRegression
from lib.saxutil import prd_model


class TestModel(unittest.TestCase):
    def test_run(self):
        # isMale, isEmployed
        dta = [ [1, 0], [1, 0], [0, 1], [0, 1], [0, 1], [1, 1], [1, 1] ]
        # bitcoin interest level
        tgt = [ 2, 2, 0, 0, 0, 1, 1 ]
        mdl = LinearRegression()
        mdl.fit(dta, tgt)
        model = prd_model.Model(mdl, ['isMale', 'isEmployed'] )
        model.set_default_col_val('isMale', 0.5)
        model.set_default_col_val('isEmployed', 0.5)
        model.add_inst( {'isMale':1} )
        model.add_inst( {'isEmployed':1} )
        model.add_inst( {'isMale':0} )
        model.add_inst( {'isEmployed': 0} )
        model.add_inst( {'isEmployed': 0, 'isMale': 1} )
        model.add_inst( {'isEmployed': 1, 'isMale': 0} )
        print(model.rows)
        prds = model.predict()
        self.assertTrue(prds[0] > prds[1])
        self.assertTrue(prds[0] > prds[2])
        self.assertTrue(prds[0] > prds[5])
        self.assertTrue(prds[2] > prds[1])
        self.assertTrue(prds[3] > prds[1])
        self.assertEqual(list(prds).index(max(prds)), 4)
        self.assertEqual(list(prds).index(min(prds)), 5)
        model.clear()

if __name__ == '__main__':
    unittest.main()
