import unittest
import datetime
import numpy as np
import pandas as pd
from lib.saxutil.ts import pd_ts


def mk_test_datetime_ix_df():
    '''
    For use within the terminal. Get a quick datetime indexed DataFrame
    to play around with.
    '''
    lil = [
        [datetime.datetime(2017, 1, 1, 0,  0, 0), 50, 5],
        [datetime.datetime(2017, 1, 1, 0, 15, 0), 45, 5],
        [datetime.datetime(2017, 1, 1, 0, 30, 0), 40, 5],
        [datetime.datetime(2017, 1, 1, 0, 45, 0), 30, 5],
        [datetime.datetime(2017, 1, 1, 1, 15, 0), 20, 5]
    ]
    cols = ['Datetime', 'Classic', 'Boneless']
    df = pd.DataFrame(lil, columns=cols)
    df.index = df['Datetime']
    del df['Datetime']
    return df


class TestPrvPds(unittest.TestCase):
    def test_get_prv_pds_dta(self):
        lil = [
            [datetime.datetime(2017, 1, 1, 0,  0, 0), 50, 5],
            [datetime.datetime(2017, 1, 1, 0, 15, 0), 45, 5],
            [datetime.datetime(2017, 1, 1, 0, 30, 0), 40, 5],
            [datetime.datetime(2017, 1, 1, 0, 45, 0), 30, 5],
            [datetime.datetime(2017, 1, 1, 1, 15, 0), 20, 5]
        ]
        cols = ['Datetime', 'Classic', 'Boneless']
        df = pd.DataFrame(lil, columns=cols)
        df.index = df['Datetime']
        del df['Datetime']

        ref_dt = datetime.datetime(2017, 1, 1, 1, 15, 0)
        assert(df.index[-1] == ref_dt)
        # Test the method interface
        ppdf = pd_ts.get_prv_pds_dta(
            df=df,
            ref_dt=ref_dt,
            lookback=5,
            period_seconds=60 * 15
        ).fillna(-1)
        self.assertEqual(list(ppdf['Classic'].values), [50, 45, 40, 30, -1])
        self.assertEqual(list(ppdf['Boneless'].values), [5, 5, 5, 5, -1])

        # Test the method interface with skipna set to True
        ppdf = pd_ts.get_prv_pds_dta(
            df=df,
            ref_dt=ref_dt,
            lookback=100,
            period_seconds=60 * 15,
            skipna=True
        )
        self.assertEqual(list(ppdf['Classic'].values), [50, 45, 40, 30])
        self.assertEqual(list(ppdf['Boneless'].values), [5, 5, 5, 5])

        # Test the object interface
        ppf = pd_ts.PrvPdsFetcher(df['Classic'], lookback=4, period_seconds=60 * 15)
        srs = ppf.get_prv_pds_dta(ref_dt=ref_dt).fillna(-1)
        self.assertEqual(list(srs.values), [45, 40, 30, -1])


if __name__ == '__main__':
    unittest.main()
