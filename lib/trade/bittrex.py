import os
import bittrex


def load_api_v1(private=True):
    if not private:
        return bittrex.Bittrex('', '', api_version=bittrex.API_V1_1)
    return bittrex.Bittrex(
        os.environ['BITTREX_KEY'],
        os.environ['BITTREX_SECRET'],
        api_version=bittrex.API_V1_1
    )


def load_api_v2(private=True):
    if not private:
        return bittrex.Bittrex('', '', api_version=bittrex.API_V2_0)
    return bittrex.Bittrex(
        os.environ['BITTREX_KEY'],
        os.environ['BITTREX_SECRET'],
        api_version=bittrex.API_V2_0
    )


def get_balance_resp(asset):
    api = load_api_v2()
    return api.get_balance(asset)


def get_balance(asset):
    return get_balance_resp(asset)['result']['Balance']


def get_ticker(asset, exasset):
    api = load_api_v1(private=False)
    return api.get_ticker(exasset + '-' + asset)


def get_marketsummary(asset, exasset):
    api = load_api_v1(private=False)
    return api.get_marketsummary(exasset + '-' + asset)


def get_market_summaries():
    api = load_api_v1(private=False)
    return api.get_market_summaries()


def sell_limit(asset, exasset, quantity, rate):
    api = load_api_v1()
    api.sell_limit(exasset + '-' + asset, quantity, rate)


def get_availible(asset):
    return get_balance_resp(asst)['Available']

