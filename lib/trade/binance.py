import os
from binance.client import Client


def load_api(private=True):
    if not private:
        return Client('', '')
    return binance.client.Client(
        os.environ['BINANCE_KEY'],
        os.environ['BINANCE_SECRET']
    )


def get_all_tickers_response():
    api = load_api(False)
    return api.get_all_tickers()


def get_candles_response(asset, exasset, intervalstr, limit=500):
    api = load_api(False)
    mkt = asset + exasset
    return api.get_klines(symbol=mkt, interval=intervalstr, limit=limit)