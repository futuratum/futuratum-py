import datetime
import pandas as pd
from lib.saxutil import date_util

class Ledger(object):

    BUYCODE = 1
    SELLCODE = 0

    def __init__(self):
        self.l = []

    def add_transaction(self, num_units, price, code, ep):
        self.l.append({'U':num_units, 'P':price, 'C':code, 'E':ep})

    def add_buy(self, num_units, price):
        ep = date_util.convert_to_timestamp(
            pd.to_datetime(datetime.datetime.utcnow())
        )
        self.add_transaction(num_units, price, Ledger.BUYCODE, ep)

    def add_sell(self, num_units, price):
        ep = date_util.convert_to_timestamp(
            pd.to_datetime(datetime.datetime.utcnow())
        )
        self.add_transaction(num_units, price, Ledger.SELLCODE, ep)

    def get_buys(self):
        return [d for d in self.l if d['C'] == Ledger.BUYCODE]

    def get_sells(self):
        return [d for d in self.l if d['C'] == Ledger.SELLCODE]

    def total_revenue(self):
        return sum([d['P'] * d['U'] for d in self.get_sells()])

    def total_expense(self):
        return sum([d['P'] * d['U'] for d in self.get_buys()])



class TradeRecord(object):
    def __init__(self, ex_asset_bal):
        '''
        :type ex_asset_bal: flot
        :param ex_asset_bal: the total balance of the asset used for pricing
            purposes. Usually BTC, USDT, or ETH
        '''
        self.cur_ex_bal = float(ex_asset_bal)

        # Balance of the asset we're buying and selling
        # EG: POWR, LSK, OMG, SALT
        self.cur_bal = 0.0

        self.ldgr = Ledger()

    def buy(self, price, num_units):
        total_cost = num_units * price
        '''
        if total_cost > self.cur_ex_bal:
            errstmt = '{total_cost} exceeds {bal}. Balance cannot be negative'
            errstmt = errstmt.format(
                total_cost=str(total_cost), bal=str(self.cur_ex_bal)
            )
            raise ValueError(errstmt)
        '''
        self.cur_ex_bal -= total_cost
        self.ldgr.add_buy(num_units, price)
        self.cur_bal += num_units

    def buy_max_units(self, price):
        num_units = self.cur_ex_bal / float(price)
        self.buy(price, num_units)

    def sell(self, price, num_units):
        # The balance can't negative.
        if num_units > self.cur_bal:
            errstmt = '{num_units} exceeds {bal}. Balance cannot be negative.'
            errstmt = errstmt.format(
                num_units=str(num_units), bal=str(self.cur_bal)
            )
            raise ValueError(errstmt)
        self.cur_bal -= num_units
        total_rev = num_units * price
        self.cur_ex_bal += total_rev
        self.ldgr.add_sell(num_units, price)

    def sell_all_units(self, price):
        self.sell(price, self.cur_bal)

    def profit(self):
        return self.ldgr.total_revenue() - self.ldgr.total_expense()
